﻿Shader "Custom/ShadowDraw" {
	Properties {
		_MainTex ("Albedo", 2D) = "defaulttexture" {}
		_Texture ("Texture", 2D) = "defaulttexture" {}
		_Alpha ("Alpha", 2D) = "defaulttexture" {}
		_Texture_ST ("Texture_ST", Vector) = (1.0, 1.0, 0.0, 0.0)
		_Mirror_ST ("Mirror_ST", Vector) = (1.0, 1.0, 0.5, 0.0)
	}
	SubShader {
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
			ZTest Always 
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			uniform sampler2D _Alpha;
			uniform float4 _Texture_ST;
			uniform float4 _Mirror_ST;

			struct vertexInput
			{
				float4 vertex : POSITION; 
				float4 texcoord : TEXCOORD0;
			};
			
			struct fragmentInput
			{
				float4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
			};
			
			fragmentInput vert(vertexInput i) {
				fragmentInput o;
				o.pos = mul( UNITY_MATRIX_MVP, i.vertex );
				o.uv = (i.texcoord.xy * _Mirror_ST.xy - _Mirror_ST.xw * 0.5 + _Mirror_ST.zw) * _Texture_ST.xy + _Texture_ST.zw;
	            return o;
	        }

			half4 frag(fragmentInput i) : COLOR {
				half alpha = tex2D(_Alpha, i.uv).a;
				return half4(0.0, 0.0, 0.0, alpha * 0.6);
			}
			
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
