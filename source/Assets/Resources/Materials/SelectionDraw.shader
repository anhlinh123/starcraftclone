﻿Shader "Custom/SelectionDraw" {
	Properties {
		_MainTex ("Albedo", 2D) = "defaulttexture" {}
		_Texture ("Texture", 2D) = "defaulttexture" {}
		_Alpha ("Alpha", 2D) = "defaulttexture" {}
		_Palette ("Palette", 2D) = "defaulttexture" {}
		_Texture_ST ("Texture_ST", Vector) = (1.0, 1.0, 0.0, 0.0)
		_Color ("Color", Vector) = (0.5, 1.0, 0.0, 1.0)
	}
	SubShader {
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			uniform sampler2D _Texture;
			uniform sampler2D _Alpha;
			uniform sampler2D _Palette;
			uniform float4 _Texture_ST;
			uniform float4 _Mirror_ST;
			uniform float4 _Color;

			struct vertexInput
			{
				float4 vertex : POSITION; 
				float4 texcoord : TEXCOORD0;
			};
			
			struct fragmentInput
			{
				float4 pos : SV_POSITION;
				half2 uv : TEXCOORD0;
			};
			
			fragmentInput vert(vertexInput i) {
				fragmentInput o;
				o.pos = mul( UNITY_MATRIX_MVP, i.vertex );
				o.uv = i.texcoord.xy * _Texture_ST.xy + _Texture_ST.zw;
	            return o;
	        }

			half4 frag(fragmentInput i) : COLOR {
				//half refColor = tex2D(_Texture, i.uv).a;
				half alpha = tex2D(_Alpha, i.uv).a;
				return half4(_Color.rgb, alpha);
			}
			
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
