﻿Shader "Custom/MapDraw" {
	Properties {
		map ("Map", 2D) = "defaulttexture" {}
		tileAtlas_1 ("TileAtlas 1", 2D) = "defaulttexture" {}
		tileAtlas_2 ("TileAtlas 2", 2D) = "defaulttexture" {}
		palette ("Palette", 2D) = "defaulttexture" {}
	}
	SubShader {
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#define ATLAS_WIDTH   128
			#define ATLAS_HEIGHT  128
			#define MEGATILE_SIZE 32
			#define MINITILE_SIZE 8
			#define MINI_PER_MEGA 4

			uniform sampler2D map;
			uniform sampler2D tileAtlas_1;
			uniform sampler2D tileAtlas_2;
			uniform sampler2D palette;
			uniform float mapWidth, mapHeight;

			struct vertexInput
			{
				float4 vertex : POSITION; 
				float2 texcoord : TEXCOORD0;
			};
			
			struct fragmentInput
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};
			
			struct TileData
			{
				int vr4Ref;
				int flipFlag;
			};

			fragmentInput vert(vertexInput i) {
				fragmentInput o;
				o.pos = mul( UNITY_MATRIX_MVP, i.vertex );
				o.uv = i.texcoord;
	            return o;
	        }

			half4 frag(fragmentInput i) : COLOR {
				//Get tile data
				int4 data = round(tex2D(map, i.uv) * 255);
				TileData tileData;
				tileData.vr4Ref   = data.r * 128 + data.g / 2;
				tileData.flipFlag = data.g & 0x1;

				if (tileData.vr4Ref == 0)
					discard;

				//Convert UV to pixels
				int pixelCoordX = floor(i.uv.x * int(mapWidth) * MEGATILE_SIZE);
				int pixelCoordY = floor(i.uv.y * int(mapHeight) * MEGATILE_SIZE);

				//Calculate map translation
				int mapTranslateX = -floor(i.uv.x * int(mapWidth) * MINI_PER_MEGA) * MINITILE_SIZE - clamp(tileData.flipFlag * MINITILE_SIZE, 0 ,7);
				int mapTranslateY = -floor(i.uv.y * int(mapHeight) * MINI_PER_MEGA) * MINITILE_SIZE;

				//Calculate tile translation
				int tileTranslateX = (tileData.vr4Ref % ATLAS_WIDTH) * MINITILE_SIZE;
				int tileTranslateY = ((tileData.vr4Ref / ATLAS_WIDTH) % ATLAS_HEIGHT) * MINITILE_SIZE;

				//Destination UV
				float u = float((1 - tileData.flipFlag * 2) * (pixelCoordX + mapTranslateX) + tileTranslateX) / (ATLAS_WIDTH * MINITILE_SIZE);
				float v = float(pixelCoordY + mapTranslateY + tileTranslateY) / (ATLAS_HEIGHT * MINITILE_SIZE);

				half refColor1 = tex2D(tileAtlas_1, float2(u, v)).a;
				half refColor2 = tex2D(tileAtlas_2, float2(u, v)).a;
				uint refBit = 1 << (tileData.vr4Ref / ATLAS_WIDTH / ATLAS_HEIGHT);
				half2 refFlag = half2(refBit & 0x1, (refBit >> 1) & 0x1);
				half refColor = refColor1 * refFlag.x + refColor2 * refFlag.y;

				return half4(tex2D(palette, half2(refColor, 0.0)).rgb, 1.0);
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
