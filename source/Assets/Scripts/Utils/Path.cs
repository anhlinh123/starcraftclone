using System;
using UnityEngine;

public static class Path
{
#if UNITY_ANDROID
	public static String Tileset = Application.persistentDataPath + "/data/tileset/";
	public static String Arr = Application.persistentDataPath + "/data/arr/";
	public static String Unit = Application.persistentDataPath + "/data/unit/";
	public static String Map = Application.persistentDataPath + "/data/";
	public static String Scripts = Application.persistentDataPath + "/data/scripts/";
	public static String Format = Application.persistentDataPath + "/data/formats/";
#else
	public static String Tileset = "data/tileset/";
	public static String Arr = "data/arr/";
	public static String Unit = "data/unit/";
	public static String Map = "data/";
	public static String Scripts = "data/scripts/";
	public static String Format = "data/formats/";
#endif
}

