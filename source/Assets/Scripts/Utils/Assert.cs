using System;
using UnityEngine;

public class Assert
{
	public static void CheckError (bool condition, string format, params object[] args)
	{
		if (condition == false) {
			Debug.LogErrorFormat(format, args);
			Application.Quit();
		}
	}

	public static void CheckWarning (bool condition, string format, params object[] args)
	{
		if (condition == false) {
			Debug.LogWarningFormat(format, args);
		}
	}
}


