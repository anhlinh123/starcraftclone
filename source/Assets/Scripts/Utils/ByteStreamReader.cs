using System;
using System.IO;

namespace StarcraftClone
{
    public class ByteStreamReader : IDisposable
    {
        Stream m_internalStream = null;

        ByteStreamReader(byte[] data)
        {
            m_internalStream = new MemoryStream(data, false);
        }

        ByteStreamReader(String fileName, bool useByteStream)
        {
            try
            {
                if (useByteStream == true)
                {
                    m_internalStream = new MemoryStream(File.ReadAllBytes(fileName), false);
                }
                else
                {
                    m_internalStream = File.OpenRead(fileName);
                }
            }
            catch (FileNotFoundException e)
            {
#if UNITY_ASSERTIONS
                Assert.CheckError(false, e.Message);
#endif
            }
        }

        public static ByteStreamReader CreateFileStream(string fileName)
        {
            return new ByteStreamReader(fileName, false);
        }

        public static ByteStreamReader CreateMemoryStream(string fileName)
        {
            return new ByteStreamReader(fileName, true);
        }

        public static ByteStreamReader CreateByteStream(byte[] data)
        {
            return new ByteStreamReader(data);
        }

        public long Position
        {
            get
            {
                return m_internalStream.Position;
            }
        }

        public long Length
        {
            get
            {
                return m_internalStream.Length;
            }
        }

        public int ReadByte()
        {
            return m_internalStream.ReadByte();
        }

        public int ReadUInt16()
        {

            return (m_internalStream.ReadByte() & 0x00FF) | ((m_internalStream.ReadByte() << 8) & 0xFF00);
        }

        public int ReadUInt32()
        {
            return (int)  (((uint) m_internalStream.ReadByte()        & 0x000000FF)
                        | (((uint) m_internalStream.ReadByte() << 8)  & 0x0000FF00)
                        | (((uint) m_internalStream.ReadByte() << 16) & 0x00FF0000)
                        | (((uint) m_internalStream.ReadByte() << 24) & 0xFF000000));
        }

        public int ReadUInt(int length)
        {
            int value = 0;
            for (int i = 0; i < length; i++)
            {
                value |= m_internalStream.ReadByte() << (i << 3);
            }
            return value;
        }

        public String ReadString(long length)
        {
            if (m_internalStream.Position + length > m_internalStream.Length)
            {
                length = m_internalStream.Length - m_internalStream.Position - 1;
                if (length < 1)
                    return null;
            }
            byte[] buffer = new byte[length];
            m_internalStream.Read(buffer, 0, (int) length);
            return System.Text.Encoding.ASCII.GetString(buffer);
        }

        public byte[] Read(long length)
        {
            if (m_internalStream.Position + length > m_internalStream.Length)
            {
                length = m_internalStream.Length - m_internalStream.Position - 1;
                if (length < 1)
                    return null;
            }
            byte[] data = new byte[length];
            m_internalStream.Read(data, 0, (int) length);
            return data;
        }

        public int ReadByte(long offset)
        {
            m_internalStream.Seek(offset, SeekOrigin.Begin);
            return ReadByte();
        }

        public int ReadUInt16(long offset)
        {
            m_internalStream.Seek(offset, SeekOrigin.Begin);
            return ReadUInt16();
        }

        public int ReadUInt32(long offset)
        {
            m_internalStream.Seek(offset, SeekOrigin.Begin);
            return ReadUInt32();
        }

        public int ReadUint(long offset, int length)
        {
            m_internalStream.Seek(offset, SeekOrigin.Begin);
            return ReadUInt(length);
        }

        public String ReadString(long offset, long length)
        {
            m_internalStream.Seek(offset, SeekOrigin.Begin);
            return ReadString(length);
        }

        public byte[] Read(long offset, long length)
        {
            m_internalStream.Seek(offset, SeekOrigin.Begin);
            return Read(length);
        }

        public void Seek(long offset)
        {
            m_internalStream.Seek(offset, SeekOrigin.Begin);
        }

        public void Skip(long numByte)
        {
            m_internalStream.Seek(numByte, SeekOrigin.Current);
        }

        public void Close()
        {
            m_internalStream.Close();
        }

        public void Dispose()
        {
            Close();
        }

        public byte[] ReadAll()
        {
            return Read(0, m_internalStream.Length);
        }
    }
}