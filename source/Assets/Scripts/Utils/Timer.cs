﻿namespace StarcraftClone
{
    class Timer
    {
        public delegate void Callback();

        Callback m_callback = null;
        int m_timeInterval = 0;
        int m_curTime = 0;
        bool m_isLoop = true;
        bool m_isActive = false;

        public void Init(Callback callback)
        {
            m_callback = callback;
        }

        public void Start(int timeInterval, bool isLoop)
        {
            m_timeInterval = timeInterval;
            m_isLoop = isLoop;
            m_isActive = true;
        }

        public void Update()
        {
            if (m_isActive == true)
            {
                if (m_curTime >= m_timeInterval)
                {                   
                    m_curTime = 0;
                    if (m_isLoop == false)
                    {
                        m_isActive = false;
                    }
                    m_callback();
                }
                else
                    m_curTime++;
            }
        }

        public void Restart(int timeInterval)
        {
            m_curTime = 0;
            m_timeInterval = timeInterval;
            m_isActive = true;
        }
    }
}
