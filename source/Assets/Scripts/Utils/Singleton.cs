﻿using System;
using System.Reflection;

namespace StarcraftClone
{
    class Singleton<T> where T : class
    {
        static T m_instance = null;
        public static T Instance
        {
            get
            {
#if UNITY_ASSERTIONS
                Assert.CheckError(m_instance != null, "Access an singleton instance before it is created");
#endif
                return m_instance;
            }
        }

        public static void CreateInstance()
        {
            if (m_instance == null)
            {
                ConstructorInfo constructor = null;
                constructor = typeof(T).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[0], null);

                if (constructor == null || constructor.IsAssembly)
                {
#if UNITY_ASSERTIONS
                    Assert.CheckError(false, "A private or protected constructor is missing for '{0}'.", typeof(T).Name);
#endif
                }

                m_instance = (T)constructor.Invoke(null);
            }
        }

        protected Singleton()
        {
        }
    }
}
