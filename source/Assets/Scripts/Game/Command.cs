﻿using System;
using System.Runtime.InteropServices;

namespace StarcraftClone
{
    [StructLayout(LayoutKind.Explicit, Pack = 8)]
    class Command
    {
        [FieldOffset(0)] 
        public readonly CommandType Type = CommandType.NONE;

        [FieldOffset(4)] 
        public readonly int PosX = 0;

        [FieldOffset(8)]
        public readonly int PosY = 0;

        [FieldOffset(12)]
        public readonly int Object = 0;

        [FieldOffset(16)]
        public readonly int Subject = 0;

        public Command(string command)
        {
            string[] args = command.Split(' ');
            switch (args[0].ToLower())
            {
                case "select":
                    {
                        Type = CommandType.SELECT;
                        if (!Int32.TryParse(args[1], out Object))
                            Type = CommandType.NONE;
                    }
                    break;
                case "move":
                    {
                        Type = CommandType.MOVE;
                        if (!Int32.TryParse(args[1], out Object) ||
                            !Int32.TryParse(args[2], out PosX) ||
                            !Int32.TryParse(args[3], out PosY))
                            Type = CommandType.NONE;
                    }
                    break;
                case "attack":
                    {
                        Type = CommandType.ATTACK;
                        if (!Int32.TryParse(args[1], out Object) ||
                            !Int32.TryParse(args[2], out Subject))
                            Type = CommandType.NONE;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    enum CommandType
    {
        NONE,
        SELECT,
        MOVE,
        ATTACK
    }
}
