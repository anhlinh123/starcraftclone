﻿namespace StarcraftClone
{
    struct LevelInfo
    {
        public PlayerInfo[] playerInfos;
    }

    struct PlayerInfo
    {
        public int id;
        public int race;
        public bool isHuman;      
    }
}
