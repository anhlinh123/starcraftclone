﻿using StarcraftClone.AI.Steer;
using UnityEngine;

namespace StarcraftClone
{
    class BoxCastCollisionDetector : ICollisionDetector
    {
        private Transform _ownerTransform;
        private BoxCollider2D _ownerBoxCollider;
        private Flingy _ownerFlingy;
        private float _distance;

        public GameObject Owner
        {
            get
            {
                return _ownerTransform.gameObject;
            }
            set
            {
                _ownerTransform = value.transform;
                _ownerBoxCollider = value.GetComponent<BoxCollider2D>();
                _ownerFlingy = value.GetComponent<Flingy>();
            }
        }

        public float Distance
        {
            get
            {
                return _distance;
            }
            set
            {
                _distance = value;
            }
        }

        public BoxCastCollisionDetector(GameObject owner = null, float distance = Mathf.Infinity)
        {
            Owner = owner;
            _distance = distance;
        }

        public bool CanCollide()
        {
            throw new System.NotImplementedException();
        }

        public bool FindCollision(out StarcraftClone.AI.Steer.Collision outputCollision)
        {
            RaycastHit2D hit = Physics2D.BoxCast(_ownerTransform.position, _ownerBoxCollider.size, 0, _ownerFlingy.AngleToVector(_ownerFlingy.Orientation), _distance);
            /*
            if (hit.collider != null)
            {
                outputCollision.normal = new Vector3(
                    hit.point.x - hit.collider.gameObject.transform.position.x,
                    hit.point.y - hit.collider.gameObject.transform.position.y,
                    0.0f);
                outputCollision.point = hit.point;

                Gizmo.Draw("Hit", new Geometry(outputCollision.point, outputCollision.point + outputCollision.normal, Color.white));

                return true;
            }
            outputCollision.normal = hit.normal;
            outputCollision.point = hit.point;

            return false;
            */
            outputCollision.normal = hit.normal;
            outputCollision.point = hit.point;
            outputCollision.center = hit.point;
            if (hit.collider != null)
            {
                outputCollision.center = hit.collider.bounds.center;
                Gizmo.Draw("Hit", new LineGeometry(outputCollision.point, outputCollision.point + outputCollision.normal, Color.white));
                return true;
            }
            return false;
        }
    }
}
