using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace StarcraftClone
{
    class Game : MonoBehaviour
    {
        public GameObject welcomeScreen;
        public GameObject loadingText;
        public GameObject mainMenu;
        public GameObject mainCamera;
        public GameObject console;

        public Level level;

        IGameState state;
        float deltaTime = 0.0f;

        void Awake()
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 24;
        }

        // Use this for initialization
        void Start()
        {
            DataProvider.CreateInstance();
            Animation.InitAnimation();
            state = new StateWelcomeScreen(this);
            state.OnEnter();

            FileSystem.FileSystem fs = new FileSystem.DefaultFileSystem("data/");
            FileSystem.FileManager.AddFileSystem(fs);
        }

        // Update is called once per frame
        void Update()
        {
            state.OnUpdate();
        }

        public void SetState(IGameState state)
        {
            this.state.OnEnd();
            this.state = state;
            this.state.OnEnter();
        }
    }
}