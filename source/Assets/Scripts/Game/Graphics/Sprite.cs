﻿using System.Collections.Generic;
using UnityEngine;

namespace StarcraftClone
{
    class Sprite : MonoBehaviour, IAnimationEventListener
    {
        public Image imageObject;

        const int LAYER_ORDER_OVERLAY = -1;
        const int LAYER_ORDER_MAIN = 0;
        const int LAYER_ORDER_SELECTION = 1;       
        const int LAYER_ORDER_UNDERLAY = 2;

        Image m_mainImage = null;
        Image m_selectCircle = null;
        List<Image> m_overlays = null;

        string m_layer = "Default";

        public void Init(int index, string layer)
        {
            m_overlays = new List<Image>();
            m_layer = layer;

            Data spriteData = DataProvider.Instance.GetData("sprites", index);
            int imageID = spriteData.Get("image");
            m_mainImage = Instantiate(imageObject);
            m_mainImage.Init(imageID, 0, 0, layer);
            m_mainImage.transform.SetParent(transform, false);
            m_mainImage.AddAnimationListener(this);
            m_mainImage.SetFlingy(GetComponent<Flingy>());
            
            int selectCircleID = spriteData.Get("selectionCircle") + 561;
            int selectCirclePos = -spriteData.Get("verticalPosition");
            m_selectCircle = Instantiate(imageObject);
            m_selectCircle.Init(selectCircleID, 0, selectCirclePos, layer, LAYER_ORDER_SELECTION);
            m_selectCircle.transform.SetParent(transform, false);
            m_selectCircle.gameObject.SetActive(false);
        }

        public void AddAnimationListener(IAnimationEventListener listener)
        {
            m_mainImage.AddAnimationListener(listener);
        }

        public void Select(bool isSelected)
        {
            m_selectCircle.gameObject.SetActive(isSelected);
        }

        public bool PlayAnim(Animation.AnimationSet animation)
        {
            bool canPlay = m_mainImage.PlayAnim(animation);
            if (canPlay)
                foreach (Image overlay in m_overlays)
                {
                    overlay.PlayAnim(animation);
                }
            return canPlay;
        }

        public void PlayAnimLate(Animation.AnimationSet animation)
        {
            m_mainImage.PlayAnimLate(animation);
            foreach (Image overlay in m_overlays)
            {
                overlay.PlayAnimLate(animation);
            }
        }

        public void OnEvent(AnimationEvent animationEvent)
        {
            switch (animationEvent.Event)
            {
                case EventType.CREATE_OVERLAY:
                    {
                        Image image = Instantiate(imageObject);
                        image.Init(animationEvent.imageId, animationEvent.offsetX, -animationEvent.offsetY, m_layer, LAYER_ORDER_OVERLAY, 1, m_mainImage);
                        image.transform.SetParent(transform, false);
                        image.AddAnimationListener(this);
                        m_overlays.Add(image);
                    }
                    break;
                case EventType.CREATE_UNDERLAY:
                    {
                        Image image = Instantiate(imageObject);
                        image.Init(animationEvent.imageId, animationEvent.offsetX, -animationEvent.offsetY, m_layer, LAYER_ORDER_UNDERLAY, 1, m_mainImage);
                        image.transform.SetParent(transform, false);
                        image.AddAnimationListener(this);
                        m_overlays.Add(image);
                    }
                    break;
                case EventType.CREATE_UNDERLAY_NEXT_ID:
                    {
                        Image image = Instantiate(imageObject);
                        image.Init(m_mainImage.id + 1, animationEvent.offsetX, -animationEvent.offsetY, m_layer, LAYER_ORDER_UNDERLAY, 1, m_mainImage);
                        image.transform.SetParent(transform, false);
                        image.AddAnimationListener(this);
                        m_overlays.Add(image);
                    }
                    break;
                case EventType.END:
                    {
                        foreach (Image overlay in m_overlays)
                            if (overlay != null)
                                break;
                        if (m_mainImage == null)
                            Destroy(gameObject);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
