﻿using System.Collections.Generic;
using UnityEngine;

namespace StarcraftClone
{
    class Image : MonoBehaviour
    {
        const int LOCAL_LAYER_OVERLAY = -1;
        const int LOCAL_LAYER_MAIN = 0;
        const int LOCAL_LAYER_UNDERLAY = 1;
        static string[] m_drawfunctions =
        {
            "Custom/NormalDraw",    //0
            "Custom/NormalDraw",    //1
            "Custom/NormalDraw",    //2
            "Custom/NormalDraw",    //3
            "Custom/NormalDraw",    //4
            "Custom/NormalDraw",    //5
            "Custom/NormalDraw",    //6
            "Custom/NormalDraw",    //7
            "Custom/NormalDraw",    //8
            "Custom/NormalDraw",    //9
            "Custom/ShadowDraw",    //10
            "Custom/NormalDraw",    //11
            "Custom/NormalDraw",    //12
            "Custom/SelectionDraw",    //13
            "Custom/NormalDraw",    //14
            "Custom/NormalDraw",    //15
            "Custom/NormalDraw",    //16
            "Custom/NormalDraw",    //17
        };

        int m_id = 0;
        Graphic m_grp = null;
        Animation m_animation = null;
        int m_canGraphicTurn = 0;
        //bool m_bClickable       = false;
        //bool m_bUseFullIscript  = false;
        //bool m_bDrawIfCloaked   = false;
        int m_iDrawFunction = 0;
        int m_iRemapping = 0;
        int m_iIscriptID = 0;
        int m_iShieldOverlay = 0;
        int m_iAttackOverlay = 0;
        int m_iDamageOverlay = 0;
        int m_iSpecialOverlay = 0;
        int m_iLandingOverlay = 0;
        int m_iLiftOffOVerlay = 0;
        int m_iCurrentFrame = 0;
        int m_iDirection = 0;
        int m_bClickable;
        int m_bUseFullIscript;
        int m_bDrawIfCloaked;

        Image m_parent = null;
        Flingy flingy;

        SpriteRenderer spriteRenderer;


        public int id
        {
            get
            {
                return m_id;
            }
        }

        public void SetFlingy(Flingy flingy)
        {
            this.flingy = flingy;
        }

        public void PlayAnimLate(Animation.AnimationSet animation)
        {
            m_animation.PlayAnimLate(animation);
        }

        public bool PlayAnim(Animation.AnimationSet animation)
        {
            return m_animation.PlayAnim(animation);
        }

        public void AddAnimationListener(IAnimationEventListener listener)
        {
            m_animation.AddListener(listener);
        }

        public void RemoveAnimationListener(IAnimationEventListener listener)
        {
            m_animation.RemoveListener(listener);
        }

        public int Direction
        {
            get
            {
                return m_iDirection;
            }
        }

        public int Frame
        {
            get
            {
                return m_iCurrentFrame;
            }
            set
            {
                // Adjust for directions
                int adjustedDirection = value % 17;
                m_iCurrentFrame = value - adjustedDirection * m_canGraphicTurn;
                m_iDirection = adjustedDirection == 0 ? m_iDirection : adjustedDirection - 1;
                ShowFrame();
            }
        }

        public void SetLocalPosX(int posX)
        {
            transform.localPosition = new Vector3(posX / (float)Dimension.PIXELS_PER_UNIT, transform.localPosition.y, transform.localPosition.z);
        }

        public void SetLocalPosY(int posY)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, -posY / (float)Dimension.PIXELS_PER_UNIT, transform.localPosition.z);
        }

        public void SetLocalPosXY(int posX, int posY)
        {
            transform.localPosition = new Vector3(posX / (float)Dimension.PIXELS_PER_UNIT, -posY / (float)Dimension.PIXELS_PER_UNIT, transform.localPosition.z);
        }

        public void FollowMainGraphic()
        {
            if (m_parent != null)
            {
                m_iCurrentFrame = m_parent.Frame;
                m_iDirection = m_parent.Direction;
                ShowFrame();
            }
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }

        public void Init(
            int index,
            int offsetX = 0,
            int offsetY = 0,
            string layer = "Default",
            int layerOrder = 0,
            int isVisible = 1,
            Image parent = null)
        {
            name = "Image_" + index;
            transform.localPosition = new Vector3(
                offsetX / (float)Dimension.PIXELS_PER_UNIT,
                offsetY / (float)Dimension.PIXELS_PER_UNIT,
                layerOrder);

            m_id = index;
            m_parent = parent;
            Data imageData = DataProvider.Instance.GetData("images", index);

            int grpFile = imageData.Get("grpFile");
            m_grp = Graphic.GetGraphic(grpFile);
            m_canGraphicTurn = imageData.Get("graphicTurns");
            m_bClickable        = imageData.Get("clickable");
            m_bUseFullIscript   = imageData.Get("useFullIscript");
            m_bDrawIfCloaked    = imageData.Get("drawIfCloaked");
            m_iDrawFunction = imageData.Get("drawFunction");
            m_iRemapping = imageData.Get("remapping");
            m_iIscriptID = imageData.Get("iscriptID");
            m_iShieldOverlay = imageData.Get("shieldOverlay");
            m_iAttackOverlay = imageData.Get("attackOverlay");
            m_iDamageOverlay = imageData.Get("damageOverlay");
            m_iSpecialOverlay = imageData.Get("specialOverlay");
            m_iLandingOverlay = imageData.Get("landingOverlay");
            m_iLiftOffOVerlay = imageData.Get("liftOffOverlay");

            m_animation = GetComponent<Animation>();
            if (m_animation != null)
            {
                m_animation.Init(m_iIscriptID);
                m_animation.PlayAnim(Animation.AnimationSet.INIT);
            }

            spriteRenderer = GetComponent<SpriteRenderer>();
            if (spriteRenderer != null)
            {
                if (isVisible == 1)
                {
                    spriteRenderer.sortingLayerName = layer;
                    spriteRenderer.sortingOrder = -layerOrder;
                    spriteRenderer.sprite = m_grp.sprite;
                    spriteRenderer.material = new Material(Shader.Find(m_drawfunctions[m_iDrawFunction]));
                    spriteRenderer.material.SetTexture("_Texture", m_grp.texture);
                    spriteRenderer.material.SetTexture("_Alpha", m_grp.textureAlpha);
                    spriteRenderer.material.SetTexture("_Palette", m_grp.palette);
                    spriteRenderer.material.SetVector("_Texture_ST", m_grp.regions[m_iCurrentFrame]);
                    spriteRenderer.material.SetVector("_Mirror_ST", new Vector4(1.0f, 1.0f, 0.5f, 0.0f));
                }
                else
                {
                    spriteRenderer.enabled = false;
                }
            }
        }

        void Update()
        {
            if (flingy != null)
            {
                int flingyDirection = flingy.Direction;
                if (flingyDirection != m_iDirection)
                {
                    m_iDirection = flingyDirection;
                    ShowFrame();
                }
            }
        }

        void ShowFrame()
        {
            int direction = 0;
            int sign = 1;
            if (m_canGraphicTurn != 0)
            {
                //bit hack: tmp = (31 - abs(2*direction - 33)) / 2
                direction = ((m_iDirection + 1) << 1) - 33;
                sign = -(1 | (direction >> (sizeof(int) * 8 - 1)));
                int mask = direction >> sizeof(int) * 8 - 1;
                direction = (direction + mask) ^ mask;
                direction = ((31 - direction) >> 1) + 1;
            }

            SpriteRenderer renderer = GetComponent<SpriteRenderer>();
            if (renderer != null)
            {
                renderer.material.SetVector("_Texture_ST", m_grp.regions[m_iCurrentFrame + direction]);
                renderer.material.SetVector("_Mirror_ST", new Vector4((float)sign * 1.0f, 1.0f, 0.5f, 0.0f));
            }
        }
    }
}