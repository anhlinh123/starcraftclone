﻿using UnityEngine;

namespace StarcraftClone
{
    class Graphic
    {
        static Graphic[] m_cache = null;

        public Vector4[] regions { get; private set; }
        public UnityEngine.Sprite sprite { get; private set; }
        public Texture2D texture { get; private set; }
        public Texture2D textureAlpha { get; private set; }

        //This is only for testing.
        //Need a system to provide palettes to all units
        public Texture2D palette { get; private set; }

        Graphic(int index)
        {
            LoadGraphic(index);
        }

        public static Graphic GetGraphic(int index)
        {
            if (m_cache == null)
            {
                using (ByteStreamReader stream = ByteStreamReader.CreateMemoryStream(Path.Arr + "images.tbl"))
                {
                    int grpListSize = stream.ReadUInt16();
                    m_cache = new Graphic[grpListSize];
                }
            }
#if UNITY_ASSERTIONS
            Assert.CheckError(index < m_cache.Length, "Index out of bound. Index = {0}", index);
#endif
            if (m_cache[index] == null)
            {
                m_cache[index] = new Graphic(index);
            }
            return m_cache[index];
        }

        void LoadGraphic(int grpIndex)
        {
            string text = null;
            using (ByteStreamReader stream = ByteStreamReader.CreateFileStream(Path.Arr + "images.tbl"))
            {
                int offset = stream.ReadUInt16(grpIndex * 2);
                int size = grpIndex < m_cache.Length
                    ? stream.ReadUInt16() - offset
                    : (int)stream.Length - offset;
                byte[] tmp = stream.Read(offset, size - 1);
                text = stream.ReadString(offset, size - 1).Replace('\\', '/');
            }

            using (ByteStreamReader stream = ByteStreamReader.CreateMemoryStream(Path.Unit + text))
            {
                int frameCount = stream.ReadUInt16();
                int frameWidth = stream.ReadUInt16();
                int frameHeight = stream.ReadUInt16();
                int atlasWidth = Mathf.CeilToInt(Mathf.Sqrt((float)frameCount));
                int atlasHeight = Mathf.CeilToInt((float)frameCount / (float)atlasWidth);
                regions = new Vector4[frameCount];
                texture = new Texture2D(atlasWidth * frameWidth, atlasHeight * frameHeight, TextureFormat.Alpha8, false);
                textureAlpha = new Texture2D(atlasWidth * frameWidth, atlasHeight * frameHeight, TextureFormat.Alpha8, false);
                byte[] rawData = new byte[atlasWidth * atlasHeight * frameWidth * frameHeight];
                byte[] rawDataAlpha = new byte[atlasWidth * atlasHeight * frameWidth * frameHeight];

                for (int curFrame = 0; curFrame < frameCount; curFrame++)
                {
                    int frameX = curFrame % atlasWidth;
                    int frameY = curFrame / atlasWidth;
                    regions[curFrame] = new Vector4(
                        1.0f / (float)atlasWidth,
                        1.0f / (float)atlasHeight,
                        (float)frameX / (float)atlasWidth,
                        (float)frameY / (float)atlasHeight);

                    int xOffset = stream.ReadByte();
                    int yOffset = stream.ReadByte();
                    int width = stream.ReadByte();
                    int height = stream.ReadByte();
                    int lineOffset = stream.ReadUInt32();

                    long currentPos = stream.Position;
                    for (int y = 0; y < height; y++)
                    {
                        int rleOffset = stream.ReadUInt16(lineOffset + y * 2) + lineOffset;
                        int linePixels = ((frameY + 1) * frameHeight - yOffset - y - 1) * (atlasWidth * frameWidth);
                        for (int x = 0; x < width; )
                        {
                            int rleValue = stream.ReadByte(rleOffset);
                            if ((rleValue & 0x80) != 0)
                            {
                                x += stream.ReadByte(rleOffset++) & 0x7f;
                            }
                            else if ((rleValue & 0x40) != 0)
                            {
                                int length = x + (stream.ReadByte(rleOffset++) & 0x3f);
                                byte color = (byte)stream.ReadByte(rleOffset++);
                                while (x < length)
                                {
                                    int dataIndex = linePixels + frameX * frameWidth + xOffset + x;
                                    rawData[dataIndex] = color;
                                    rawDataAlpha[dataIndex] = 255;
                                    x++;
                                }
                            }
                            else
                            {
                                for (int length = x + stream.ReadByte(rleOffset++); x < length; x++)
                                {
                                    byte color = (byte)stream.ReadByte(rleOffset++);
                                    int dataIndex = linePixels + frameX * frameWidth + xOffset + x;
                                    rawData[dataIndex] = color;
                                    rawDataAlpha[dataIndex] = 255;
                                }
                            }
                        }
                    }
                    stream.Seek(currentPos);
                }

                texture.LoadRawTextureData(rawData);
                texture.wrapMode = TextureWrapMode.Clamp;
                texture.filterMode = FilterMode.Point;
                texture.anisoLevel = 1;
                texture.Apply();

                textureAlpha.LoadRawTextureData(rawDataAlpha);
                textureAlpha.wrapMode = TextureWrapMode.Clamp;
                textureAlpha.filterMode = FilterMode.Point;
                textureAlpha.anisoLevel = 1;
                textureAlpha.Apply();

                sprite = UnityEngine.Sprite.Create(
                    new Texture2D(frameWidth, frameHeight),
                    new Rect(0, 0, frameWidth, frameHeight),
                    new Vector2(0.5f, 0.5f),
                    (float)Dimension.PIXELS_PER_UNIT);
            }

            //This is only for testing.
            //Need a system to provide palettes to all units
            using (ByteStreamReader stream = ByteStreamReader.CreateMemoryStream(Path.Tileset + "badlands.wpe"))
            {
                palette = new Texture2D(256, 1, TextureFormat.RGBA32, false);
                palette.wrapMode = TextureWrapMode.Clamp;
                palette.filterMode = FilterMode.Point;
                palette.anisoLevel = 1;
                palette.LoadRawTextureData(stream.ReadAll());
                palette.Apply();
            }
        }
    }
}
