﻿using System.Collections.Generic;
using UnityEngine;

namespace StarcraftClone
{
    partial class Animation : MonoBehaviour
    {
        public enum AnimationSet
        {
            NONE = -1,
            INIT,
            DEATH,
            GND_ATK_INIT,
            AIR_ATK_INIT,
            GND_ATK = GND_ATK_INIT,
            AIR_ATK = AIR_ATK_INIT,
            UNUSED_1,
            GND_ATK_RPT,
            AIR_ATK_RPT,
            CAST_SPELL,
            GND_ATK_TO_IDLE,
            AIR_ATK_TO_IDLE,
            UNUSED_2,
            WALKING,
            WALKING_TO_IDLE,
            SPECIAL_STATE_1,
            SPECIAL_STATE_2,
            ALMOST_BUILT,
            BUILT,
            LANDING,
            LIFTING,
            IS_WORKING,
            WORKING_TO_IDLE,
            WARP_IN,
            UNUSED_3,
            STAR_EDIT_INIT,
            DISABLE,
            BURROW,
            UNBURROW,
            ENABLE
        }

        enum InternalState
        {
            PLAYING,
            WAITING,
            DONE
        }

        InternalState m_state = InternalState.DONE;
        int[] m_animationOffsets = null;
        System.Collections.Generic.HashSet<int> m_animationSet = null;
        int m_currentAnimation = -1;
        int m_currentOffset = -1;
        bool m_isNoBreakCode = false;
        bool m_canRepeatAttack = true;
        List<IAnimationEventListener> m_listeners;
        Image m_image;
        AnimationSet m_nextAnimation = AnimationSet.NONE;
        Timer m_timer = null;

        public void Init(int iscriptId)
        {
            m_listeners = new List<IAnimationEventListener>();
            m_animationOffsets = GetAnimationSet(iscriptId);
            m_animationSet = new System.Collections.Generic.HashSet<int>();
            for (int i = 0; i < m_animationOffsets.Length; i++)
            {
                m_animationSet.Add(m_animationOffsets[i]);
            }
            m_image = GetComponent<Image>();
            m_timer = new Timer();
            m_timer.Init(Play);
        }

        public void AddListener(IAnimationEventListener animListener)
        {
            if (!m_listeners.Contains(animListener))
                m_listeners.Add(animListener);
        }

        public void RemoveListener(IAnimationEventListener animListener)
        {
            m_listeners.Remove(animListener);
        }

        public void PlayAnimLate(AnimationSet anim)
        {
            m_nextAnimation = anim;
            m_state = InternalState.PLAYING;
        }

        public bool PlayAnim(AnimationSet anim)
        {
#if UNITY_ASSERTIONS
            Assert.CheckError(m_animationOffsets != null && (int)anim < m_animationOffsets.Length,
                "Invalid animation, anim = {0}", anim);
#endif
            //Test code, need to remove later
            if (m_animationOffsets == null || (int)anim >= m_animationOffsets.Length)
                return false;
            ////////////////////////////////
            int offset = m_animationOffsets[(int)anim];
            if (offset == 0)
                return false;
            if (m_isNoBreakCode == false)
            {
                switch (anim)
                {
                    case AnimationSet.GND_ATK_INIT:
                    case AnimationSet.GND_ATK_RPT:
                        {
                            if (m_currentAnimation == m_animationOffsets[(int)AnimationSet.GND_ATK_RPT])
                            {
                                if (m_canRepeatAttack)
                                {
                                    Goto(m_animationOffsets[(int)AnimationSet.GND_ATK_RPT]);
                                    m_canRepeatAttack = false;
                                    return true;
                                }
                            }
                            else if (m_currentAnimation != m_animationOffsets[(int)AnimationSet.GND_ATK_INIT])
                            {
                                Goto(m_animationOffsets[(int)AnimationSet.GND_ATK_INIT]);
                                return true;
                            }
                            break;
                        }
                    case AnimationSet.AIR_ATK_INIT:
                    case AnimationSet.AIR_ATK_RPT:
                        {
                            if (m_currentAnimation == m_animationOffsets[(int)AnimationSet.AIR_ATK_RPT])
                            {
                                if (m_canRepeatAttack)
                                {
                                    Goto(m_animationOffsets[(int)AnimationSet.AIR_ATK_RPT]);
                                    m_canRepeatAttack = false;
                                    return true;
                                }
                            }
                            else if (m_currentAnimation != m_animationOffsets[(int)AnimationSet.AIR_ATK_INIT])
                            {
                                Goto(m_animationOffsets[(int)AnimationSet.AIR_ATK_INIT]);
                                return true;
                            }
                            break;
                        }
                    case AnimationSet.DEATH:
                        {
                            Goto(offset);
                            return true;
                        }
                    case AnimationSet.GND_ATK_TO_IDLE:
                    case AnimationSet.AIR_ATK_TO_IDLE:
                    case AnimationSet.WALKING_TO_IDLE:                   
                    case AnimationSet.WORKING_TO_IDLE:
                        {
                            int animationSetCount = m_animationOffsets.Length;
                            if ((animationSetCount < (int)AnimationSet.GND_ATK_TO_IDLE || m_currentAnimation != m_animationOffsets[(int)AnimationSet.GND_ATK_TO_IDLE])
                                && (animationSetCount < (int)AnimationSet.AIR_ATK_TO_IDLE || m_currentAnimation != m_animationOffsets[(int)AnimationSet.AIR_ATK_TO_IDLE])
                                && (animationSetCount < (int)AnimationSet.WALKING_TO_IDLE || m_currentAnimation != m_animationOffsets[(int)AnimationSet.WALKING_TO_IDLE])
                                && (animationSetCount < (int)AnimationSet.WORKING_TO_IDLE || m_currentAnimation != m_animationOffsets[(int)AnimationSet.WORKING_TO_IDLE]))
                            {
                                Goto(offset);
                                return true;
                            }
                            break;
                        }
                    default:
                        {
                            if (offset != m_currentAnimation)
                            {
                                Goto(offset);
                                return true;
                            }
                            break;
                        }
                }
            }
            return false;
        }

        void Goto(int offset)
        {
            m_currentOffset = offset;
            m_currentAnimation = offset;
            m_state = InternalState.PLAYING;
            m_timer.Start(0, false);
        }

        void FixedUpdate()
        {
            if (!m_isNoBreakCode && m_nextAnimation != AnimationSet.NONE)
            {
                PlayAnim(m_nextAnimation);
                m_nextAnimation = AnimationSet.NONE;
            }
            if (m_state != InternalState.DONE)
                m_timer.Update();
        }

        void Play()
        {
            m_state = InternalState.PLAYING;
            while (m_state == InternalState.PLAYING)
            {
                int opcode = 0;
                byte[] args = null;
                GetInstruction(ref m_currentOffset, ref opcode, ref args);
                Execute(opcode, args);
                if (m_animationSet.Contains(m_currentOffset))
                {
                    m_currentAnimation = m_currentOffset;
                }
            }
        }

        void Execute(int opcode, byte[] args)
        {
            switch (opcode)
            {
                case 0x00:      //<frame#> - displays a particular frame, adjusted for direction.
                    {
                        m_image.Frame = args[0] | ((args[1] << 8) & 0xFF00);
                    }
                    break;
                case 0x01:      //<frame#> - displays a particular frame dependent on tileset.
                    break;
                case 0x02:      //<x> - sets the current horizontal offset of the current image overlay.
                    {
                        m_image.SetLocalPosX(args[0]);
                    }
                    break;
                case 0x03:      //<y> - sets the vertical position of an image overlay.
                    {
                        m_image.SetLocalPosY(args[0]);
                    }
                    break;
                case 0x04:      //<x> <y> - sets the current horizontal and vertical position of the current image overlay.
                    {
                        m_image.SetLocalPosXY(args[0], args[1]);
                    }
                    break;
                case 0x05:      //<#ticks> - pauses script execution for a specific number of ticks.
                    {
                        m_state = InternalState.WAITING;
                        m_timer.Restart(args[0]);
                    }
                    break;
                case 0x06:      //<#ticks1> <#ticks2> - pauses script execution for a random number of ticks given two possible wait times.
                    {
                        m_state = InternalState.WAITING;
                        m_timer.Restart(UnityEngine.Random.Range(args[0], args[1]));
                    }
                    break;
                case 0x07:      //<labelname> - unconditionally jumps to a specific code block.
                    {
                        int offset = args[0] | ((args[1] << 8) & 0xFF00);
                        m_currentOffset = offset;
                    }
                    break;
                case 0x08:      //<image#> <x> <y> - displays an active image overlay at an animation level higher than the current image overlay at a specified offset position.
                    {
                        AnimationEvent animEvent = new AnimationEvent(EventType.CREATE_OVERLAY);
                        animEvent.imageId = args[0] | ((args[1] << 8) & 0xFF00);
                        animEvent.offsetX = args[2];
                        animEvent.offsetY = args[3];
                        foreach (IAnimationEventListener listener in m_listeners)
                            listener.OnEvent(animEvent);
                    }
                    break;
                case 0x09:      //<image#> <x> <y> - displays an active image overlay at an animation level lower than the current image overlay at a specified offset position.
                    {
                        AnimationEvent animEvent = new AnimationEvent(EventType.CREATE_UNDERLAY);
                        animEvent.imageId = args[0] | ((args[1] << 8) & 0xFF00);
                        animEvent.offsetX = args[2];
                        animEvent.offsetY = args[3];
                        foreach (IAnimationEventListener listener in m_listeners)
                            listener.OnEvent(animEvent);
                    }
                    break;
                case 0x0a:      //<image#> - displays an active image overlay at an animation level higher than the current image overlay at the relative origin offset position.
                    break;
                case 0x0b:      //<image#> - only for powerups. Hypothesised to replace the image overlay that was first created by the current image overlay.
                    break;
                case 0x0c:      //no parameters - unknown.
                    break;
                case 0x0d:      //<image#> <x> <y> - displays an active image overlay at an animation level higher than the current image overlay, using a LO* file to determine the offset position.
                    break;
                case 0x0e:      //<image#> <x> <y> - displays an active image overlay at an animation level lower than the current image overlay, using a LO* file to determine the offset position.
                    break;
                case 0x0f:      //<sprite#> <x> <y> - spawns a sprite one animation level above the current image overlay at a specific offset position.
                    break;
                case 0x10:      //<sprite#> <x> <y> - spawns a sprite at the highest animation level at a specific offset position.
                    break;
                case 0x11:      //<sprite#> <x> <y> - spawns a sprite at the lowest animation level at a specific offset position.
                    break;
                case 0x12:      //<flingy#> - creates an flingy with restrictions; supposedly crashes in most cases.
                    break;
                case 0x13:      //<sprite#> <x> <y> - spawns a sprite one animation level below the current image overlay at a specific offset position. The new sprite inherits the direction of the current sprite. Requires LO* file for unknown reason.
                    break;
                case 0x14:      //<sprite#> <x> <y> - spawns a sprite one animation level below the current image overlay at a specific offset position. The new sprite inherits the direction of the current sprite.
                    break;
                case 0x15:      //<sprite#> <overlay#> - spawns a sprite one animation level above the current image overlay, using a specified LO* file for the offset position information. The new sprite inherits the direction of the current sprite.
                    break;
                case 0x16:      //no parameters - destroys the current active image overlay, also removing the current sprite if the image overlay is the last in one in the current sprite.
                    {
                        m_image.Destroy();
                        m_state = InternalState.DONE;
                        AnimationEvent animEvent = new AnimationEvent(EventType.END);
                        foreach (IAnimationEventListener listener in m_listeners)
                            listener.OnEvent(animEvent);
                    }
                    break;
                case 0x17:      //<flipstate> - sets flip state of the current image overlay.
                    break;
                case 0x18:      //<sound#> - plays a sound.
                    {
                        //int sound = (args [0] & 0x00FF) | ((args [1] << 8) & 0xFF00);
                        //Debug.LogFormat ("Play sound {0}", sound);
                    }
                    break;
                case 0x19:      //<#sounds> <sound#> <...> - plays a random sound from a list.
                    break;
                case 0x1a:      //<firstsound#> <lastsound#> - plays a random sound between two inclusive sfxdata.dat entry IDs.
                    break;
                case 0x1b:      //no parameters - causes the damage of a weapon flingy to be applied according to its weapons.dat entry.
                    {
                        AnimationEvent animEvent = new AnimationEvent(EventType.DO_MISSILE_DAMAGE);
                        foreach (IAnimationEventListener listener in m_listeners)
                            listener.OnEvent(animEvent);
                    }
                    break;
                case 0x1c:      //<#sounds> <sound#> <...> - applies damage to target without creating a flingy and plays a sound.
                    {
                        AnimationEvent animEvent = new AnimationEvent(EventType.ATTACK_MELEE);
                        foreach (IAnimationEventListener listener in m_listeners)
                            listener.OnEvent(animEvent);
                    }
                    break;
                case 0x1d:      //no parameters - causes the current image overlay to display the same frame as the parent image overlay.
                    {
                        m_image.FollowMainGraphic();
                    }
                    break;
                case 0x1e:      //<randchance#> <labelname> - random jump, chance of performing jump depends on the parameter.
                    {
                        int offset = args[1] | ((args[2] << 8) & 0xFF00);
                        int random = UnityEngine.Random.Range(0, 255);
                        if (random <= args[0])
                        {
                            m_currentOffset = offset;
                        }
                    }
                    break;
                case 0x1f:      //<turnamount> - turns the flingy counterclockwise by a particular amount.
                    {
                        AnimationEvent animEvent = new AnimationEvent(EventType.TURN);
                        animEvent.turnAmount = -args[0];
                        foreach (IAnimationEventListener listener in m_listeners)
                            listener.OnEvent(animEvent);
                    }
                    break;
                case 0x20:      //<turnamount> - turns the flingy clockwise by a particular amount.
                    {
                        AnimationEvent animEvent = new AnimationEvent(EventType.TURN);
                        animEvent.turnAmount = args[0];
                        foreach (IAnimationEventListener listener in m_listeners)
                            listener.OnEvent(animEvent);
                    }
                    break;
                case 0x21:      //no parameters - turns the flingy clockwise by one direction unit.
                    break;
                case 0x22:      //<turnamount> - turns the flingy a specified amount in a random direction, with a heavy bias towards turning clockwise.
                    {
                        int random = UnityEngine.Random.Range(0, 2) * 2 - 1;
                        AnimationEvent animEvent = new AnimationEvent(EventType.TURN);
                        animEvent.turnAmount = args[0] * random;
                        foreach (IAnimationEventListener listener in m_listeners)
                            listener.OnEvent(animEvent);
                    }
                    break;
                case 0x23:      //<direction> - in specific situations, performs a natural rotation to the given direction.
                    break;
                case 0x24:      //<signal#> - allows the current unit's order to proceed if it has paused for an animation to be completed.
                    break;
                case 0x25:      //<ground = 1, air = 2> - attack with either the ground or air weapon depending on a parameter.
                    {
                        AnimationEvent animEvent = new AnimationEvent(EventType.ATTACK_WITH);
                        animEvent.attackWeapon = args[0];
                        foreach (IAnimationEventListener listener in m_listeners)
                            listener.OnEvent(animEvent);
                    }
                    break;
                case 0x26:      //no parameters - attack with either the ground or air weapon depending on target.
                    break;
                case 0x27:      //no parameters - identifies when a spell should be cast in a spellcasting animation. The spell is determined by the unit's current order.
                    break;
                case 0x28:      //<weapon#> - makes the unit use a specific weapons.dat ID on its target.
                    break;
                case 0x29:      //<movedistance> - sets the unit to move forward a certain number of pixels at the end of the current tick.
                    {
                        AnimationEvent animEvent = new AnimationEvent(EventType.MOVE);
                        animEvent.moveDistance = args[0];
                        foreach (IAnimationEventListener listener in m_listeners)
                            listener.OnEvent(animEvent);
                    }
                    break;
                case 0x2a:      //no parameters - signals to StarCraft that after this point, when the unit's cooldown time is over, the repeat attack animation can be called.
                    {
                        m_canRepeatAttack = true;
                    }
                    break;
                case 0x2b:      //<frame#> - plays a particular frame, often used in engine glow animations.
                    break;
                case 0x2c:      //<frameset#> - plays a particular frame set, often used in engine glow animations.
                    break;
                case 0x2d:      //no parameters - hypothesised to hide the current image overlay until the next animation.
                    break;
                case 0x2e:      //no parameters - holds the processing of player orders until a nobrkcodeend is encountered.
                    {
                        //Debug.Log ("No break code start");
                        m_isNoBreakCode = true;
                    }
                    break;
                case 0x2f:      //no parameters - allows the processing of player orders after a nobrkcodestart instruction.
                    {
                        //Debug.Log ("No break code end");
                        m_isNoBreakCode = false;
                    }
                    break;
                case 0x30:      //no parameters - conceptually, this causes the script to stop until the next animation is called.
                    {
                        //Debug.Log ("Ignore the rest");
                        m_state = InternalState.DONE;
                        m_timer.Restart(0);
                    }
                    break;
                case 0x31:      //<distance> - creates the weapon flingy at a particular distance in front of the unit.
                    break;
                case 0x32:      //no parameters - sets the current image overlay state to hidden.
                    break;
                case 0x33:      //no parameters - sets the current image overlay state to visible.
                    break;
                case 0x34:      //<direction> - sets the current direction of the flingy.
                    {
                        AnimationEvent animEvent = new AnimationEvent(EventType.SET_FL_DIRECTION);
                        animEvent.direction = args[0];
                        foreach (IAnimationEventListener listener in m_listeners)
                            listener.OnEvent(animEvent);
                    }
                    break;
                case 0x35:      //<labelname> - calls a code block.
                    break;
                case 0x36:      //no parameters - returns from call.
                    break;
                case 0x37:      //<speed> - sets the flingy.dat speed of the current flingy.
                    break;
                case 0x38:      //<gasoverlay#> - creates gas image overlays at offsets specified by LO* files.
                    break;
                case 0x39:      //<labelname> - jumps to a code block if the current unit is a powerup and it is currently picked up.
                    break;
                case 0x3a:      //<distance> <labelname> - jumps to a block depending on the distance to the target.
                    break;
                case 0x3b:      //<angle1> <angle2> <labelname> - jumps to a block depending on the current angle of the target.
                    break;
                case 0x3c:      //<angle1> <angle2> <labelname> - only for units. Jump to a code block if the current sprite is facing a particular direction.
                    break;
                case 0x3d:      //<x> <y> - displays an active image overlay at the shadow animation level at a specified offset position. The image overlay that will be displayed is the one that is after the current image overlay in images.dat.
                    {
                        AnimationEvent animEvent = new AnimationEvent(EventType.CREATE_UNDERLAY_NEXT_ID);
                        animEvent.offsetX = args[0];
                        animEvent.offsetY = args[1];
                        foreach (IAnimationEventListener listener in m_listeners)
                            listener.OnEvent(animEvent);
                    }
                    break;
                case 0x3e:      //no parameters - unknown.
                    break;
                case 0x3f:      //<labelname> - jumps to a code block when the current unit that is a building that is lifted off.
                    break;
                case 0x40:      //<frame#> - hypothesised to display the current image overlay's frame clipped to the outline of the parent image overlay.
                    break;
                case 0x41:      //<signal#> - most likely used with orders that continually repeat, like the Medic's healing and the Valkyrie's afterburners (which no longer exist), to clear the sigorder flag to stop the order.
                    break;
                case 0x42:      //<sprite#> <x> <y> - spawns a sprite one animation level above the current image overlay at a specific offset position, but only if the current sprite is over ground-passable terrain.
                    break;
                case 0x43:      //no parameters - unknown.
                    break;
                case 0x44:      //no parameters - applies damage like domissiledmg when on ground-unit-passable terrain.
                    break;
            }
        }
    }
}