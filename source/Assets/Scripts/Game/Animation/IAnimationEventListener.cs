﻿namespace StarcraftClone
{
    interface IAnimationEventListener
    {
        void OnEvent(AnimationEvent animationEvent);
    }
}
