﻿using System;

namespace StarcraftClone
{
    partial class Animation
    {
        static int[] m_argsSize = {
				2,  2,  1,  1,  2,  1,  2,  2, 
				4,  4,  2,  2,  0,  4,  4,  4, 
				4,  4,  2,  4,  4,  3,  0,  1, 
				2, -1,  4,  0, -1,  0,  3,  1, 
				1,  0,  1,  1,  1,  1,  0,  0, 
				1,  1,  0,  1,  1,  0,  0,  0, 
				0,  1,  0,  0,  1,  2,  0,  2, 
				1,  2,  4,  6,  6,  2,  0,  2, 
				2,  1,  4,  0,  0
			};
        static int[] m_animSetSize = { 
				2,  2,  4,  0,  0,  0,  0,  0, 
				0,  0,  0,  0,  14, 14, 16, 16,
				0,  0,  0,  0,  22, 22, 0,  24,
				26, 28, 28, 28, 0,  0,  0,  0
			};
        static ByteStreamReader m_script;

        public static void InitAnimation()
        {
            m_script = ByteStreamReader.CreateMemoryStream(Path.Scripts + "iscript.bin");
        }

        static void GetInstruction(ref int offset, ref int opcode, ref byte[] args)
        {
            lock (m_script)
            {
                opcode = m_script.ReadByte(offset++);
                int numArgs = m_argsSize[opcode];
                if (numArgs == -1)
                {
                    numArgs = m_script.ReadByte(offset++) * 2;
                }
                args = m_script.Read(offset, numArgs);
                offset += numArgs;
            }
        }

        static int[] GetAnimationSet(int iscriptId)
        {
            lock (m_script)
            {
                int offset = 0;
                offset = m_script.ReadUInt16(offset);
                while (offset != 0xFFFF)
                {
                    int id = m_script.ReadUInt16(offset);
                    if (id == iscriptId)
                    {
                        offset = m_script.ReadUInt16();
                        break;
                    }
                    else
                    {
                        offset += 4;
                    }
                }
#if UNITY_ASSERTIONS
                Assert.CheckError(offset != 0xFFFF, "Invalid animation entry, id = {0}", iscriptId);
#endif
                offset += 4;
                int type = m_script.ReadUInt16(offset);
                m_script.Seek(offset + 4);

                int[] result = new int[m_animSetSize[type]];
                for (int i = 0; i < m_animSetSize[type]; i++)
                {
                    result[i] = m_script.ReadUInt16();
                }
                return result;
            }
        }
    }
}

