﻿using System.Runtime.InteropServices;

namespace StarcraftClone
{
    /* Animation Event struct:
     * - Contains several 4-byte blocks of data
     * - First block is always EventType
     * - Each of the following blocks has multiple meanings depend on the event type
     */

    [StructLayout(LayoutKind.Explicit, Pack = 8)]
    struct AnimationEvent
    {
        [FieldOffset(0)]
        public readonly EventType Event;

        [FieldOffset(4)]
        public int turnAmount;
        [FieldOffset(4)]
        public int attackWeapon;
        [FieldOffset(4)]
        public int imageId;
        [FieldOffset(4)]
        public int moveDistance;
        [FieldOffset(4)]
        public int direction;

        [FieldOffset(8)]
        public int offsetX;

        [FieldOffset(16)]
        public int offsetY;
        
        public AnimationEvent(EventType animEvent) : this()
        {
            Event = animEvent;
        }
    }

    enum EventType
    {
        NONE,
        CREATE_OVERLAY,
        CREATE_UNDERLAY,
        CREATE_UNDERLAY_NEXT_ID,
        TURN,
        ATTACK_WITH,
        DO_MISSILE_DAMAGE,
        MOVE,
        SET_FL_DIRECTION,
        ATTACK_MELEE,
        END
    }
}
