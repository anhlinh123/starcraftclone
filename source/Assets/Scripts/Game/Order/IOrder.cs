﻿namespace StarcraftClone
{
    interface IOrder
    {
        string Name { get; }

        void Execute();
    }
}
