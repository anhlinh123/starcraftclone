﻿namespace StarcraftClone
{
    class Order_AttackMove : IOrder
    {
        Unit m_unit = null;
        UnityEngine.Vector2 m_destination;

        public Order_AttackMove(Unit unit, UnityEngine.Vector2 destination)
        {

        }

        public string Name
        {
            get { return "attackMove"; }
        }

        public void Execute()
        {
            m_unit.MoveDestination = m_destination;
        }
    }
}
