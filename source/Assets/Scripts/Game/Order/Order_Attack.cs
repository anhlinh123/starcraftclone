﻿namespace StarcraftClone
{
    class Order_Attack : IOrder
    {
        Unit m_unit = null;
        Unit m_target = null;

        public Order_Attack(Unit unit, Unit target)
        {
            m_unit = unit;
            m_target = target;
        }

        public string Name
        {
            get { return "attack"; }
        }

        public void Execute()
        {
            m_unit.AttackTarget = m_target;
        }
    }
}
