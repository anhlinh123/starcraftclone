﻿using UnityEngine;
using StarcraftClone.AI.Steer;

namespace StarcraftClone
{
    class Order_Move : IOrder
    {
        Unit unit;
        ILocation destination;

        public Order_Move(Unit unit, ILocation destination)
        {
            this.unit = unit;
            this.destination = destination;
        }

        public string Name
        {
            get { return "move"; }
        }

        public void Execute()
        {
            unit.ChaseTarget = destination;
        }
    }
}