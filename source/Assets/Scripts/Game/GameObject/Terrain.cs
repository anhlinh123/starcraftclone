﻿using System;
using UnityEngine;

namespace StarcraftClone
{
    class Terrain : MonoBehaviour
    {
        int[] m_mtmx = null;
        int m_width = 0;
        int m_height = 0;

        string m_tileSet;
        Mesh m_quad;
        Texture2D m_map;
        Texture2D[] m_tileAtlas;
        Texture2D m_palette;

        byte[] m_paletteData;
        Timer m_timer = null;

        public void SetTileset(int index)
        {
            switch (index)
            {
                case 0:
                    m_tileSet = "badlands";
                    break;
                case 1:
                    m_tileSet = "platform";
                    break;
                case 2:
                    m_tileSet = "install";
                    break;
                case 3:
                    m_tileSet = "ashworld";
                    break;
                case 4:
                    m_tileSet = "jungle";
                    break;
                case 5:
                    m_tileSet = "desert";
                    break;
                case 6:
                    m_tileSet = "ice";
                    break;
                case 7:
                    m_tileSet = "twilight";
                    break;
            }

            CreateTileAtlas();
            CreatePalette();
        }

        public void SetTerrain(int width, int height, byte[] mtmx)
        {
            m_mtmx = new int[width * height];
            m_width = width;
            m_height = height;
            using (ByteStreamReader stream = ByteStreamReader.CreateByteStream(mtmx))
            {
                for (int i = 0; i < m_mtmx.Length; i++)
                {
                    if (i * 2 < stream.Length)
                    {
                        m_mtmx[i] = stream.ReadUInt16();
                    }
                    else
                    {
                        m_mtmx[i] = 0;
                    }
                }
            }

            int[] cv5 = null;
            using (ByteStreamReader cv5Stream = ByteStreamReader.CreateMemoryStream(Path.Tileset + m_tileSet + ".cv5"))
            {
                int struct_count = (int)cv5Stream.Length / 52;
                cv5 = new int[struct_count * 16];
                for (int i = 0; i < struct_count; i++)
                {
                    cv5Stream.Skip(20);
                    for (int j = 0; j < 16; j++)
                    {
                        cv5[i * 16 + j] = cv5Stream.ReadUInt16();
                    }
                }
            }

            int[] vx4 = null;
            using (ByteStreamReader vx4Stream = ByteStreamReader.CreateMemoryStream(Path.Tileset + m_tileSet + ".vx4"))
            {
                vx4 = new int[vx4Stream.Length / 2];
                for (int i = 0; i < vx4.Length; i++)
                {
                    vx4[i] = vx4Stream.ReadUInt16();
                }
            }     

            byte[] rawData = new byte[m_width * m_height * 16 * 3];
            for (int i = 0; i < m_width; i++)
                for (int j = 0; j < m_height; j++ )
                {
                    int pos = i + j * m_width;
                    int cv5Ref = m_mtmx[pos];
                    int vx4Ref = cv5[cv5Ref];
                    for (int k = 0; k < 16; k++)
                    {
                        int x = k % 4;
                        int y = k / 4;
                        int pixPos = i * 4 + x + ((m_height - 1 - j) * 4 + (3 - y)) * m_width * 4;         //flip the map and tiles upside down
                        int vr4Ref = vx4[vx4Ref * 16 + k];
                        rawData[pixPos * 3] = (byte) ((vr4Ref >> 8) & 0xFF);
                        rawData[pixPos * 3 + 1] = (byte) (vr4Ref & 0xFF);
                    }
                }
            m_map = new Texture2D(m_width * 4, m_height * 4, TextureFormat.RGB24, false);
            m_map.wrapMode = TextureWrapMode.Clamp;
            m_map.filterMode = FilterMode.Point;
            m_map.anisoLevel = 1;
            m_map.LoadRawTextureData(rawData);
            m_map.Apply();

            MeshRenderer renderer = GetComponent<MeshRenderer>();
            renderer.material.SetTexture("map", m_map);
            renderer.material.SetFloat("mapWidth", m_width);
            renderer.material.SetFloat("mapHeight", m_height);
        }

        void CreateTileAtlas()
        {
            byte[] vr4 = null;
            using (ByteStreamReader vr4Stream = ByteStreamReader.CreateMemoryStream(Path.Tileset + m_tileSet + ".vr4"))
            {
                vr4 = vr4Stream.ReadAll();
            }
            int tile_count = vr4.Length / 64;
            int atlasWidth = 128;
            int atlasHeight = 128;
            byte[][] rawData = new byte[2][];

            int atlasIndex = 0;
            rawData[atlasIndex] = new byte[atlasWidth * atlasHeight * 8 * 8];
            for (int i = 0; i < tile_count; i++)
            {
                int tileX = i % atlasWidth;
                int tileY = (i / atlasWidth) % atlasHeight;

                for (int pixX = 0; pixX < 8; pixX++)
                    for (int pixY = 0; pixY < 8; pixY++)
                    {
                        int rawPos = (tileY * 8 + 7 - pixY) * atlasWidth * 8 + tileX * 8 + pixX;       // Flip the minitile upside down
                        int vr4Pos = i * 64 + pixY * 8 + pixX;
                        rawData[atlasIndex][rawPos] = vr4[vr4Pos];
                    }

                if ((tileY == 0 && i > atlasWidth) || i == tile_count - 1)
                {
                    m_tileAtlas[atlasIndex] = new Texture2D(atlasWidth * 8, atlasHeight * 8, TextureFormat.Alpha8, false);
                    m_tileAtlas[atlasIndex].wrapMode = TextureWrapMode.Clamp;
                    m_tileAtlas[atlasIndex].filterMode = FilterMode.Point;
                    m_tileAtlas[atlasIndex].anisoLevel = 1;
                    m_tileAtlas[atlasIndex].LoadRawTextureData(rawData[atlasIndex]);
                    m_tileAtlas[atlasIndex].Apply();

                    if (atlasIndex < 1)
                    {
                        atlasIndex++;
                        rawData[atlasIndex] = new byte[atlasWidth * atlasHeight * 8 * 8];
                    }
                }
            }

            MeshRenderer renderer = GetComponent<MeshRenderer>();
            renderer.material.SetTexture("tileAtlas_1", m_tileAtlas[0]);
            renderer.material.SetTexture("tileAtlas_2", m_tileAtlas[1]);      
        }

        void CreatePalette()
        {
            using (ByteStreamReader wpeStream = ByteStreamReader.CreateMemoryStream(Path.Tileset + m_tileSet + ".wpe"))
            {
                m_paletteData = wpeStream.ReadAll();
            }

            m_palette = new Texture2D(256, 1, TextureFormat.RGBA32, false);
            m_palette.wrapMode = TextureWrapMode.Clamp;
            m_palette.filterMode = FilterMode.Point;
            m_palette.anisoLevel = 1;
            m_palette.LoadRawTextureData(m_paletteData);
            m_palette.Apply();

            MeshRenderer renderer = GetComponent<MeshRenderer>();
            renderer.material.SetTexture("palette", m_palette);

            m_timer.Init(CyclePalette);
            m_timer.Start(GameProperties.PALETTE_CYCLING_INTERVAL, true);
        }

        void Awake()
        {
            m_quad = GetComponent<MeshFilter>().mesh;
            m_tileAtlas = new Texture2D[2];
            MeshRenderer renderer = GetComponent<MeshRenderer>();
            renderer.sortingLayerName = SortingLayer.LayerName[SortingLayer.DEFAULT];
            m_timer = new Timer();
        }

        void Update()
        {
            UpdateMesh();
            m_timer.Update();
        }

        void UpdateMesh()
        {
            Rect view = GetCameraView();

            Vector3[] vertices = m_quad.vertices;
            Vector2[] uv = m_quad.uv;

            vertices[0] = new Vector3(view.x, view.y, transform.position.z);
            vertices[1] = new Vector3(view.xMax, view.yMax, transform.position.z);
            vertices[2] = new Vector3(view.xMax, view.y, transform.position.z);
            vertices[3] = new Vector3(view.x, view.yMax, transform.position.z);

            float factorX = (float)Dimension.PIXELS_PER_UNIT / (m_width * 32);
            float factorY = (float)Dimension.PIXELS_PER_UNIT / (m_height * 32);
            uv[0] = new Vector2(view.x * factorX, view.y * factorY);
            uv[1] = new Vector2(view.xMax * factorX, view.yMax * factorY);
            uv[2] = new Vector2(view.xMax * factorX, view.y * factorY);
            uv[3] = new Vector2(view.x * factorX, view.yMax * factorY);

            m_quad.vertices = vertices;
            m_quad.uv = uv;

            m_quad.RecalculateBounds();
        }

        void CyclePalette()
        {     
            byte byte1 = m_paletteData[5 * 4];
            byte byte2 = m_paletteData[5 * 4 + 1];
            byte byte3 = m_paletteData[5 * 4 + 2];
            for (int i = 5; i > 2; i--)
            {
                m_paletteData[i * 4] = m_paletteData[(i - 1) * 4];
                m_paletteData[i * 4 + 1] = m_paletteData[(i - 1) * 4 + 1];
                m_paletteData[i * 4 + 2] = m_paletteData[(i - 1) * 4 + 2];
            }
            m_paletteData[2 * 4] = byte1;
            m_paletteData[2 * 4 + 1] = byte2;
            m_paletteData[2 * 4 + 2] = byte3;

            byte1 = m_paletteData[13 * 4];
            byte2 = m_paletteData[13 * 4 + 1];
            byte3 = m_paletteData[13 * 4 + 2];
            for (int i = 13; i > 7; i--)
            {
                m_paletteData[i * 4] = m_paletteData[(i - 1) * 4];
                m_paletteData[i * 4 + 1] = m_paletteData[(i - 1) * 4 + 1];
                m_paletteData[i * 4 + 2] = m_paletteData[(i - 1) * 4 + 2];
            }
            m_paletteData[7 * 4] = byte1;
            m_paletteData[7 * 4 + 1] = byte2;
            m_paletteData[7 * 4 + 2] = byte3;

            m_palette.LoadRawTextureData(m_paletteData);
            m_palette.Apply();       
        }

        Rect GetCameraView()
        {
            Camera main = Camera.main;
            Vector3 origin = main.ViewportToWorldPoint(new Vector3(0, 0, 0));
            Vector3 extend = main.ViewportToWorldPoint(new Vector3(1, 1, 0));
            return new Rect(origin.x, origin.y, extend.x - origin.x, extend.y - origin.y);
        }
    }
}
