﻿using StarcraftClone.AI.Steer;

namespace StarcraftClone
{
    class Unit : UnityEngine.MonoBehaviour, IAnimationEventListener
    {
        public enum State
        {
            ALIVE,
            DEAD
        }
        public State Status { get; private set; }

        //Unique identifier
        public int ID { get; private set; }

        // Vital statistics
        int m_currentHP = 0;
        int m_maxHP = 0;
        int m_currentShields = 0;
        int m_maxShields = 0;
        int m_hasShields = 0;
        int m_iArmor = 0;
        int m_iArmorUpgrade = 0;

        // Build cost
        int m_iMinerals = 0;
        int m_iVespenes = 0;
        int m_iBuildTime = 0;

        // Weapon
        Weapon m_groundWeapon = null;
        Weapon m_airWeapon = null;

        // Supply
        // To do: Need to figure out how to get halve amounts
        int m_iSupplyRequired = 0;
        int m_iSupplyProvided = 0;

        // Space
        int m_iSpaceRequired = 0;
        int m_iSpaceProvided = 0;

        // Score
        int m_iBuildScore = 0;
        int m_iDestroyScore = 0;

        // Other
        int m_iUnitSize = 0;
        int m_iSight = 0;
        int m_iTargetAcquireRange = 0;

        // Advanced Properties
        // To do: do this part

        // Reference Units
        int m_iSubUnit1 = 0;
        int m_iSubUnit2 = 0;
        int m_iInfestation = 0;

        // Sound
        // To do: do this part

        // Graphics
        int m_iTopSpeed = 0;
        int m_iAcceleration = 0;
        int m_iHaltDistance = 0;
        int m_iTurnRadius = 0;
        int m_iMoveControl = 0;

        //Image m_construction = null;
        // To do: idle portrait
        int m_iInitDirection = 0;
        int m_iElevation = 0;
        int m_iLeftDimension = 0;
        int m_iRightDimension = 0;
        int m_iUpDimension = 0;
        int m_iDownDimension = 0;
        int m_iAddonPosX = 0;
        int m_iAddonPosY = 0;

        // AI Actions
        // To do: do this part

        // New system
        Sprite m_sprite = null;
        Flingy m_flingy = null;
        UnityEngine.BoxCollider2D boxCollider;

        // Behavior tree - //To do - consider to make behavior tree become Monobehavior
        AI.BehaviorTree.BehaviorTree m_behaviorTree = null;
        System.Collections.Generic.Queue<IOrder> m_orders = null;
        IOrder m_currentOrder = null;

        //Steering behavior cache
        HaltSteering haltSteering;
        HaltSteering HaltSteering
        {
            get
            {
                if (haltSteering == null)
                {
                    haltSteering = new HaltSteering(m_flingy);
                }
                return haltSteering;
            }
        }

        UnitReachTarget _reachSteering;
        UnitReachTarget ReachSteering
        {
            get
            {
                if (_reachSteering == null)
                {
                    _reachSteering = new UnitReachTarget(gameObject, _chaseTarget);
                }
                return _reachSteering;
            }
        }

        /*
        public static Unit CreateUnit(int posX, int posY, int identifier,int id, int owner, int resourceAmount = 0)
        {
            UnityEngine.GameObject unitObject = new UnityEngine.GameObject("Unit_" + identifier + "_" + id);
            unitObject.AddComponent<Unit>();
            unitObject.transform.position = new UnityEngine.Vector3(
                posX / (float) Dimension.PIXELS_PER_UNIT, 
                posY / (float) Dimension.PIXELS_PER_UNIT,
                0);

            Unit unit = unitObject.GetComponent<Unit>();
            unit.Init(id, identifier, owner, resourceAmount); 
            return unit;
        }
         */

        public void Init(int index, int identifier, int owner = 0, int resourceAmount = 0)
        {
            Status = State.ALIVE;

            InitBehaviorTree();

            m_orders = new System.Collections.Generic.Queue<IOrder>();
            m_currentOrder = new Order_Guard();
            m_currentOrder.Execute();

            ID = identifier;
            Data unitData = DataProvider.Instance.GetData("units", index);

            //Graphic
            int flingyID = unitData.Get("graphics");
            m_flingy = GetComponent<Flingy>();
            if (m_flingy != null)
            {
                m_flingy.Init(flingyID, "Unit");
            }
            m_sprite = GetComponent<Sprite>();
            if (m_sprite != null)
            {
                m_sprite.AddAnimationListener(this);
            }

            //Weapon
            int groundWeapon = unitData.Get("groundWeapon");
            int airWeapon = unitData.Get("airWeapon");
            m_groundWeapon = new Weapon(groundWeapon, this);
            if (airWeapon == groundWeapon)
                m_airWeapon = m_groundWeapon;
            else if (airWeapon < 130)
                m_airWeapon = new Weapon(airWeapon, this);

            //Vital Attributes
            m_hasShields = unitData.Get("shieldEnabled");
            m_maxShields = unitData.Get("shieldAmount") * m_hasShields;
            m_maxHP = unitData.Get("hitPoints") >> 8;
            m_iArmorUpgrade = unitData.Get("armorUpgrade");
            m_iArmor = unitData.Get("armor");
            m_currentHP = m_maxHP;
            m_currentShields = m_maxShields;

            /*
            m_iSubUnit1             = m_file.ReadInt(index, "subUnit1");
            m_iSubUnit2             = m_file.ReadInt(index, "subUnit2");
            m_iInfestation          = m_file.ReadInt(index, "infestation");
            //m_construction          = Image.GetImage(m_file.ReadInt(index, "construction"));
            m_iInitDirection        = m_file.ReadInt(index, "unitDirection");
            m_bIsShieldEnabled      = m_file.ReadBool(index, "shieldEnabled");
            m_iShields              = m_file.ReadInt(index, "shieldAmount");
            m_iHitPoints            = m_file.ReadInt(index, "hitPoints");
            m_iElevation            = m_file.ReadInt(index, "elevationLevel");
            // To do: some more things
            m_iTargetAcquireRange   = m_file.ReadInt(index, "targetAcquireRange");
            m_iSight                = m_file.ReadInt(index, "sight");
            m_iArmorUpgrade         = m_file.ReadInt(index, "armorUpgrade");
            m_iUnitSize             = m_file.ReadInt(index, "unitSize");
            m_iArmor                = m_file.ReadInt(index, "armor");
            // To do: right click action
            // To do: some more things
            m_iAddonPosX            = m_file.ReadInt(index, "addonPosX");
            m_iAddonPosY            = m_file.ReadInt(index, "addonPosY");
             * */
            m_iLeftDimension = unitData.Get("leftDimension");
            m_iUpDimension = unitData.Get("upDimension");
            m_iRightDimension = unitData.Get("rightDimension");
            m_iDownDimension = unitData.Get("downDimension");
            boxCollider = GetComponent<UnityEngine.BoxCollider2D>();
            boxCollider.size = new UnityEngine.Vector2(
                (m_iLeftDimension + m_iRightDimension) / (float)Dimension.PIXELS_PER_UNIT,
                (m_iUpDimension + m_iDownDimension) / (float)Dimension.PIXELS_PER_UNIT);
            /*
            // To do: portrait
            m_iMinerals             = m_file.ReadInt(index, "minerals");
            m_iVespenes              = m_file.ReadInt(index, "vespenes");
            m_iBuildTime            = m_file.ReadInt(index, "buildTime");
            // To do: some more things
            m_iSupplyProvided       = m_file.ReadInt(index, "supplyProvided");
            m_iSupplyRequired       = m_file.ReadInt(index, "supplyRequired");
            m_iSpaceRequired        = m_file.ReadInt(index, "spaceRequired");
            m_iSpaceProvided        = m_file.ReadInt(index, "spaceProvided");
            m_iBuildScore           = m_file.ReadInt(index, "buildScore");
            m_iDestroyScore         = m_file.ReadInt(index, "destroyScore");
            // To do: the rest
            */
        }

        bool m_isSelected = false;
        public bool Selected
        {
            get
            {
                return m_isSelected;
            }
            set
            {
                m_isSelected = value;
                m_sprite.Select(value);
            }
        }

        public UnityEngine.Vector2 GetWorldPosition()
        {
            return transform.position * (int)Dimension.PIXELS_PER_UNIT;
        }

        public void ReceiveDamage(int damage)
        {
            int remainingDamage = damage;
            remainingDamage -= m_currentShields;
            m_currentShields = UnityEngine.Mathf.Clamp(m_currentShields - damage, 0, m_currentShields);

            if (remainingDamage > 0)
            {
                remainingDamage = UnityEngine.Mathf.Clamp(remainingDamage - m_iArmor, 1, remainingDamage);
                m_currentHP -= remainingDamage;

                if (m_currentHP <= 0)
                {
                    m_sprite.PlayAnim(Animation.AnimationSet.DEATH);
                    Status = State.DEAD;
                }
            }
        }

        public void PushOrder(IOrder order)
        {
            m_orders.Enqueue(order);
        }

        public void SetOrder(IOrder order)
        {
            m_behaviorTree.cancel();
            m_orders.Clear();
            m_currentOrder = order;
            m_currentOrder.Execute();
        }

        void TurnToTarget(Unit target)
        {
            m_flingy.Orientation = m_flingy.VectorToAngle(target.transform.position - transform.position);
        }

        void IAnimationEventListener.OnEvent(AnimationEvent animationEvent)
        {
            switch (animationEvent.Event)
            {
                case EventType.TURN:
                    {
                        m_flingy.Direction += animationEvent.turnAmount;
                    }
                    break;
                case EventType.ATTACK_WITH:
                    {
                        if (animationEvent.attackWeapon == 1)
                            m_groundWeapon.Attack(AttackTarget);
                        else
                            m_airWeapon.Attack(AttackTarget);
                    }
                    break;
                case EventType.MOVE:
                    {
                        m_flingy.Move(animationEvent.moveDistance);
                    }
                    break;
                case EventType.SET_FL_DIRECTION:
                    {
                        m_flingy.Direction = animationEvent.direction;
                    }
                    break;
                case EventType.ATTACK_MELEE:
                    {
                        m_groundWeapon.AttackMelee(AttackTarget);
                    }
                    break;
                default:
                    break;
            }
        }

        void FixedUpdate()
        {
            if (Status == State.ALIVE)
            {
                m_behaviorTree.step();
                if (m_behaviorTree.getStatus() == AI.BehaviorTree.Status.SUCCEEDED)
                {
                    if (m_orders.Count > 0)
                        m_currentOrder = m_orders.Dequeue();
                    else
                        m_currentOrder = new Order_Guard();
                    m_currentOrder.Execute();
                }

                if (m_groundWeapon != null)
                    m_groundWeapon.Update();
                if (m_airWeapon != null && m_airWeapon != m_groundWeapon)
                    m_airWeapon.Update();
            }
        }

        // Behavior tree's blackboard
        public Unit _attackTarget = null;
        public ILocation _chaseTarget = null;
        public UnityEngine.Vector2 MoveDestination;

        public Unit AttackTarget
        {
            get
            {
                return _attackTarget;
            }
            set
            {
                _attackTarget = value;
                _chaseTarget = _attackTarget.GetComponent<Flingy>();
                ReachSteering.Target = _chaseTarget;
            }
        }

        public ILocation ChaseTarget
        {
            get
            {
                return _chaseTarget;
            }
            set
            {
                _chaseTarget = value;
                ReachSteering.Target = _chaseTarget;
            }
        }

        void InitBehaviorTree()
        {
            m_behaviorTree = AI.BehaviorTree.BehaviorTreeLibraryManager.getInstance().createBehaviorTree("unit.bt", this);
            m_behaviorTree.addFunction("tmpFunction", tmpFunction);
            m_behaviorTree.addFunction("HasEnemyAround", HasEnemyAround);
            m_behaviorTree.addFunction("HasNoEnemyAround", HasNoEnemyAround);
            m_behaviorTree.addFunction("CanFire", CanFire);
            m_behaviorTree.addFunction("Fire", Fire);
            m_behaviorTree.addFunction("MoveToTarget", MoveToTarget);
            m_behaviorTree.addFunction("ShouldQuitAttacking", ShouldQuitAttacking);
        }

        public AI.BehaviorTree.Status tmpFunction()
        {
            //Debug.Log("tmpFunction");
            return AI.BehaviorTree.Status.SUCCEEDED;
        }

        public AI.BehaviorTree.Status HasEnemyAround()
        {
            //Debug.Log("HasEnemyAround");
            return AI.BehaviorTree.Status.FAILED;
        }

        public AI.BehaviorTree.Status HasNoEnemyAround()
        {
            Debug.Log("HasNoEnemyAround");
            return AI.BehaviorTree.Status.SUCCEEDED;
        }

        public AI.BehaviorTree.Status CanFire()
        {
            bool canFire = (m_groundWeapon != null && m_groundWeapon.IsTargetInAttackRange(AttackTarget))
                || (m_airWeapon != null && m_airWeapon.IsTargetInAttackRange(AttackTarget));
            return canFire ? AI.BehaviorTree.Status.SUCCEEDED : AI.BehaviorTree.Status.FAILED;
        }

        public AI.BehaviorTree.Status Fire()
        {
            Animation.AnimationSet animation = Animation.AnimationSet.INIT;
            if (m_groundWeapon != null && m_groundWeapon.IsWeaponCoolDown())
            {
                animation = Animation.AnimationSet.GND_ATK;
            }
            else if (m_airWeapon != null && m_airWeapon.IsWeaponCoolDown())
            {
                animation = Animation.AnimationSet.AIR_ATK;
            }
            else
                return AI.BehaviorTree.Status.FAILED;

            if (m_sprite.PlayAnim(animation))
            {
                //m_attackTarget = target;
                //TurnToTarget(target);
            }
            return AI.BehaviorTree.Status.FAILED;
        }

        public AI.BehaviorTree.Status MoveToTarget()
        {
            m_sprite.PlayAnim(Animation.AnimationSet.WALKING);
            ReachSteering.Target = _chaseTarget;
            m_flingy.Steering = _reachSteering;
            return AI.BehaviorTree.Status.FAILED;
        }

        public AI.BehaviorTree.Status Stop()
        {
            m_sprite.PlayAnim(Animation.AnimationSet.WALKING_TO_IDLE);
            m_flingy.Steering = HaltSteering;
            m_currentOrder = new Order_Guard();
            return AI.BehaviorTree.Status.FAILED;
        }

        public AI.BehaviorTree.Status ShouldQuitAttacking()
        {
            if (AttackTarget.Status == Unit.State.DEAD)
            {
                m_sprite.PlayAnimLate(Animation.AnimationSet.GND_ATK_TO_IDLE);
                return AI.BehaviorTree.Status.SUCCEEDED;
            }
            return AI.BehaviorTree.Status.FAILED;
        }

        [AI.BehaviorTree.SwitchFunction]
        public string GetCurrentOrder()
        {
            return m_currentOrder.Name;
        }
    }
}