﻿using StarcraftClone.AI.Steer;

namespace StarcraftClone
{
    class HaltSteering : BlendedSteering
    {
        public HaltSteering(ISteerable owner) : base(owner)
        {
        }

        protected override SteeringAcceleration CalculateRealSteering(SteeringAcceleration steering)
        {
            SteeringAcceleration haltSteering = base.CalculateRealSteering(steering);
            if (haltSteering.IsZero)
                haltSteering.linear = -Owner.LinearVelocity;
            return haltSteering;
        }
    }
}
