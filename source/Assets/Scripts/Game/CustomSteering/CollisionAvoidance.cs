using UnityEngine;
using StarcraftClone.AI.Steer;

namespace StarcraftClone
{
    public class CollisionAvoidance : SteeringBehavior
    {
        /** The minimum distance to a wall, i.e. how far to avoid collision. */
        private float _distanceFromBoundary;
        public float DistanceFromBoundary
        {
            get
            {
                return _distanceFromBoundary;
            }
            set
            {
                _distanceFromBoundary = value;
            }
        }

        BoxCollider2D boxCollider2D;

        public CollisionAvoidance(ISteerable owner, BoxCollider2D boxCollider2D) : base(owner)
        {
            this.boxCollider2D = boxCollider2D;
        }

        protected override SteeringAcceleration CalculateRealSteering(SteeringAcceleration steering)
        {
            Vector2 ownerPosition = Owner.Position;
            Vector2 ownerLinearVelocity = Owner.LinearVelocity + steering.linear;

            RaycastHit2D hit = Physics2D.BoxCast(ownerPosition, boxCollider2D.size, 0, ownerLinearVelocity, Owner.BoundingRadius * 4);

            if (hit.collider != null)
            {
                Vector2 hitCenter = hit.collider.bounds.center;
                Vector2 hitPerpendicular = new Vector2(hit.normal.y, -hit.normal.x);
                Vector2 intersection = LineIntersection(hitCenter, hitCenter + hitPerpendicular, ownerPosition, ownerPosition + ownerLinearVelocity);
                steering.linear = (intersection - hitCenter).normalized * GetActualLimiter().MaxLinearAcceleration;

                Gizmo.Draw("hitPerpendicular", new LineGeometry(hit.point, hit.point + hitPerpendicular, Color.black));
            }

            //Collision outputCollision;
            //bool collided = _collisionDetector.FindCollision(out outputCollision);

            // Return zero steering if no collision has occurred
            //if (!collided) return SteeringAcceleration.Zero;

            // Calculate and seek the target position
            //Vector3 linear = (outputCollision.point + outputCollision.normal * (Owner.BoundingRadius + _distanceFromBoundary) - Owner.Position).normalized
            //    * GetActualLimiter().MaxLinearAcceleration;

            //Vector3 linear = (Owner.Position + Owner.LinearVelocity - outputCollision.center).normalized * GetActualLimiter().MaxLinearAcceleration;

            Gizmo.Draw("CollisionAvoidance", new LineGeometry(Owner.Position, Owner.Position + steering.linear * 10, Color.yellow));

            return steering;
        }

        Vector2 LineIntersection(Vector2 origin1, Vector2 head1, Vector2 origin2, Vector2 head2)
        {
            Vector3 equation1 = new Vector3(
                origin1.y - head1.y,
                head1.x - origin1.x,
                head1.x * origin1.y - origin1.x * head1.y);
            Vector3 equation2 = new Vector3(
                origin2.y - head2.y,
                head2.x - origin2.x,
                head2.x * origin2.y - origin2.x * head2.y);

            float D = equation1.x * equation2.y - equation1.y * equation2.x;
            if (D == 0)
            {
                return new Vector2(Mathf.Infinity, Mathf.Infinity);
            }

            float Dx = equation1.z * equation2.y - equation1.y * equation2.z;
            float Dy = equation1.x * equation2.z - equation1.z * equation2.x;

            return new Vector2(Dx / D, Dy / D);
        }
    }
}