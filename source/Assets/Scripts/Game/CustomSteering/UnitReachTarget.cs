﻿using StarcraftClone.AI.Steer;
using UnityEngine;

namespace StarcraftClone
{
    class UnitReachTarget : SequenceSteering
    {
        private ILocation _target;
        public ILocation Target
        {
            get
            {
                return _target;
            }

            set
            {
                _target = value;
                arrive.Target = _target;
            }
        }

        Arrive arrive;
        CollisionAvoidance collisionAvoidance;
        Face face;

        public UnitReachTarget(GameObject owner, ILocation target) : base(owner.GetComponent<Flingy>())
        {
            _target = target;

            //face = new Face(Owner, target);
            arrive = new Arrive(Owner, target);
            arrive.DecelerationRadius = (Owner as Flingy).DecelerationRadius;
            arrive.ArrivalTolerance = 0.1f;
            collisionAvoidance = new CollisionAvoidance(Owner, owner.GetComponent<BoxCollider2D>());

            //Add(face);
            Add(arrive);
            Add(collisionAvoidance);
        }
    }
}
