﻿namespace StarcraftClone
{
    interface IGameState
    {
        void OnEnter();
        void OnUpdate();
        void OnEnd();
    }
}