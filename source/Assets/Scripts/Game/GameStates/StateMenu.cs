﻿namespace StarcraftClone
{
    class StateMenu : IGameState
    {
        Game game;
        MainMenuScript mainMenu;

        public StateMenu(Game game)
        {
            this.game = game;
            mainMenu = game.mainMenu.GetComponent<MainMenuScript>();
            mainMenu.startGameButton.onClick.AddListener(OnStartGame);
            mainMenu.mapList.selectMapCallback = OnSelectMap;
        }

        public void OnEnter()
        {
            mainMenu.gameObject.SetActive(true);
        }

        public void OnUpdate()
        {
        }

        public void OnEnd()
        {
            mainMenu.gameObject.SetActive(false);
        }

        public void OnStartGame()
        {
            game.SetState(new StateMain(game));
            game.level.CreatePlayers(mainMenu.lobby.LevelInformation);
            game.level.LoadLevel(mainMenu.mapList.GetMapName());
        }

        public void OnSelectMap()
        {
            LevelInfo levelInfo = game.level.GetLevelInfo(mainMenu.mapList.GetMapName());
            mainMenu.lobby.LevelInformation = levelInfo;
        }

        public void OnCreateGame()
        {
        }

        public void OnJoinGame()
        {
        }
    }
}
