﻿using UnityEngine;

namespace StarcraftClone
{
    class StateWelcomeScreen : IGameState
    {
        Game game;
        float deltaTime = 0.0f;

        public StateWelcomeScreen(Game game)
        {
            this.game = game;
        }

        public void OnEnter()
        {
            game.welcomeScreen.SetActive(true);
            game.loadingText.SetActive(true);
        }

        public void OnUpdate()
        {
            if (deltaTime >= 0.0f)
            {
                deltaTime = 0.0f;
                game.SetState(new StateMenu(game));
            }
            else
            {
                deltaTime += Time.deltaTime;
            }
        }

        public void OnEnd()
        {
            game.welcomeScreen.SetActive(false);
            game.loadingText.SetActive(false);
        }
    }
}
