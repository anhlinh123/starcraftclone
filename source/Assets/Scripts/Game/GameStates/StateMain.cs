﻿namespace StarcraftClone
{
    class StateMain : IGameState
    {
        Game game;

        public StateMain(Game game)
        {
            this.game = game;
        }

        public void OnEnter()
        {
            game.console.SetActive(true);
        }

        public void OnUpdate()
        {
        }

        public void OnEnd()
        {
            game.console.SetActive(false);
        }

        public void OnCommandReceived(string command)
        {
            game.level.OnCommandReceived(new Command(command));
        }
    }
}
