﻿using System.Collections.Generic;
using UnityEngine;

namespace StarcraftClone
{
    class Level : MonoBehaviour
    {
        public GameObject unitObject;
        public GameObject terrainObject;
        public Image imageObject;
        public Camera camera;

        Terrain m_terrain = null;
        List<Unit> m_units = null;
        int m_width, m_height = 0;
        int m_idCount = 0;

        void Start()
        {
            m_units = new List<Unit>();
        }

        void OnDrawGizmos()
        {
            Gizmo.Flush();
        }

        public LevelInfo GetLevelInfo(string mapName)
        {
            LevelInfo info = new LevelInfo();
            using (ByteStreamReader byteStream = ByteStreamReader.CreateFileStream(Path.Map + mapName))
            {
                while (byteStream.Position < byteStream.Length)
                {
                    string header = byteStream.ReadString(4);
                    int chunk_size = byteStream.ReadUInt32();
#if UNITY_ASSERTIONS
                    Assert.CheckError(header != null && chunk_size >= 0, "Map file corrupted");
#endif
                    if (header == "OWNR")
                    {
                        int numPlayers = 0;
                        for (int i = 0; i < 12; i++)
                        {
                            int type = byteStream.ReadByte();
                            if (type == 06)
                            {
                                numPlayers++;
                            }
                        }
                        info.playerInfos = new PlayerInfo[numPlayers];
                        break;
                    }
                    else
                    {
                        byteStream.Skip(chunk_size);
                    }
                }
            }
            return info;
        }

        public void LoadLevel(string mapName)
        {
            m_terrain = Instantiate(terrainObject).GetComponent<Terrain>();

            using (ByteStreamReader byteStream = ByteStreamReader.CreateFileStream(Path.Map + mapName))
            {
                while (byteStream.Position < byteStream.Length)
                {
                    string header = byteStream.ReadString(4);
                    int chunk_size = byteStream.ReadUInt32();
#if UNITY_ASSERTIONS
                    Assert.CheckError(header != null && chunk_size >= 0, "Map file corrupted");
#endif
                    switch (header)
                    {
                        case "ERA ":
#if UNITY_ASSERTIONS
                            Assert.CheckError(chunk_size == 2, "Invalid ERA chunk");
#endif
                            m_terrain.SetTileset(byteStream.ReadUInt16());
                            break;
                        case "DIM ":
#if UNITY_ASSERTIONS
                            Assert.CheckError(chunk_size == 4, "Invalid DIM chunk");
#endif
                            m_width = byteStream.ReadUInt16();
                            m_height = byteStream.ReadUInt16();
                            break;
                        case "MTXM":
                            m_terrain.SetTerrain(m_width, m_height, byteStream.Read(chunk_size));
                            break;
                        case "UNIT":
                            for (int i = 0; i < chunk_size; i += 36)
                            {
                                byteStream.Skip(4);     // Why?  
                                int posX = byteStream.ReadUInt16();
                                int posY = m_height * 32 - byteStream.ReadUInt16();
                                int id = byteStream.ReadUInt16();
                                byteStream.Skip(6);     // Why?
                                int owner = byteStream.ReadByte();
                                byteStream.Skip(3);     // Why?
                                int resourceAmount = byteStream.ReadUInt32();
                                byteStream.Skip(12);     // Why?
                                CreateUnit(posX, posY, id, owner, resourceAmount);
                            }
                            break;
                        case "THG2":
                            for (int i = 0; i < chunk_size; i += 10)
                            {
                                int id = byteStream.ReadUInt16();
                                int posX = byteStream.ReadUInt16();
                                int posY = m_height * 32 - byteStream.ReadUInt16();
                                int owner = byteStream.ReadByte();
                                byteStream.Skip(1);
                                int flag = byteStream.ReadUInt16();

                                if (((flag >> 3) & 0x1) == 0)
                                {
                                    Data spriteData = DataProvider.Instance.GetData("sprites", id);
                                    int imageID = spriteData.Get("image");
                                    Image image = Instantiate(imageObject);
                                    image.Init(imageID, posX, posY);
                                }
                                else
                                    CreateUnit(posX, posY, id, owner);
                            }
                            break;
                        case "FORC":
                        default:
                            byteStream.Skip(chunk_size);
                            break;
                    }
                }
            }
        }

        public void CreatePlayers(LevelInfo levelInfo)
        {
            int len = levelInfo.playerInfos.Length;
            for (int i = 0; i < len; i++)
            {
                PlayerInfo playerInfo = levelInfo.playerInfos[i];
                if (playerInfo.isHuman == true)
                {
                    GameObject player = new GameObject("player_" + playerInfo.id);
                    player.AddComponent<HumanPlayer>();
                }
            }
        }

        public Unit[] GetUnits(Player player)
        {
            return null;
        }

        public Unit[] GetUnitsInArea(Rect area, Player player)
        {
            Collider2D[] colliders = Physics2D.OverlapAreaAll(area.min, area.max);

            int len = colliders.Length;
            Unit[] result = new Unit[len];
            
            for (int i = 0; i < len; i++)
            {
                result[i] = colliders[i].GetComponent<Unit>();
            }

            return result;
        }

        public void OnCommandReceived(Command command)
        {
            switch (command.Type)
            {
                case CommandType.SELECT:
                    Unit selected = GetUnitByIdentifier(command.Object);
                    if (selected != null)
                        selected.Selected = true;
                    break;
                case CommandType.ATTACK:
                    {
                        Unit attacker = GetUnitByIdentifier(command.Object);
                        Unit target = GetUnitByIdentifier(command.Subject);
                        if (attacker != null && target != null)
                        {
                            attacker.SetOrder(new Order_Attack(attacker, target));
                        }
                        break;
                    }
                default:
                    break;
            }
        }

        void CreateUnit(int posX, int posY, int id, int owner, int resourceAmount = 0)
        {
            GameObject unitClone = (GameObject)Instantiate(
                unitObject, 
                new Vector3(posX / (float)Dimension.PIXELS_PER_UNIT,posY / (float)Dimension.PIXELS_PER_UNIT,0),
                transform.rotation);
            int i = GetUniqueIdentifier();
            unitClone.name = "unit_" + i;
            unitClone.GetComponent<Unit>().Init(id, i, owner, resourceAmount);
        }

        int GetUniqueIdentifier()
        {
            return m_idCount++;
        }

        Unit GetUnitByIdentifier(int identifier)
        {
            GameObject unit = GameObject.Find("unit_" + identifier);
            if (unit != null)
            {
                return unit.GetComponent<Unit>();
            }
            return null;
        }
    }
}
