﻿using UnityEngine;
using System.Collections.Generic;

namespace StarcraftClone
{
    class HumanPlayer : MonoBehaviour
    {
        Camera camera;
        List<Unit> selectedUnits;

        bool isSelecting = false;
        Vector3 mousePosition1;
        Rect selectionArea;

        static Texture2D _whiteTexture;
        static Texture2D WhiteTexture
        {
            get
            {
                if (_whiteTexture == null)
                {
                    _whiteTexture = new Texture2D(1, 1);
                    _whiteTexture.SetPixel(0, 0, Color.white);
                    _whiteTexture.Apply();
                }

                return _whiteTexture;
            }
        }

        static void DrawScreenRect(Rect rect, Color color)
        {
            GUI.color = color;
            GUI.DrawTexture(rect, WhiteTexture);
            GUI.color = Color.white;
        }

        static Rect GetScreenRect(Vector3 screenPosition1, Vector3 screenPosition2)
        {
            // Move origin from bottom left to top left
            screenPosition1.y = Screen.height - screenPosition1.y;
            screenPosition2.y = Screen.height - screenPosition2.y;
            // Calculate corners
            var topLeft = Vector3.Min(screenPosition1, screenPosition2);
            var bottomRight = Vector3.Max(screenPosition1, screenPosition2);
            // Create Rect
            return Rect.MinMaxRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
        }

        public static void DrawScreenRectBorder(Rect rect, float thickness, Color color)
        {
            // Top
            DrawScreenRect(new Rect(rect.xMin, rect.yMin, rect.width, thickness), color);
            // Left
            DrawScreenRect(new Rect(rect.xMin, rect.yMin, thickness, rect.height), color);
            // Right
            DrawScreenRect(new Rect(rect.xMax - thickness, rect.yMin, thickness, rect.height), color);
            // Bottom
            DrawScreenRect(new Rect(rect.xMin, rect.yMax - thickness, rect.width, thickness), color);
        }

        void Start()
        {
            camera = Camera.main;
            selectedUnits = new List<Unit>();
        }

        void Update()
        {
            if (Input.GetButtonUp("Fire2"))
            {
                foreach (Unit unit in selectedUnits)
                {
                    Vector3 destination = camera.ScreenToWorldPoint(Input.mousePosition);
                    destination.z = 0.0f;
                    unit.SetOrder(new Order_Move(unit, new Location(destination)));
                }
            }

            // If we press the left mouse button, save mouse location and begin selection
            if (Input.GetButtonDown("Fire1"))
            {
                isSelecting = true;
                mousePosition1 = Input.mousePosition;
            }
            // If we let go of the left mouse button, end selection
            if (Input.GetMouseButtonUp(0))
            {
                isSelecting = false;
                Select(mousePosition1, Input.mousePosition, null);
            }
        }

        void OnGUI()
        {
            if (isSelecting)
            {
                // Create a rect from both mouse positions
                selectionArea = GetScreenRect(mousePosition1, Input.mousePosition);
                DrawScreenRect(selectionArea, new Color(0.8f, 0.8f, 0.95f, 0.25f));
                DrawScreenRectBorder(selectionArea, 2, new Color(0.8f, 0.8f, 0.95f));
            }
        }

        void Select(Vector3 pointA, Vector3 pointB, Player player)
        {
            // Deselect all
            foreach (Unit unit in selectedUnits)
            {
                unit.Selected = false;
            }
            selectedUnits.Clear();

            // Convert to world space
            Vector2 min = camera.ScreenToWorldPoint(pointA);
            Vector2 max = camera.ScreenToWorldPoint(pointB);
            Collider2D[] colliders = Physics2D.OverlapAreaAll(min, max);

            //Select
            int len = colliders.Length;
            for (int i = 0; i < len; i++)
            {
                Unit unit = colliders[i].GetComponent<Unit>();
                selectedUnits.Add(unit);
                unit.Selected = true;
            }
        }
    }
}
