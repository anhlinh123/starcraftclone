﻿using System;
using System.Xml.Linq;
using System.IO;
using UnityEngine;

namespace StarcraftClone
{
    class Data
    {
        XNode m_dataDescriptor = null;
        ByteStreamReader m_internalStream = null;
        int m_entry = 0;

        public Data(XNode dataDescriptor, ByteStreamReader internalStream, int entry)
        {
            m_dataDescriptor = dataDescriptor;
            m_internalStream = internalStream;
            m_entry = entry;
        }

        public int Get(string propertyName)
        {
            int offset = -1, size = -1, arraySize = -1, varStart = 0, varEnd = 0;
            XNode currentNode = m_dataDescriptor;
            while(currentNode != null)
            {
                XElement currentElement = currentNode as XElement;
                if (currentElement.FirstAttribute.Value == propertyName)
                {
                    offset = (int)currentElement.Attribute("offset");
                    size = (int)currentElement.Attribute("size");
                    arraySize = (int)currentElement.Attribute("arraysize");
                    varStart = (int)currentElement.Attribute("varstart");
                    varEnd = (int)currentElement.Attribute("varend");
                    break;
                }
                currentNode = currentNode.NextNode;
            }

#if UNITY_ASSERTIONS
            Assert.CheckError(offset > -1 && size > 0, "Unknown property or dataDescriptor is corrupted. Offset = {0}, Size = {1}", offset, size);
#endif

            if (varStart > m_entry || varEnd < m_entry)
                return 0;

            int realOffset = (m_entry - varStart) * arraySize + offset;
            switch (size)
            {
                case 1:
                    return m_internalStream.ReadByte(realOffset);
                case 2:
                    return m_internalStream.ReadUInt16(realOffset);
                case 4:
                    return m_internalStream.ReadUInt32(realOffset);
                default:
#if UNITY_ASSERTIONS
                    Assert.CheckError(false, "Wrong size ({0})", size);
#endif
                    return -1;
            }
        }
    }
}
