﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Linq;
using System.IO;
using UnityEngine;

namespace StarcraftClone
{
    class DataProvider : Singleton<DataProvider>
    {
        Dictionary<XNode, ByteStreamReader> m_dataDictionary = null;
 
        DataProvider()
        {
            m_dataDictionary = new Dictionary<XNode, ByteStreamReader>();

            TextAsset data = Resources.Load("Xml/dataDescriptor") as TextAsset;
#if UNITY_ASSERTIONS
            Assert.CheckError(data != null, "Unable to open dataDescriptor.xml");
#endif

            XDocument xDoc = XDocument.Parse(data.text);
            xDoc.DescendantNodes().OfType<XComment>().Remove();
#if UNITY_ASSERTIONS
            Assert.CheckError(xDoc != null, "dataDescriptor.xml is corrupted");
#endif

            XNode currentNode = xDoc.Root.FirstNode;
            while (currentNode != null)
            {
                XElement currentElement = currentNode as XElement;
                string fileName = currentElement.Attribute("file").Value;
                ByteStreamReader byteStream = ByteStreamReader.CreateMemoryStream(Path.Arr + fileName);
                m_dataDictionary.Add(currentNode, byteStream);
                currentNode = currentNode.NextNode;
            }
        }

        public Data GetData(string name, int entry)
        {
            foreach (KeyValuePair<XNode, ByteStreamReader> pair in m_dataDictionary)
            {
                XElement key = pair.Key as XElement;
                if (key.Attribute("name").Value == name)
                {
#if UNITY_ASSERTIONS
                    int maxEntry = (int)key.Attribute("entryCount");
                    Assert.CheckError(entry < maxEntry, "Entry index exceeds max entry count. {0} >= {1}", entry, maxEntry);
#endif
                    return new Data(key.FirstNode, pair.Value, entry);
                }
            }
            return null;
        }
    }
}
