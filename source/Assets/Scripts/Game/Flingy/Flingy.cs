﻿using UnityEngine;
using StarcraftClone.AI.Steer;

namespace StarcraftClone
{
    class Flingy : MonoBehaviour, ISteerable
    {
        SteeringBehavior _steering;
        public SteeringBehavior Steering
        {
            get
            {
                return _steering;
            }
            set
            {
                _steering = value;
            }
        }

        public int direction;     // same with "orientation", but in game specific unit (0-31 clockwise)
        public int Direction
        {
            get
            {
                return direction;
            }
            set
            {
                direction = (value + 31) % 32;
                orientation = DirectionToOrientation(direction);
            }
        }

        int movingControl;
        Location destination;
        Sprite sprite;

        public static int OrientationToDirection(float orientation)
        {
            return (int)(31 - (orientation - Mathf.PI / 2) * 32 / (Mathf.PI * 2));
        }

        public static float DirectionToOrientation(int direction)
        {
            return (31 - direction) * Mathf.PI * 2 / 32 + Mathf.PI / 2;
        }

        public void Init(int index, string layer)
        {
            Data flingyData = DataProvider.Instance.GetData("flingy", index);

            int spriteId = flingyData.Get("sprite");
            sprite = GetComponent<Sprite>();
            if (sprite != null)
            {
                sprite.Init(spriteId, layer);
            }

            movingControl = flingyData.Get("moveControl");
            if (movingControl != 2)
            {
                maxLinearSpeed = flingyData.Get("topSpeed") * 3 / (float)(640 * (int) Dimension.PIXELS_PER_UNIT);
                maxLinearAcceleration = flingyData.Get("acceleration") * 3 / (float)(640 * (int)Dimension.PIXELS_PER_UNIT);
                turnRadius = flingyData.Get("turnRadius") / (float)Dimension.PIXELS_PER_UNIT;
            }

            decelerationRadius = flingyData.Get("haltDistance") / (float)(256 * (int)Dimension.PIXELS_PER_UNIT);
            _steering = SteeringBehavior.NullSteer;

            boxCollider = GetComponent<BoxCollider2D>();
        }

        /* The update function is used only for self control flingy(s) */
        void FixedUpdate()
        {
            if (movingControl != 2)
            {
                ApplySteering();
            }
        }

        /* The move function is used only for iscript control flingy(s) */
        public void Move(int distance)
        {
            if (movingControl == 2)
            {
                float worldDistance = distance / (float)Dimension.PIXELS_PER_UNIT;
                linearVelocity = linearVelocity.normalized * worldDistance;
                maxLinearSpeed = worldDistance;
                maxLinearAcceleration = worldDistance;
                ApplySteering();
                Gizmo.Draw("linearVelocity", new LineGeometry(transform.position, transform.position + linearVelocity * 10, Color.cyan));
            }
        }

        void ApplySteering()
        {
            SteeringAcceleration steeringAcceleration = _steering.CalculateSteering(SteeringAcceleration.Zero);
            linearVelocity = Vector3.ClampMagnitude(linearVelocity + steeringAcceleration.linear, maxLinearSpeed);
            if (linearVelocity.sqrMagnitude <= zeroLinearSpeedThreshold)
            {
                linearVelocity = Vector3.zero;
                sprite.PlayAnim(Animation.AnimationSet.WALKING_TO_IDLE);
                GetComponent<Unit>().Stop();
            }
            else
            {
                transform.position += linearVelocity;
                Orientation = VectorToAngle(linearVelocity);
                CheckOverlap();
            }
        }

        Collider2D[] overlapResults = new Collider2D[2];
        BoxCollider2D boxCollider;
        void CheckOverlap()
        {
            if (Physics2D.OverlapAreaNonAlloc(
                new Vector2(boxCollider.bounds.max.x, boxCollider.bounds.max.y),
                new Vector2(boxCollider.bounds.min.x, boxCollider.bounds.min.y),
                overlapResults) > 1)
                transform.position -= linearVelocity;
        }

        /* Exposed variable */
        /* ISteerable variable */
        public UnityEngine.Vector3 linearVelocity;
        public float angularVelocity;
        public float boundingRadius;
        public bool isTagged;
        public float zeroLinearSpeedThreshold;
        public float maxLinearSpeed;
        public float maxLinearAcceleration;
        public float maxAngularSpeed;
        public float maxAngularAcceleration;
        public float orientation;   // same with "direction" but in radians

        /* Game specific variable */
        public float decelerationRadius;
        public float turnRadius;


        public float DecelerationRadius
        {
            get
            {
                return decelerationRadius;
            }
        }

        public UnityEngine.Vector3 LinearVelocity
        {
            get
            {
                return linearVelocity;
            }
            set
            {
                linearVelocity = value;
            }
        }
        public float AngularVelocity
        {
            get
            {
                return angularVelocity;
            }
            set
            {
                angularVelocity = value;
            }
        }
        public float BoundingRadius
        {
            get
            {
                if (boundingRadius == 0)
                {
                    BoxCollider2D collider = GetComponent<BoxCollider2D>();
                    if (collider != null)
                    {
                        boundingRadius = Mathf.Max(collider.size.x, collider.size.y) / 2;
                    }
                }
                return boundingRadius;
            }
            set
            {
                boundingRadius = value;
            }
        }
        public bool IsTagged
        {
            get
            {
                return isTagged;
            }
            set
            {
                isTagged = value;
            }
        }

        public float ZeroLinearSpeedThreshold
        {
            get
            {
                return zeroLinearSpeedThreshold;
            }
            set
            {
                zeroLinearSpeedThreshold = value;
            }
        }
        public float MaxLinearSpeed
        {
            get
            {
                return maxLinearSpeed;
            }
            set
            {
                maxLinearSpeed = value;
            }
        }
        public float MaxLinearAcceleration
        {
            get
            {
                return maxLinearAcceleration;
            }
            set
            {
                maxLinearAcceleration = value;
            }
        }
        public float MaxAngularSpeed
        {
            get
            {
                return maxAngularSpeed;
            }
            set
            {
                maxAngularSpeed = value;
            }
        }
        public float MaxAngularAcceleration
        {
            get
            {
                return maxAngularAcceleration;
            }
            set
            {
                maxAngularAcceleration = value;
            }
        }

        public UnityEngine.Vector3 Position
        {
            get { return transform.position; }
        }

        public float Orientation
        {
            get
            {
                return orientation;
            }
            set
            {
                orientation = value;
                direction = (OrientationToDirection(orientation) + 31) % 32;
            }
        }

        public Vector3 AngleToVector(float angle)
        {
            return new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0.0f);
        }

        public float VectorToAngle(Vector3 vector)
        {
            return Mathf.Atan2(vector.y, vector.x);
        }
    }
}
