﻿using UnityEngine;
using StarcraftClone.AI.Steer;

namespace StarcraftClone
{
    class Location : ILocation
    {
        public Vector3 Position { get; set; }

        public float Orientation { get; set; }

        public float BoundingRadius { get; set; }

        public Location(Vector3 position, float orientation = 0.0f)
        {
            Position = position;
            Orientation = orientation;
        }

        public Vector3 AngleToVector(float angle)
        {
            return new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0.0f);
        }

        public float VectorToAngle(Vector3 vector)
        {
            return Mathf.Atan2(vector.y, vector.x);
        }
    }
}
