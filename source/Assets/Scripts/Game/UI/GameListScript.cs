﻿using UnityEngine;

namespace StarcraftClone
{
    class GameListScript : MonoBehaviour
    {
        private enum STATE
        {
            STATE_NONE,
            STATE_ENABLING,
            STATE_DISABLING
        }

        private STATE m_state = STATE.STATE_NONE;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            //		switch (m_state) {
            //		case STATE.STATE_ENABLING:
            //			{
            //			}
            //		case STATE.STATE_DISABLING:
            //			{
            //			}
            //		default:
            //			break;
            //		}
        }

        public void OnEnable()
        {
            if (!gameObject.activeSelf)
            {
                m_state = STATE.STATE_ENABLING;
                gameObject.SetActive(true);
            }
        }

        public void OnDisable()
        {
            if (gameObject.activeSelf)
            {
                m_state = STATE.STATE_DISABLING;
                gameObject.SetActive(false);
            }
        }
    }
}