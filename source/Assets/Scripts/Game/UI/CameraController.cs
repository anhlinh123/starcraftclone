﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    Camera mainCamera;

	void Start ()
	{
        mainCamera = GetComponent<Camera>();
	}
	
	void Update ()
	{
        float d = Input.GetAxis("Mouse ScrollWheel");
        if (d != 0f)
        {
            // scroll up
            mainCamera.orthographicSize += d;
        }

        float horizontal = Input.GetAxis("Horizontal") * Time.deltaTime * 5.0f;
        float vertical = Input.GetAxis("Vertical") * Time.deltaTime * 5.0f;

        transform.position += new Vector3(horizontal, vertical, 0.0f);
	}
}
