﻿using UnityEngine;

public class WelcomeScreenScript : MonoBehaviour
{
	// Use this for initialization
	void Start ()
	{
		RectTransform thisRt = gameObject.GetComponent<RectTransform> ();
		RectTransform parentRt = gameObject.transform.parent.gameObject.GetComponentInParent<RectTransform> ();
		
		if (thisRt.rect.height < parentRt.rect.height) {
			float fScale = parentRt.rect.height / thisRt.rect.height;
			thisRt.sizeDelta = new Vector2 (thisRt.rect.width * fScale, parentRt.rect.height);
		}
	}
}
