﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace StarcraftClone
{
    public class CommandInputController : MonoBehaviour
    {
        InputField m_inputField;

        void Start()
        {
            m_inputField = GetComponent<InputField>();
        }

        public void OnEndEdit(String command)
        {
            m_inputField.text = "";
        }
    }
}