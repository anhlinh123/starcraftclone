﻿using UnityEngine;
using UnityEngine.UI;

namespace StarcraftClone
{
    public class LoadingTextScript : MonoBehaviour
    {
        private Text m_text;
        private float m_deltaTime = 0.0f;

        void Start()
        {
            m_text = gameObject.GetComponent<Text>();
        }

        // Update is called once per frame
        void Update()
        {
            if (m_deltaTime >= 0.5f)
            {
                m_text.enabled = !m_text.enabled;
                m_deltaTime = 0.0f;
            }
            else
            {
                m_deltaTime += Time.deltaTime;
            }
        }
    }
}