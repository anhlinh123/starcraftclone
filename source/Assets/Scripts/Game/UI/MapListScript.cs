using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.UI;
using System.IO;

namespace StarcraftClone
{
    class MapListScript : MonoBehaviour
    {
        public Text m_mapName;
        public UnityAction selectMapCallback;

        private Text[] m_maps;
        private Text m_selection = null;

        // Use this for initialization
        void Start()
        {
            DirectoryInfo mapDir = new DirectoryInfo(Path.Map);
            FileInfo[] files = mapDir.GetFiles("*.*");
            m_maps = new Text[files.Length];

            for (int i = 0; i < files.Length; i++)
            {
                Text map = m_maps[i];
                map = Instantiate(m_mapName);
                map.text = files[i].Name;
                map.gameObject.transform.position = new Vector3(
                    map.transform.position.x,
                    -(i * 50 + 80),
                    0
                );
                Button btn = map.gameObject.GetComponent<Button>();
                btn.onClick.AddListener(() => OnSelect(map));
                map.transform.SetParent(gameObject.transform, false);
            }
        }

        // Update is called once per frame
        void Update()
        {
        }

        public void OnEnable()
        {
            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }
        }

        public void OnDisable()
        {
            if (gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }
        }

        public void OnSelect(Text map)
        {
            if (m_selection != null)
                m_selection.color = m_mapName.color;
            m_selection = map;
            m_selection.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

            if (selectMapCallback != null)
                selectMapCallback();
        }

        public string GetMapName()
        {
            return m_selection.text;
        }
    }
}