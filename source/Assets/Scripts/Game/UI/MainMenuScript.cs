﻿using UnityEngine;
using UnityEngine.UI;

namespace StarcraftClone
{
    class MainMenuScript : MonoBehaviour
    {
        public MapListScript mapList;
        public GameListScript gameList;
        public Button startGameButton;
        public Button createGameButton;
        public Button joinGameButton;
        public Lobby lobby;
    }
}