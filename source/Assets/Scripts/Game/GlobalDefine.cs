﻿namespace StarcraftClone
{
    enum Dimension
    {
        PIXELS_PER_UNIT = 128
    }

    static class SortingLayer
    {
        public const int DEFAULT = 3;
        public const int SELECTION = 2;
        public const int UNIT = 1;
        public const int BULLET = 0;

        public static readonly string[] LayerName =
        {
            "Bullet",
            "Unit",
            "Selection",
            "Default"
        };
    }

    static class GameProperties
    {
        public const int PALETTE_CYCLING_INTERVAL = 3;
    }
}
