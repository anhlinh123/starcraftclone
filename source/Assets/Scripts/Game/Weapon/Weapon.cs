﻿namespace StarcraftClone
{
    class Weapon
    {
        Unit m_unit = null;
        int m_damage = 0;
        int m_bonusPerUpgrade = 0;
        int m_coolDown = 0;
        int m_minRange = 0;
        int m_maxRange = 0;
        int m_graphics = 0;
        int m_behavior = 0;
        int m_currentCoolDown = 0;

        public Weapon(int id, Unit unit)
        {
            m_unit = unit;

            Data weaponData = DataProvider.Instance.GetData("weapons", id);
            m_damage = weaponData.Get("damage");
            m_bonusPerUpgrade = weaponData.Get("bonus");
            m_coolDown = weaponData.Get("coolDown");
            m_minRange = weaponData.Get("minRange");
            m_maxRange = weaponData.Get("maxRange");
            m_graphics = weaponData.Get("graphics");
            m_behavior = weaponData.Get("behavior");
        }

        public bool Attack(Unit target)
        {
            if (IsTargetInAttackRange(target))
            {
                LaunchBulletTo(target);
                m_currentCoolDown = m_coolDown;
                return true;
            }

            return false;
        }

        public bool AttackMelee(Unit target)
        {
            if (IsTargetInAttackRange(target))
            {
                DoDamage(target);
                m_currentCoolDown = m_coolDown;
                return true;
            }

            return false;
        }

        public bool IsTargetInAttackRange(Unit target)
        {
            UnityEngine.Vector2 unitPos = m_unit.GetWorldPosition();
            UnityEngine.Vector2 targetPos = target.GetWorldPosition();

            int distance = (int)(targetPos - unitPos).magnitude;
            return distance >= m_minRange && distance <= m_maxRange;
        }

        public void DoDamage(Unit target)
        {
            target.ReceiveDamage(m_damage);
        }

        public void Update()
        {
            m_currentCoolDown = UnityEngine.Mathf.Clamp(m_currentCoolDown - 1, 0, m_coolDown);
        }

        public bool IsWeaponCoolDown()
        {
            return m_currentCoolDown <= 0;
        }

        void LaunchBulletTo(Unit target)
        {
            Bullet bullet = Bullet.CreateBullet(m_unit.transform.position, m_behavior);
            if (bullet != null)
            {
                bullet.Init(m_graphics, this);
                bullet.SetTarget(target);
            }
        }
    }
}
