﻿namespace StarcraftClone
{
    class Bullet_AppearOnTargetUnit : Bullet
    {
        Unit m_target = null;

        public override void SetTarget(Unit target)
        {
            m_target = target;
            transform.position = target.transform.position;
        }

        public override void OnEvent(AnimationEvent animEvent)
        {
            switch (animEvent.Event)
            {
                case EventType.DO_MISSILE_DAMAGE:
                    {
                        m_launcher.DoDamage(m_target);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
