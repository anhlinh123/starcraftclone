﻿namespace StarcraftClone
{
    class Bullet_FlyAndFollowTarget : Bullet
    {
        Unit m_target = null;
        bool m_didHitTarget = false;

        public override void SetTarget(Unit target)
        {
            m_target = target;
        }

        public override void OnEvent(AnimationEvent animEvent)
        {
            switch (animEvent.Event)
            {
                case EventType.DO_MISSILE_DAMAGE:
                    {
                        m_launcher.DoDamage(m_target);
                    }
                    break;
                default:
                    break;
            }
        }

        void Update()
        {
            /*
            if (m_didHitTarget == false && m_flingy.Update() == true)
            {
                m_didHitTarget = true;
                m_sprite.PlayAnim(Animation.AnimationSet.DEATH);
            }*/
        }
    }
}
