﻿namespace StarcraftClone
{
    class Bullet_AppearOnAttacker : Bullet
    {
        Unit m_target = null;

        public override void SetTarget(Unit target)
        {
            m_target = target;
        }

        public override void OnEvent(AnimationEvent animEvent)
        {
            base.OnEvent(animEvent);
        }
    }
}
