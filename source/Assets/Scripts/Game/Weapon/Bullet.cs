﻿using UnityEngine;

namespace StarcraftClone
{ 
    abstract class Bullet : MonoBehaviour, IAnimationEventListener
    {
        protected Weapon m_launcher = null;
        protected Sprite m_sprite = null;
        protected Flingy m_flingy = null;

        public static Bullet CreateBullet(Vector3 position, int behavior)
        {
            GameObject bulletObject = new GameObject("Bullet");
            bulletObject.transform.position = position;

            switch (behavior)
            {
                case 0:
                    bulletObject.AddComponent<Bullet_FlyAndNotFollowTarget>();
                    break;
                case 1:
                    bulletObject.AddComponent<Bullet_FlyAndFollowTarget>();
                    break;
                case 2:
                    bulletObject.AddComponent<Bullet_AppearOnTargetUnit>();
                    break;
                case 3:
                    bulletObject.AddComponent<Bullet_PersistOnTargetSite>();
                    break;
                case 4:
                    bulletObject.AddComponent<Bullet_AppearOnTargetSite>();
                    break;
                case 5:
                    bulletObject.AddComponent<Bullet_AppearOnAttacker>();
                    break;
                case 6:
                    bulletObject.AddComponent<Bullet_AppearAndSelfDestruct>();
                    break;
                case 7:
                    bulletObject.AddComponent<Bullet_Bounce>();
                    break;
                case 8:
                    bulletObject.AddComponent<Bullet_AttackTarget3x3Area>();
                    break;
                case 9:
                    bulletObject.AddComponent<Bullet_GoToMaxRange>();
                    break;
                default:
                    break;
            }

            return bulletObject.GetComponent<Bullet>();
        }

        abstract public void SetTarget(Unit target);
        public virtual void OnEvent(AnimationEvent animEvent)
        {
            switch (animEvent.Event)
            {
                default:
                    break;
            }
        }

        public void Init(int graphicID, Weapon launcher)
        {
            m_launcher = launcher;
            m_flingy = GetComponent<Flingy>();
            m_flingy.Init(graphicID, "Bullet");
            m_sprite = GetComponent<Sprite>();
            m_sprite.AddAnimationListener(this);
        }
    }
}
