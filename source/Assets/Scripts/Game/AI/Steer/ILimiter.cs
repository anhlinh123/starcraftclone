namespace StarcraftClone.AI.Steer
{
    /** A Limiter provides the maximum magnitudes of speed and acceleration for both linear and angular components. */
    public interface ILimiter
    {
        float ZeroLinearSpeedThreshold { get; set; }

        float MaxLinearSpeed { get; set; }

        float MaxLinearAcceleration { get; set; }

        float MaxAngularSpeed { get; set; }

        float MaxAngularAcceleration { get; set; }
    }
}