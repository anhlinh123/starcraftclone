﻿namespace StarcraftClone.AI.Steer
{
    /** The callback object used by a proximity to report the owner's neighbor. */
    public interface IProximityCallback
    {

        /** The callback method used to report a neighbor.
         * @param neighbor the reported neighbor.
         * @return {@code true} if the given neighbor is valid; {@code false} otherwise. */
        bool ReportNeighbor(ISteerable neighbor);

    }
}
