namespace StarcraftClone.AI.Steer
{

    /** A SteeringBehavior calculates the linear and/or angular accelerations to be applied to its owner. */
    public abstract class SteeringBehavior
    {
        static SteeringBehavior _nullSteer = new NullSteering();
        public static SteeringBehavior NullSteer
        {
            get
            {
                return _nullSteer;
            }
        }

        /** The owner of this steering behavior */
        public ISteerable Owner { get; set; }

        /** The limiter of this steering behavior */
        public ILimiter Limiter { get; set; }

        /** A flag indicating whether this steering behavior is enabled or not. */
        public bool IsEnabled { get; set; }

        /** Creates a {@code SteeringBehavior} for the specified owner. The behavior is enabled and has no explicit limiter, meaning
         * that the owner is used instead.
         * 
         * @param owner the owner of this steering behavior */
        public SteeringBehavior(ISteerable owner)
            : this(owner, null, true)
        {
        }

        /** Creates a {@code SteeringBehavior} for the specified owner and limiter. The behavior is enabled.
         * 
         * @param owner the owner of this steering behavior
         * @param limiter the limiter of this steering behavior */
        public SteeringBehavior(ISteerable owner, ILimiter limiter)
            : this(owner, limiter, true)
        {
        }

        /** Creates a {@code SteeringBehavior} for the specified owner and activation flag. The behavior has no explicit limiter,
         * meaning that the owner is used instead.
         * 
         * @param owner the owner of this steering behavior
         * @param enabled a flag indicating whether this steering behavior is enabled or not */
        public SteeringBehavior(ISteerable owner, bool enabled)
            : this(owner, null, enabled)
        {
        }

        /** Creates a {@code SteeringBehavior} for the specified owner, limiter and activation flag.
         * 
         * @param owner the owner of this steering behavior
         * @param limiter the limiter of this steering behavior
         * @param enabled a flag indicating whether this steering behavior is enabled or not */
        public SteeringBehavior(ISteerable owner, ILimiter limiter, bool isEnabled)
        {
            Owner = owner;
            Limiter = limiter;
            IsEnabled = isEnabled;
        }

        /** If this behavior is enabled calculates the steering acceleration. If it is
         * disabled the steering output is set to zero. */
        public SteeringAcceleration CalculateSteering(SteeringAcceleration steering)
        {
            return IsEnabled ? CalculateRealSteering(steering) : steering.SetZero();
        }

        protected abstract SteeringAcceleration CalculateRealSteering(SteeringAcceleration steering);

        /** Returns the actual limiter of this steering behavior. */
        protected ILimiter GetActualLimiter()
        {
            return Limiter == null ? Owner : Limiter;
        }

        class NullSteering : SteeringBehavior
        {
            public NullSteering() : base(null) { }

            protected override SteeringAcceleration CalculateRealSteering(SteeringAcceleration steering)
            {
                return SteeringAcceleration.Zero;
            }
        }
    }
}