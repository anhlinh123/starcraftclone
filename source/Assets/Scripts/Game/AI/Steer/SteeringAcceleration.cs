using UnityEngine;

namespace StarcraftClone.AI.Steer
{

    /** {@code SteeringAcceleration} is a movement requested by the steering system. It is made up of two components, linear and angular
     * acceleration.
     * 
     * @param <T> Type of vector, either 2D or 3D, implementing the {@link Vector} interface
     * 
     * @author davebaol */
    public struct SteeringAcceleration
    {
        /** The linear component of this steering acceleration. */
        public Vector3 linear;

        /** The angular component of this steering acceleration. */
        public float angular;

        /** Returns {@code true} if both linear and angular components of this steering acceleration are zero; {@code false} otherwise. */
        public bool IsZero
        {
            get
            {
                return angular == 0f && linear == Vector3.zero;
            }
        }

        public static SteeringAcceleration Zero
        {
            get
            {
                return new SteeringAcceleration(Vector3.zero, 0f);
            }
        }

        /** Returns the square of the magnitude of this steering acceleration. This includes the angular component. */
        public float SquareMagnitude
        {
            get
            {
                return linear.sqrMagnitude + angular * angular;
            }
        }

        /** Returns the magnitude of this steering acceleration. This includes the angular component. */
        public float Magnitude
        {
            get
            {
                return Mathf.Sqrt(SquareMagnitude);
            }
        }

        /** Creates a {@code SteeringAcceleration} with the given linear acceleration and zero angular acceleration.
         * 
         * @param linear The initial linear acceleration to give this SteeringAcceleration. */
        public SteeringAcceleration(Vector3 linear)
            : this(linear, 0f)
        {
        }

        /** Creates a {@code SteeringAcceleration} with the given linear and angular components.
         * 
         * @param linear The initial linear acceleration to give this SteeringAcceleration.
         * @param angular The initial angular acceleration to give this SteeringAcceleration. */
        public SteeringAcceleration(Vector3 linear, float angular)
        {
            this.linear = linear;
            this.angular = angular;
        }

        /** Zeros the linear and angular components of this steering acceleration.
         * @return this steering acceleration for chaining */
        public SteeringAcceleration SetZero()
        {
            linear.Set(0f, 0f, 0f);
            angular = 0f;
            return this;
        }

        /** Adds the given steering acceleration to this steering acceleration.
         * 
         * @param steering the steering acceleration
         * @return this steering acceleration for chaining */
        public static SteeringAcceleration operator + (SteeringAcceleration steering1, SteeringAcceleration steering2)
        {
            return new SteeringAcceleration(steering1.linear + steering2.linear, steering1.angular + steering2.angular);
        }

        /** Scales this steering acceleration by the specified scalar.
         * 
         * @param scalar the scalar
         * @return this steering acceleration for chaining */
        public static SteeringAcceleration operator * (SteeringAcceleration steering, float scalar)
        {
            return new SteeringAcceleration(steering.linear * scalar, steering.angular * scalar);
        }

        /** First scale a supplied steering acceleration, then add it to this steering acceleration.
         * 
         * @param steering the steering acceleration
         * @param scalar the scalar
         * @return this steering acceleration for chaining */
        public SteeringAcceleration MulAdd(SteeringAcceleration steering, float scalar)
        {
            linear += steering.linear * scalar;
            angular += steering.angular * scalar;
            return this;
        }
    }
}