namespace StarcraftClone.AI.Steer
{
    /** A {@code LinearSpeedLimiter} provides the maximum magnitudes of linear speed. All other methods throw an
     * {@link UnsupportedOperationException}.
     * 
     * @author davebaol */
    public class LinearSpeedLimiter : NullLimiter
    {
        public override float MaxLinearSpeed { get; set; }

        /** Creates a {@code LinearSpeedLimiter}.
         * @param maxLinearSpeed the maximum linear speed */
        public LinearSpeedLimiter(float maxLinearSpeed)
        {
            MaxLinearSpeed = maxLinearSpeed;
        }
    }
}