namespace StarcraftClone.AI.Steer
{
    /** A {@code FullLimiter} provides the maximum magnitudes of speed and acceleration for both linear and angular components.
     * 
     * @author davebaol */
    public class FullLimiter : ILimiter
    {

        public float MaxLinearAcceleration { get; set; }
        public float MaxLinearSpeed { get; set; }
        public float MaxAngularAcceleration { get; set; }
        public float MaxAngularSpeed { get; set; }
        public float ZeroLinearSpeedThreshold { get; set; }

        /** Creates a {@code FullLimiter}.
         * @param maxLinearAcceleration the maximum linear acceleration
         * @param maxLinearSpeed the maximum linear speed
         * @param maxAngularAcceleration the maximum angular acceleration
         * @param maxAngularSpeed the maximum angular speed */
        public FullLimiter(float maxLinearAcceleration, float maxLinearSpeed, float maxAngularAcceleration, float maxAngularSpeed)
        {
            MaxLinearAcceleration = maxLinearAcceleration;
            MaxLinearSpeed = maxLinearSpeed;
            MaxAngularAcceleration = maxAngularAcceleration;
            MaxAngularSpeed = maxAngularSpeed;
        }
    }
}