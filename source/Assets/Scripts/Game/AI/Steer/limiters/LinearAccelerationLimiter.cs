namespace StarcraftClone.AI.Steer
{
    /** A {@code LinearAccelerationLimiter} provides the maximum magnitude of linear acceleration. All other methods throw an
     * {@link UnsupportedOperationException}.
     * 
     * @author davebaol */
    public class LinearAccelerationLimiter : NullLimiter
    {
        public override float MaxLinearAcceleration { get; set; }

        /** Creates a {@code LinearAccelerationLimiter}.
         * @param maxLinearAcceleration the maximum linear acceleration */
        public LinearAccelerationLimiter(float maxLinearAcceleration)
        {
            MaxLinearAcceleration = maxLinearAcceleration;
        }
    }
}
