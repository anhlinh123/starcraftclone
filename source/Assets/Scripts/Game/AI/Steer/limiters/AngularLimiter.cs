namespace StarcraftClone.AI.Steer
{
    /** An {@code AngularLimiter} provides the maximum magnitudes of angular speed and angular acceleration. Linear methods throw an
     * {@link UnsupportedOperationException}.
     * 
     * @author davebaol */
    public class AngularLimiter : NullLimiter
    {

        public override float MaxAngularAcceleration { get; set; }
        public override float MaxAngularSpeed { get; set; }

        /** Creates an {@code AngularLimiter}.
         * @param maxAngularAcceleration the maximum angular acceleration
         * @param maxAngularSpeed the maximum angular speed */
        public AngularLimiter(float maxAngularAcceleration, float maxAngularSpeed)
        {
            MaxAngularAcceleration = maxAngularAcceleration;
            MaxAngularSpeed = maxAngularSpeed;
        }
    }
}