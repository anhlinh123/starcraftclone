namespace StarcraftClone.AI.Steer
{
    /** An {@code AngularSpeedLimiter} provides the maximum magnitudes of angular speed. All other methods throw an
     * {@link UnsupportedOperationException}.
     * 
     * @author davebaol */
    public class AngularSpeedLimiter : NullLimiter
    {

        public override float MaxAngularSpeed { get; set; }

        /** Creates an {@code AngularSpeedLimiter}.
         * @param maxAngularSpeed the maximum angular speed */
        public AngularSpeedLimiter(float maxAngularSpeed)
        {
            MaxAngularSpeed = maxAngularSpeed;
        }

    }
}