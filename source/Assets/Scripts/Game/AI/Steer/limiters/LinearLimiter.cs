namespace StarcraftClone.AI.Steer
{
    /** A {@code LinearLimiter} provides the maximum magnitudes of linear speed and linear acceleration. Angular methods throw an
     * {@link UnsupportedOperationException}.
     * 
     * @author davebaol */
    public class LinearLimiter : NullLimiter
    {
        public override float MaxLinearAcceleration { get; set; }
        public override float MaxLinearSpeed { get; set; }

        /** Creates a {@code LinearLimiter}.
         * @param maxLinearAcceleration the maximum linear acceleration
         * @param maxLinearSpeed the maximum linear speed */
        public LinearLimiter(float maxLinearAcceleration, float maxLinearSpeed)
        {
            MaxLinearAcceleration = maxLinearAcceleration;
            MaxLinearSpeed = maxLinearSpeed;
        }
    }
}