using System;

namespace StarcraftClone.AI.Steer
{

    /** A {@code NullLimiter} always throws {@link UnsupportedOperationException}. Typically it's used as the base class of partial or
     * immutable limiters.
     * 
     * @author davebaol */
    public class NullLimiter : ILimiter
    {

        /** An immutable limiter whose getters return {@link Float#POSITIVE_INFINITY} and setters throw
         * {@link UnsupportedOperationException}. */
        class NeutralNullLimiter : NullLimiter
        {

            public override float MaxLinearSpeed
            {
                get
                {
                    return float.PositiveInfinity;
                }
            }

            public override float MaxLinearAcceleration
            {
                get
                {
                    return float.PositiveInfinity;
                }
            }

            public override float MaxAngularSpeed
            {
                get
                {
                    return float.PositiveInfinity;
                }
            }

            public override float MaxAngularAcceleration
            {
                get
                {
                    return float.PositiveInfinity;
                }
            }
        }

        private static NeutralNullLimiter _neutralLimiter = new NeutralNullLimiter();
        public static NullLimiter NeutralLimiter
        {
            get
            {
                return _neutralLimiter;
            }
        }

        /** Creates a {@code NullLimiter}. */
        public NullLimiter()
        {
        }

        public virtual float MaxLinearSpeed
        {
            get
            {
                throw new InvalidOperationException();
            }
            set
            {
                throw new InvalidOperationException();
            }
        }

        public virtual float MaxLinearAcceleration
        {
            get
            {
                throw new InvalidOperationException();
            }
            set
            {
                throw new InvalidOperationException();
            }
        }

        public virtual float MaxAngularSpeed
        {
            get
            {
                throw new InvalidOperationException();
            }
            set
            {
                throw new InvalidOperationException();
            }
        }

        public virtual float MaxAngularAcceleration
        {
            get
            {
                throw new InvalidOperationException();
            }
            set
            {
                throw new InvalidOperationException();
            }
        }

        public virtual float ZeroLinearSpeedThreshold
        {
            get
            {
                throw new InvalidOperationException();
            }
            set
            {
                throw new InvalidOperationException();
            }
        }
    }
}