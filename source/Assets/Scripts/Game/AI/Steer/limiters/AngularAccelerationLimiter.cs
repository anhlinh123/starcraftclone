namespace StarcraftClone.AI.Steer
{
    /** An {@code AngularAccelerationLimiter} provides the maximum magnitude of angular acceleration. All other methods throw an
     * {@link UnsupportedOperationException}.
     * 
     * @author davebaol */
    public class AngularAccelerationLimiter : NullLimiter
    {
        /** Returns the maximum angular acceleration. */
        public override float MaxAngularAcceleration { get; set; }

        /** Creates an {@code AngularAccelerationLimiter}.
         * @param maxAngularAcceleration the maximum angular acceleration */
        public AngularAccelerationLimiter(float maxAngularAcceleration)
        {
            MaxAngularAcceleration = maxAngularAcceleration;
        }
    }
}