using UnityEngine;

namespace StarcraftClone.AI.Steer
{
    /** An adapter class for Steerable. You can derive from this and only override what you are interested in. For example,
     * this comes in handy when you have to create on the fly a target for a particular behavior. */
    public class DefaultSteerable : ISteerable
    {
        public float ZeroLinearSpeedThreshold
        {
            get { return 0.001f; }
            set { }
        }

        public float MaxLinearSpeed
        {
            get { return 0; }
            set { }
        }

        public float MaxLinearAcceleration
        {
            get { return 0; }
            set { }
        }

        public float MaxAngularSpeed
        {
            get { return 0; }
            set { }
        }

        public float MaxAngularAcceleration
        {
            get { return 0; }
            set { }
        }

        public Vector3 Position
        {
            get { return Vector3.zero; }
        }

        public float Orientation
        {
            get { return 0; }
            set { }
        }

        public Vector3 LinearVelocity
        {
            get { return Vector3.zero; }
        }

        public float AngularVelocity
        {
            get { return 0; }
        }

        public float BoundingRadius
        {
            get { return 0; }
        }

        public bool IsTagged
        {
            get { return false; }
            set { }
        }

        public Vector3 AngleToVector(float angle)
        {
            return Vector3.zero;
        }

        public float VectorToAngle(Vector3 vector)
        {
            return 0.0f;
        }
    }
}