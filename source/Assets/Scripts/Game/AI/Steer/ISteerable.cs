using UnityEngine;

namespace StarcraftClone.AI.Steer
{
    /** A {@code Steerable} is a {@link Location} that gives access to the character's data required by steering system.
     * <p>
     * Notice that there is nothing to connect the direction that a Steerable is moving and the direction it is facing. For
     * instance, a character can be oriented along the x-axis but be traveling directly along the y-axis.
     * 
     * @param <T> Type of vector, either 2D or 3D, implementing the {@link Vector} interface
     * 
     * @author davebaol */
    public interface ISteerable : ILocation, ILimiter
    {
        /** Returns the vector indicating the linear velocity of this Steerable. */
        Vector3 LinearVelocity { get; }

        /** Returns the float value indicating the the angular velocity in radians of this Steerable. */
        float AngularVelocity { get; }

        /** Returns the bounding radius of this Steerable. */
        float BoundingRadius { get; }

        /** Returns {@code true} if this Steerable is tagged; {@code false} otherwise. */
        bool IsTagged { get; set; }
    }
}