using System.Collections.Generic;

namespace StarcraftClone.AI.Steer
{

    /** ProximityBase is the base class for any concrete proximity based on an iterable collection of agents. */
    public abstract class ProximityBase : IProximity
    {
        /** The owner of  this proximity. */
        public ISteerable Owner { get; set; }

        /** The collection of the agents handled by this proximity. */
        protected List<ISteerable> Agents { get; set; }

        /** Creates a {@code ProximityBase} for the specified owner and list of agents.
         * @param owner the owner of this proximity
         * @param agents the list of agents */
        public ProximityBase(ISteerable owner, List<ISteerable> agents)
        {
            Owner = owner;
            Agents = agents;
        }

        public abstract int FindNeighbors(IProximityCallback callback);
    }
}