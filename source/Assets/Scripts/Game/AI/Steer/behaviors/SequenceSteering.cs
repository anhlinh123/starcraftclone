﻿using System.Collections.Generic;
using UnityEngine;

namespace StarcraftClone.AI.Steer
{
    class SequenceSteering : SteeringBehavior
    {
        private List<SteeringBehavior> behaviors;

        public SequenceSteering(ISteerable owner)
            : base(owner)
        {
            behaviors = new List<SteeringBehavior>();
        }

        public void Add(SteeringBehavior behavior, float weight = 1.0f)
        {
            behavior.Owner = Owner;
            behaviors.Add(behavior);
        }

        protected override SteeringAcceleration CalculateRealSteering(SteeringAcceleration steering)
        {
            steering.SetZero();

            int len = behaviors.Count;
            for (int i = 0; i < len; i++)
            {
                steering = behaviors[i].CalculateSteering(steering);
            }

            ILimiter actualLimiter = GetActualLimiter();

            steering.linear = Vector3.ClampMagnitude(steering.linear, actualLimiter.MaxLinearAcceleration);
            if (steering.angular > actualLimiter.MaxAngularAcceleration)
                steering.angular = actualLimiter.MaxAngularAcceleration;

            return steering;
        }
    }
}
