using UnityEngine;

namespace StarcraftClone.AI.Steer
{
    /** {@code Seek} behavior moves the owner towards the target position. Given a target, this behavior calculates the linear steering
     * acceleration which will direct the agent towards the target as fast as possible.
     * 
     * @param <T> Type of vector, either 2D or 3D, implementing the {@link Vector} interface
     * 
     * @author davebaol */
    public class Seek : SteeringBehavior
    {

        /** The target to seek */
        private ILocation _target;
        public ILocation Target
        {
            get
            {
                return _target;
            }
            set
            {
                _target = value;
            }
        }

        /** Creates a {@code Seek} behavior for the specified owner.
         * @param owner the owner of this behavior. */
        public Seek(ISteerable owner)
            : this(owner, null)
        {
        }

        /** Creates a {@code Seek} behavior for the specified owner and target.
         * @param owner the owner of this behavior
         * @param target the target agent of this behavior. */
        public Seek(ISteerable owner, ILocation target)
            : base(owner)
        {
            _target = target;
        }

        protected override SteeringAcceleration CalculateRealSteering(SteeringAcceleration steering)
        {
            // Try to match the position of the character with the position of the target by calculating
            // the direction to the target and by moving toward it as fast as possible.
            Vector3 linear = (_target.Position - Owner.Position).normalized * GetActualLimiter().MaxLinearAcceleration;

            // No angular acceleration
            // Output steering acceleration
            return new SteeringAcceleration(linear, 0f);
        }
    }
}