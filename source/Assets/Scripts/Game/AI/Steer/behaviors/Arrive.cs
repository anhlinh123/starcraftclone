using UnityEngine;

namespace StarcraftClone.AI.Steer
{
    /** {@code Arrive} behavior moves the agent towards a target position. It is similar to seek but it attempts to arrive at the target
     * position with a zero velocity.
     * <p>
     * {@code Arrive} behavior uses two radii. The {@code arrivalTolerance} lets the owner get near enough to the target without
     * letting small errors keep it in motion. The {@code decelerationRadius}, usually much larger than the previous one, specifies
     * when the incoming character will begin to slow down. The algorithm calculates an ideal speed for the owner. At the slowing-down
     * radius, this is equal to its maximum linear speed. At the target point, it is zero (we want to have zero speed when we arrive).
     * In between, the desired speed is an interpolated intermediate value, controlled by the distance from the target.
     * <p>
     * The direction toward the target is calculated and combined with the desired speed to give a target velocity. The algorithm
     * looks at the current velocity of the character and works out the acceleration needed to turn it into the target velocity. We
     * can't immediately change velocity, however, so the acceleration is calculated based on reaching the target velocity in a fixed
     * time scale known as {@code timeToTarget}. This is usually a small value; it defaults to 0.1 seconds which is a good starting
     * point.
     * 
     * @param <T> Type of vector, either 2D or 3D, implementing the {@link Vector} interface
     * 
     * @author davebaol */
    public class Arrive : SteeringBehavior
    {

        /** The target to arrive to. */
        private ILocation _target;
        public ILocation Target
        {
            get
            {
                return _target;
            }
            set
            {
                _target = value;
            }
        }

        /** The tolerance for arriving at the target. It lets the owner get near enough to the target without letting small errors keep
         * it in motion. */
        private float _arrivalTolerance;
        public float ArrivalTolerance
        {
            get
            {
                return _arrivalTolerance;
            }
            set
            {
                _arrivalTolerance = value;
            }
        }

        /** The radius for beginning to slow down */
        private float _decelerationRadius;
        public float DecelerationRadius
        {
            get
            {
                return _decelerationRadius;
            }
            set
            {
                _decelerationRadius = value;
            }
        }

        ///** The time over which to achieve target speed */
        //private float _timeToTarget = 0.1f;
        //public float TimeToTarget
        //{
        //    get
        //    {
        //        return _timeToTarget;
        //    }
        //    set
        //    {
        //        _timeToTarget = value;
        //    }
        //}

        /** Creates an {@code Arrive} behavior for the specified owner.
         * @param owner the owner of this behavior */
        public Arrive(ISteerable owner) : base(owner, null)
        {
        }

        /** Creates an {@code Arrive} behavior for the specified owner and target.
         * @param owner the owner of this behavior
         * @param target the target of this behavior */
        public Arrive(ISteerable owner, ILocation target) : base(owner)
        {
            this._target = target;
        }

        protected override SteeringAcceleration CalculateRealSteering(SteeringAcceleration steering)
        {
            if (_target != null)
                return arrive(_target.Position);
            else
                return SteeringAcceleration.Zero;
        }

        protected SteeringAcceleration arrive(Vector3 targetPosition)
        {
            // Get the direction and distance to the target
            Vector3 toTarget = targetPosition - Owner.Position;
            float distance = toTarget.magnitude - Owner.BoundingRadius - _target.BoundingRadius;

            ILimiter actualLimiter = GetActualLimiter();

            // Check if we are there, return no steering
            if (distance <= _arrivalTolerance)
            {
                toTarget = Vector3.zero - Owner.LinearVelocity;
            }
            else
            {
                // Go max speed
                float targetSpeed = actualLimiter.MaxLinearSpeed;

                // If we are inside the slow down radius calculate a scaled speed
                if (distance <= _decelerationRadius) targetSpeed *= distance / _decelerationRadius;

                // Target velocity combines speed and direction
                toTarget *= (targetSpeed / distance); // Optimized code for: toTarget.nor().scl(targetSpeed)

                // Acceleration tries to get to the target velocity without exceeding max acceleration
                // Notice that steering.linear and targetVelocity are the same vector
                toTarget = Vector3.ClampMagnitude((toTarget - Owner.LinearVelocity) /* * (1f / _timeToTarget)*/, actualLimiter.MaxLinearAcceleration);
            }

            // No angular acceleration
            SteeringAcceleration steering = new SteeringAcceleration(toTarget, 0f);

            // Debug
            Gizmo.Draw("Arrive", new LineGeometry(Owner.Position, Owner.Position + toTarget * 10, Color.red));

            // Output the steering
            return steering;
        }
    }
}