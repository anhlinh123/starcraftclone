using UnityEngine;

namespace StarcraftClone.AI.Steer
{
    /** {@code Face} behavior makes the owner look at its target immediately. */
    public class Face : SteeringBehavior
    {
        ILocation _target;
        public ILocation Target
        {
            get
            {
                return _target;
            }
            set
            {
                _target = value;
            }
        }

        public Face(ISteerable owner)
            : this(owner, null)
        {
        }

        public Face(ISteerable owner, ILocation target)
            : base(owner)
        {
            _target = target;
        }

        protected override SteeringAcceleration CalculateRealSteering(SteeringAcceleration steering)
        {
            // Get the direction to target
            Vector3 toTarget = _target.Position - Owner.Position;

            // Calculate the orientation to face the target
            Owner.Orientation = Owner.VectorToAngle(toTarget);

            return SteeringAcceleration.Zero;
        }
    }
}