using UnityEngine;
using System.Collections.Generic;

namespace StarcraftClone.AI.Steer
{
    /** This combination behavior simply sums up all the behaviors, applies their weights, and truncates the result before returning. */
    public class BlendedSteering : SteeringBehavior
    {

        /** The list of behaviors and their corresponding blending weights. */
        private List<SteeringBehavior> behaviors;
        private List<float> weights;

        /** Creates a {@code BlendedSteering} for the specified {@code owner}, {@code maxLinearAcceleration} and
         * {@code maxAngularAcceleration}.
         * @param owner the owner of this behavior. */
        public BlendedSteering(ISteerable owner) : base(owner)
        {
            behaviors = new List<SteeringBehavior>();
            weights = new List<float>();
        }

        /** Adds a steering behavior and its weight to the list.
         * @param behavior the steering behavior to add
         * @param weight the weight of the behavior
         * @return this behavior for chaining. */
        public void Add(SteeringBehavior behavior, float weight = 1.0f)
        {
            behavior.Owner = Owner;
            behaviors.Add(behavior);
            weights.Add(weight);
        }

        protected override SteeringAcceleration CalculateRealSteering(SteeringAcceleration steering)
        {
            // Clear the output to start with
            SteeringAcceleration blendedSteering = SteeringAcceleration.Zero;

            // Go through all the behaviors
            int len = behaviors.Count;
            for (int i = 0; i < len; i++)
            {
                // Scale and add the steering to the accumulator
                blendedSteering.MulAdd(behaviors[i].CalculateSteering(blendedSteering), weights[i]);
            }

            ILimiter actualLimiter = GetActualLimiter();

            // Crop the result
            blendedSteering.linear = Vector3.ClampMagnitude(blendedSteering.linear, actualLimiter.MaxLinearAcceleration);
            if (blendedSteering.angular > actualLimiter.MaxAngularAcceleration)
                blendedSteering.angular = actualLimiter.MaxAngularAcceleration;

            return blendedSteering;
        }
    }
}