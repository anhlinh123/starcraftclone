using UnityEngine;

namespace StarcraftClone.AI.Steer
{
    /** A {@code Collision} is made up of a collision point and the normal at that point of collision. */
    public struct Collision
    {
        /** The collision point. */
        public Vector3 point;

        /** The normal of this collision. */
        public Vector3 normal;

        /** The center of this collision object. Generally, this is not very meaningful for some complex 
         * structure (for example, a long wall. We do care about point and normal of this kind of collision,
         * not the center of the wall). But in some situations, it brings a lot of advantages for 
         * collision avoidance behaviors (for example, a box or a circle/sphere). */
        public Vector3 center;

        /** Creates a {@code Collision} with the given {@code point} and {@code normal}.
         * @param point the point where this collision occurred
         * @param normal the normal of this collision */
        public Collision(Vector3 point, Vector3 normal, Vector3 center)
        {
            this.point = point;
            this.normal = normal;
            this.center = center;
        }

        /** Sets this collision from the given collision.
         * @param collision The collision
         * @return this collision for chaining. */
        public Collision Set(Collision collision)
        {
            point = collision.point;
            normal = collision.normal;
            center = collision.center;
            return this;
        }

        /** Sets this collision from the given point and normal.
         * @param point the collision point
         * @param normal the normal of this collision
         * @return this collision for chaining. */
        public Collision Set(Vector3 point, Vector3 normal, Vector3 center)
        {
            this.point = point;
            this.normal = normal;
            this.center = center;
            return this;
        }
    }
}