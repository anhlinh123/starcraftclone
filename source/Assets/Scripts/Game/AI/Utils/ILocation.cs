using UnityEngine;

namespace StarcraftClone.AI.Steer
{
    /** The Location interface represents any game object having a position and an orientation. */
    public interface ILocation
    {
        Vector3 Position { get; }
        float Orientation { get; set; }
        float BoundingRadius { get; }
        Vector3 AngleToVector(float angle);
        float VectorToAngle(Vector3 vector);
    }
}