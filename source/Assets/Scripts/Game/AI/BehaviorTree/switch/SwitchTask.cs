namespace StarcraftClone.AI.BehaviorTree
{
    public class SwitchTask : BranchTask
    {
        [TaskProperty]
        public string name { get; set; }

        private delegate string Function();
        private Function m_function = null;
        private Task m_runningTask = null;

        private System.Collections.Generic.Dictionary<string, CaseTask> m_cases = null;

        public SwitchTask()
            : this(new System.Collections.Generic.List<Task>())
        {
        }

        public SwitchTask(System.Collections.Generic.List<Task> caseTasks)
        {
            m_cases = new System.Collections.Generic.Dictionary<string, CaseTask>();
            children = caseTasks;
        }

        public override void start()
        {
            if (m_cases.Count <= 0)
            {
                for (int i = children.Count - 1; i >= 0; i--)
                {
                    CaseTask caseTask = children[i] as CaseTask;
                    if (caseTask == null)
                        throw new System.InvalidOperationException("A switch task cannot have children other than case tasks");
                    if (caseTask.name == null)
                        throw new System.InvalidOperationException("CaseTask with no name is not allowed");
                    if (m_cases.ContainsKey(caseTask.name))
                        throw new System.InvalidOperationException("Duplicated case name:" + caseTask.name);
                    m_cases.Add(caseTask.name, caseTask);
                }
                if (!m_cases.ContainsKey("default"))
                    throw new System.InvalidOperationException("No default case has been found");
            }
        }

        protected override int addChildToTask(Task child)
        {
            CaseTask caseTask = child as CaseTask;
            if (caseTask == null) throw new System.InvalidOperationException("A switch task cannot have children other than case tasks");
            children.Add(caseTask);
            return children.Count - 1;
        }

        private void SetFunction()
        {
            object _object = getObject();
            if (_object != null)
            {
                System.Type type = _object.GetType();
                System.Reflection.MethodInfo method = type.GetMethod(name, System.Type.EmptyTypes);
                if (method != null && method.GetCustomAttributes(typeof(SwitchFunction), false).Length > 0)
                {
                    Function function = System.Delegate.CreateDelegate(typeof(Function), _object, method) as Function;
                    if (function != null)
                        this.m_function = function;
                }
            }
        }

        public override void run()
        {
            string caseName = "default";
            if (m_function == null)
                SetFunction();

            if (m_function != null)
            {
                caseName = m_function();
            }

            CaseTask task = null;
            if (!m_cases.TryGetValue(caseName, out task))
            {
                task = m_cases["default"];
            }
            if (m_runningTask != null && task != m_runningTask)
            {
                m_runningTask.cancel();
                m_runningTask = null;
            }
            if (m_runningTask == null)
            {
                m_runningTask = task;
                m_runningTask.setControl(this);
                m_runningTask.start();
            }
            m_runningTask.run();
        }

        public override void childRunning(Task task, Task reporter)
        {
            running(); // Return a running status when a child says it's running
        }

        public override void childSuccess(Task task)
        {
            success();
        }

        public override void childFail(Task task)
        {
            fail();
        }

        protected override Task copyTo(Task task)
        {
            SwitchTask switchTask = (SwitchTask)task;
            switchTask.name = this.name;
            if (m_cases.Count > 0)
            {          
                System.Collections.Generic.Dictionary<string, CaseTask>.Enumerator enumarator = switchTask.m_cases.GetEnumerator();
                while (enumarator.MoveNext())
                {
                    System.Collections.Generic.KeyValuePair<string, CaseTask> pair = enumarator.Current;
                    if (pair.Key != null)
                    {
                        if (!m_cases.ContainsKey(pair.Key))
                        {
                            m_cases.Add(pair.Key, (CaseTask)pair.Value.cloneTask());
                        }
                        else
                            throw new System.InvalidOperationException("Duplicated case name:" + pair.Key);
                    }
                }
            }
            
            base.copyTo(task);
            return task;
        }
    }
}