namespace StarcraftClone.AI.BehaviorTree
{
    public class DefaultTask : CaseTask
    {
        [TaskProperty]
        public override string name
        {
            get
            {
                return "default";
            }
        }
    }
}