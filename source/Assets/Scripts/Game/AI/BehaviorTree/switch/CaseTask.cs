namespace StarcraftClone.AI.BehaviorTree
{
    public class CaseTask : Decorator
    {
        /** Value of the "case" */
        [TaskProperty("", true)]
        public virtual string name { get; set; }

        protected override Task copyTo(Task task)
        {
            CaseTask caseTask = (CaseTask)task;
            caseTask.name = this.name;
            base.copyTo(task);

            return task;
        }
    }
}
