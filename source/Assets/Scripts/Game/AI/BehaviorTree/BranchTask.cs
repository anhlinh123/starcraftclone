namespace StarcraftClone.AI.BehaviorTree
{
    /** A branch task defines a behavior tree branch, contains logic of starting or running sub-branches and leaves
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation
     * @author davebaol */
    [TaskConstraint(1)]
    public abstract class BranchTask : Task
    {

        /** The children of this branch task. */
        protected System.Collections.Generic.List<Task> children;

        /** Create a branch task with no children */
        public BranchTask()
            : this(new System.Collections.Generic.List<Task>())
        {
        }

        /** Create a branch task with a list of children
         * 
         * @param tasks list of this task's children, can be empty */
        public BranchTask(System.Collections.Generic.List<Task> tasks)
        {
            this.children = tasks;
        }

        protected override int addChildToTask(Task child)
        {
            children.Add(child);
            return children.Count - 1;
        }

        public override int getChildCount()
        {
            return children.Count;
        }

        public override Task getChild(int i)
        {
            return children[i];
        }

        protected override Task copyTo(Task task)
        {
            BranchTask branch = (BranchTask)task;
            if (children != null)
            {
                for (int i = 0; i < children.Count; i++)
                {
                    branch.children.Add(children[i].cloneTask());
                }
            }

            return task;
        }

    }
}