namespace StarcraftClone.AI.BehaviorTree
{

    public sealed class TriangularFloatDistribution : FloatDistribution
    {

        private readonly float low;
        private readonly float high;
        private readonly float mode;

        public TriangularFloatDistribution(float high)
            : this(-high, high)
        {
        }

        public TriangularFloatDistribution(float low, float high)
            : this(low, high, (low + high) * .5f)
        {
        }

        public TriangularFloatDistribution(float low, float high, float mode)
        {
            this.low = low;
            this.high = high;
            this.mode = mode;
        }

        public override float nextFloat()
        {
            if (-low == high && mode == 0) return (float)TriangularDoubleDistribution.randomTriangular(high); // It's faster
            return (float)TriangularDoubleDistribution.randomTriangular(low, high, mode);
        }

        public float getLow()
        {
            return low;
        }

        public float getHigh()
        {
            return high;
        }

        public float getMode()
        {
            return mode;
        }

    }
}