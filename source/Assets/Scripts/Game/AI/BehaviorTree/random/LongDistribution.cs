namespace StarcraftClone.AI.BehaviorTree
{
    public abstract class LongDistribution : Distribution
    {
        public int nextInt()
        {
            return (int)nextLong();
        }

        public float nextFloat()
        {
            return (float)nextLong();
        }

        public double nextDouble()
        {
            return (double)nextLong();
        }

        public abstract long nextLong();
    }
}