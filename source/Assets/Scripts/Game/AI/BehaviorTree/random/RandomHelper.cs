﻿namespace StarcraftClone.AI.BehaviorTree
{
    public static class RandomHelper
    {
        private static System.Random rand = new System.Random();
        
        public static int Next(int min, int max)
        {
            return rand.Next(min, max);
        }

        public static double NextDouble()
        {
            return rand.NextDouble();
        }
    }
}
