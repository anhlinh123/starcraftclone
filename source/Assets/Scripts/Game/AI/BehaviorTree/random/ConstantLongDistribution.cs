namespace StarcraftClone.AI.BehaviorTree
{
    public sealed class ConstantLongDistribution : LongDistribution
    {
        public static readonly ConstantLongDistribution NEGATIVE_ONE = new ConstantLongDistribution(-1);
        public static readonly ConstantLongDistribution ZERO = new ConstantLongDistribution(0);
        public static readonly ConstantLongDistribution ONE = new ConstantLongDistribution(1);

        private readonly long value;

        public ConstantLongDistribution(long value)
        {
            this.value = value;
        }

        public override long nextLong()
        {
            return value;
        }

        public long getValue()
        {
            return value;
        }
    }
}