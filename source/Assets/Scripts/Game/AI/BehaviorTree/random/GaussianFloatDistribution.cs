namespace StarcraftClone.AI.BehaviorTree
{

    public sealed class GaussianFloatDistribution : FloatDistribution
    {

        public static readonly GaussianFloatDistribution STANDARD_NORMAL = new GaussianFloatDistribution(0, 1);

        private readonly float mean;
        private readonly float standardDeviation;

        public GaussianFloatDistribution(float mean, float standardDeviation)
        {
            this.mean = mean;
            this.standardDeviation = standardDeviation;
        }

        public override float nextFloat()
        {
            double u1 = RandomHelper.NextDouble();
            double u2 = RandomHelper.NextDouble();
            double randStdNormal = System.Math.Sqrt(-2.0 * System.Math.Log(u1)) *
                 System.Math.Sin(2.0 * System.Math.PI * u2);
            return mean + (float)randStdNormal * standardDeviation;
        }

        public float getMean()
        {
            return mean;
        }

        public float getStandardDeviation()
        {
            return standardDeviation;
        }

    }
}