namespace StarcraftClone.AI.BehaviorTree
{

    public sealed class TriangularIntegerDistribution : IntegerDistribution
    {

        private readonly int low;
        private readonly int high;
        private readonly float mode;

        public TriangularIntegerDistribution(int high)
            : this(-high, high)
        {
        }

        public TriangularIntegerDistribution(int low, int high)
            : this(low, high, (low + high) * .5f)
        {
        }

        public TriangularIntegerDistribution(int low, int high, float mode)
        {
            this.low = low;
            this.high = high;
            this.mode = mode;
        }

        public override int nextInt()
        {
            double r;
            if (-low == high && mode == 0)
                r = TriangularDoubleDistribution.randomTriangular(high); // It's faster
            else
                r = TriangularDoubleDistribution.randomTriangular(low, high, mode);
            return (int)System.Math.Round(r);
        }

        public int getLow()
        {
            return low;
        }

        public int getHigh()
        {
            return high;
        }

        public float getMode()
        {
            return mode;
        }

    }
}