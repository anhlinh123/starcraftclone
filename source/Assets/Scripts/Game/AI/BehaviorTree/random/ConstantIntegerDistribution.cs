namespace StarcraftClone.AI.BehaviorTree
{
    public sealed class ConstantIntegerDistribution : IntegerDistribution
    {
        public static readonly ConstantIntegerDistribution NEGATIVE_ONE = new ConstantIntegerDistribution(-1);
        public static readonly ConstantIntegerDistribution ZERO = new ConstantIntegerDistribution(0);
        public static readonly ConstantIntegerDistribution ONE = new ConstantIntegerDistribution(1);

        private readonly int value;

        public ConstantIntegerDistribution(int value)
        {
            this.value = value;
        }

        public override int nextInt()
        {
            return value;
        }

        public int getValue()
        {
            return value;
        }

    }
}