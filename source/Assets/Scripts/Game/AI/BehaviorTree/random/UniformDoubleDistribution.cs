namespace StarcraftClone.AI.BehaviorTree
{

    public sealed class UniformDoubleDistribution : DoubleDistribution
    {

        private readonly double low;
        private readonly double high;

        public UniformDoubleDistribution(double high)
            : this(0, high)
        {
        }

        public UniformDoubleDistribution(double low, double high)
        {
            this.low = low;
            this.high = high;
        }

        public override double nextDouble()
        {
            return low + RandomHelper.NextDouble() * (high - low);
        }

        public double getLow()
        {
            return low;
        }

        public double getHigh()
        {
            return high;
        }

    }
}
