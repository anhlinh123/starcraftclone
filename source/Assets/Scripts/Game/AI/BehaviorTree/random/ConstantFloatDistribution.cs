namespace StarcraftClone.AI.BehaviorTree
{
    public sealed class ConstantFloatDistribution : FloatDistribution
    {
        public static readonly ConstantFloatDistribution NEGATIVE_ONE = new ConstantFloatDistribution(-1);
        public static readonly ConstantFloatDistribution ZERO = new ConstantFloatDistribution(0);
        public static readonly ConstantFloatDistribution ONE = new ConstantFloatDistribution(1);
        public static readonly ConstantFloatDistribution ZERO_POINT_FIVE = new ConstantFloatDistribution(.5f);

        private readonly float value;

        public ConstantFloatDistribution(float value)
        {
            this.value = value;
        }

        public override float nextFloat()
        {
            return value;
        }

        public float getValue()
        {
            return value;
        }

    }
}