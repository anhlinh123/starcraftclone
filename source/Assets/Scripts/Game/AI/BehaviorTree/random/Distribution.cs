namespace StarcraftClone.AI.BehaviorTree
{
    public interface Distribution
    {
        int nextInt();

        long nextLong();

        float nextFloat();

        double nextDouble();

    }
}