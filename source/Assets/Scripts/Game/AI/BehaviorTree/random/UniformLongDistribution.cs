namespace StarcraftClone.AI.BehaviorTree
{

    public sealed class UniformLongDistribution : LongDistribution
    {

        private readonly long low;
        private readonly long high;

        public UniformLongDistribution(long high)
            : this(0, high)
        {
        }

        public UniformLongDistribution(long low, long high)
        {
            this.low = low;
            this.high = high;
        }

        public override long nextLong()
        {
            return low + (long)(RandomHelper.NextDouble() * (high - low));
        }

        public long getLow()
        {
            return low;
        }

        public long getHigh()
        {
            return high;
        }

    }
}
