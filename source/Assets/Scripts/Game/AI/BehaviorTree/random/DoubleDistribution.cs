namespace StarcraftClone.AI.BehaviorTree
{
    public abstract class DoubleDistribution : Distribution
    {
        public int nextInt()
        {
            return (int)nextDouble();
        }

        public long nextLong()
        {
            return (long)nextDouble();
        }

        public float nextFloat()
        {
            return (float)nextDouble();
        }

        public abstract double nextDouble();
    }
}