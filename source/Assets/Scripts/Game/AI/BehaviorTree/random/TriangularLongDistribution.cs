namespace StarcraftClone.AI.BehaviorTree
{

    /** @author davebaol */
    public sealed class TriangularLongDistribution : LongDistribution
    {

        private readonly long low;
        private readonly long high;
        private readonly double mode;

        public TriangularLongDistribution(long high)
            : this(-high, high)
        {
        }

        public TriangularLongDistribution(long low, long high)
            : this(low, high, (low + high) * .5)
        {
        }

        public TriangularLongDistribution(long low, long high, double mode)
        {
            this.low = low;
            this.high = high;
            this.mode = mode;
        }

        public override long nextLong()
        {
            double r;
            if (-low == high && mode == 0)
                r = TriangularDoubleDistribution.randomTriangular(high); // It's faster
            else
                r = TriangularDoubleDistribution.randomTriangular(low, high, mode);
            return (long)System.Math.Round(r);
        }

        public long getLow()
        {
            return low;
        }

        public long getHigh()
        {
            return high;
        }

        public double getMode()
        {
            return mode;
        }

    }
}
