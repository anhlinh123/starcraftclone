namespace StarcraftClone.AI.BehaviorTree
{

    public sealed class UniformIntegerDistribution : IntegerDistribution
    {

        private readonly int low;
        private readonly int high;

        public UniformIntegerDistribution(int high)
            : this(0, high)
        {
        }

        public UniformIntegerDistribution(int low, int high)
        {
            this.low = low;
            this.high = high;
        }

        public override int nextInt()
        {
            return RandomHelper.Next(low, high);
        }

        public int getLow()
        {
            return low;
        }

        public int getHigh()
        {
            return high;
        }

    }
}