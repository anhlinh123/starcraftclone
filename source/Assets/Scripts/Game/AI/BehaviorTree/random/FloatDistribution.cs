namespace StarcraftClone.AI.BehaviorTree
{
    public abstract class FloatDistribution : Distribution
    {
        public int nextInt()
        {
            return (int)nextFloat();
        }

        public long nextLong()
        {
            return (long)nextFloat();
        }

        public double nextDouble()
        {
            return (double)nextFloat();
        }

        public abstract float nextFloat();
    }
}
