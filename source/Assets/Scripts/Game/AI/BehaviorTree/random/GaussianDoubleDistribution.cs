namespace StarcraftClone.AI.BehaviorTree
{

    /** @author davebaol */
    public sealed class GaussianDoubleDistribution : DoubleDistribution
    {

        public static readonly GaussianDoubleDistribution STANDARD_NORMAL = new GaussianDoubleDistribution(0, 1);

        private readonly double mean;
        private readonly double standardDeviation;

        public GaussianDoubleDistribution(double mean, double standardDeviation)
        {
            this.mean = mean;
            this.standardDeviation = standardDeviation;
        }

        public override double nextDouble()
        {
            double u1 = RandomHelper.NextDouble();
            double u2 = RandomHelper.NextDouble();
            double randStdNormal = System.Math.Sqrt(-2.0 * System.Math.Log(u1)) *
                 System.Math.Sin(2.0 * System.Math.PI * u2);
            return mean + randStdNormal * standardDeviation;
        }

        public double getMean()
        {
            return mean;
        }

        public double getStandardDeviation()
        {
            return standardDeviation;
        }

    }
}
