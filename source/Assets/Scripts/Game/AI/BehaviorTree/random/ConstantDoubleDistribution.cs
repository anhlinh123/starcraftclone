namespace StarcraftClone.AI.BehaviorTree
{
    public sealed class ConstantDoubleDistribution : DoubleDistribution
    {
        public static readonly ConstantDoubleDistribution NEGATIVE_ONE = new ConstantDoubleDistribution(-1);
        public static readonly ConstantDoubleDistribution ZERO = new ConstantDoubleDistribution(0);
        public static readonly ConstantDoubleDistribution ONE = new ConstantDoubleDistribution(1);

        private readonly double value;

        public ConstantDoubleDistribution(double value)
        {
            this.value = value;
        }

        public override double nextDouble()
        {
            return value;
        }

        public double getValue()
        {
            return value;
        }
    }
}
