namespace StarcraftClone.AI.BehaviorTree
{

    public sealed class UniformFloatDistribution : FloatDistribution
    {

        private readonly float low;
        private readonly float high;

        public UniformFloatDistribution(float high)
            : this(0, high)
        {
        }

        public UniformFloatDistribution(float low, float high)
        {
            this.low = low;
            this.high = high;
        }

        public override float nextFloat()
        {
            return (float)RandomHelper.NextDouble() * (high - low) + low;
        }

        public float getLow()
        {
            return low;
        }

        public float getHigh()
        {
            return high;
        }
    }
}
