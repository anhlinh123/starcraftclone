namespace StarcraftClone.AI.BehaviorTree
{
    public abstract class IntegerDistribution : Distribution
    {
        public long nextLong()
        {
            return (long)nextInt();
        }

        public float nextFloat()
        {
            return (float)nextInt();
        }

        public double nextDouble()
        {
            return (double)nextInt();
        }

        public abstract int nextInt();
    }
}