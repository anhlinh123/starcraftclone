namespace StarcraftClone.AI.BehaviorTree
{

    /** A {@code Decorator} is a wrapper that provides custom behavior for its child. The child can be of any kind (branch task, leaf
     * task, or another decorator).
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation
     * @author davebaol */
    [TaskConstraint(1, 1)]
    public abstract class Decorator : Task
    {

        /** The child task wrapped by this decorator */
        protected Task child;

        /** Creates a decorator with no child task. */
        public Decorator()
        {
        }

        /** Creates a decorator that wraps the given task.
         * 
         * @param child the task that will be wrapped */
        public Decorator(Task child)
        {
            this.child = child;
        }

        protected override int addChildToTask(Task child)
        {
            if (this.child != null) throw new System.InvalidOperationException("A decorator task cannot have more than one child");
            this.child = child;
            return 0;
        }

        public override int getChildCount()
        {
            return child == null ? 0 : 1;
        }

        public override Task getChild(int i)
        {
            if (i == 0 && child != null) return child;
            throw new System.IndexOutOfRangeException("index can't be >= size: " + i + " >= " + getChildCount());
        }

        public override void run()
        {
            if (child.status == Status.RUNNING)
            {
                child.run();
            }
            else
            {
                child.setControl(this);
                child.start();
                if (child.checkGuard(this))
                    child.run();
                else
                    child.fail();
            }
        }

        public override void childRunning(Task runningTask, Task reporter)
        {
            running();
        }

        public override void childFail(Task runningTask)
        {
            fail();
        }

        public override void childSuccess(Task runningTask)
        {
            success();
        }

        protected override Task copyTo(Task task)
        {
            if (this.child != null)
            {
                Decorator decorator = (Decorator)task;
                decorator.child = this.child.cloneTask();
            }

            return task;
        }

    }
}