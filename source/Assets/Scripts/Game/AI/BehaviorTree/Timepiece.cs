﻿namespace StarcraftClone.AI.BehaviorTree
{

    public static class Timepiece
    {

        private static float time;
        private static float deltaTime;
        private static float maxDeltaTime;

        static Timepiece()
        {
            time = 0f;
            deltaTime = 0f;
            maxDeltaTime = float.MaxValue;
        }

        public static float getTime()
        {
            return time;
        }

        public static float getDeltaTime()
        {
            return deltaTime;
        }

        public static void update(float deltaTime)
        {
            deltaTime = (deltaTime > maxDeltaTime ? maxDeltaTime : deltaTime);
            time += deltaTime;
        }

    }
}