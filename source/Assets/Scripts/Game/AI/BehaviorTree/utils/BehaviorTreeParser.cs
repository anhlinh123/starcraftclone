namespace StarcraftClone.AI.BehaviorTree
{
    public class BehaviorTreeParser
    {
        private static readonly string TAG = "BehaviorTreeParser";

        public DistributionAdapters distributionAdapters;

        private DefaultBehaviorTreeReader m_btReader;

        public BehaviorTreeParser()
            : this(new DistributionAdapters())
        {
        }

        public BehaviorTreeParser(DistributionAdapters distributionAdapters)
            : this(distributionAdapters, null)
        {
        }

        public BehaviorTreeParser(DistributionAdapters distributionAdapters, DefaultBehaviorTreeReader reader)
        {
            this.distributionAdapters = distributionAdapters;
            m_btReader = reader == null ? new DefaultBehaviorTreeReader() : reader;
            m_btReader.setParser(this);
        }

        /** Parses the given string.
         * @param string the string to parse
         * @param object the blackboard object. It can be {@code null}.
         * @return the behavior tree
         * @throws SerializationException if the string cannot be successfully parsed. */
        public BehaviorTree parse(string _string, object _object)
        {
            m_btReader.parse(_string);
            return createBehaviorTree(m_btReader.m_root, _object);
        }

        /** Parses the given input stream.
         * @param input the input stream to parse
         * @param object the blackboard object. It can be {@code null}.
         * @return the behavior tree
         * @throws SerializationException if the input stream cannot be successfully parsed. */
        public BehaviorTree parse(System.IO.Stream input, object _object)
        {
            m_btReader.parse(input);
            return createBehaviorTree(m_btReader.m_root, _object);
        }

        /** Parses the given file.
         * @param file the file to parse
         * @param object the blackboard object. It can be {@code null}.
         * @return the behavior tree
         * @throws SerializationException if the file cannot be successfully parsed. */
        public BehaviorTree parse(FileSystem.IFile file, object _object)
        {
            m_btReader.parse(file);
            return createBehaviorTree(m_btReader.m_root, _object);
        }

        /** Parses the given reader.
         * @param reader the reader to parse
         * @param object the blackboard object. It can be {@code null}.
         * @return the behavior tree
         * @throws SerializationException if the reader cannot be successfully parsed. */
        public BehaviorTree parse(System.IO.StreamReader reader, object _object)
        {
            m_btReader.parse(reader);
            return createBehaviorTree(m_btReader.m_root, _object);
        }

        protected BehaviorTree createBehaviorTree(Task root, object _object)
        {
#if DEBUG
            printTree(root, 0);
#endif
            return new BehaviorTree(root, _object);
        }

        [System.Diagnostics.Conditional("DEBUG")]
        protected static void printTree(Task task, int indent)
        {
            string output = "";
            for (int i = 0; i < indent; i++)
                output += ' ';
            if (task.getGuard() != null)
            {
                output += "Guard\n";
                Debug.Log(output);
                output = "";
                indent = indent + 2;
                printTree(task.getGuard(), indent);
                for (int i = 0; i < indent; i++)
                    output += ' ';
            }
            output += task.GetType().Name;
            Debug.Log(output);
            for (int i = 0; i < task.getChildCount(); i++)
            {
                printTree(task.getChild(i), indent + 2);
            }
        }

        public class DefaultBehaviorTreeReader : BehaviorTreeReader
        {

            private static readonly System.Collections.Generic.Dictionary<string, string> DEFAULT_IMPORTS = new System.Collections.Generic.Dictionary<string, string>();
            static DefaultBehaviorTreeReader()
            {
                System.Type[] classes = new System.Type[] {
				typeof(AlwaysFail),
				typeof(AlwaysSucceed),
				typeof(DynamicGuardSelector),
				typeof(Failure),
				typeof(Include),
				typeof(Invert),
				typeof(Parallel),
				typeof(Random),
				typeof(RandomSelector),
				typeof(RandomSequence),
				typeof(Repeat),
				typeof(Selector),
				typeof(SemaphoreGuard),
				typeof(Sequence),
				typeof(Success),
				typeof(UntilFail),
				typeof(UntilSuccess),
				typeof(Wait)
			};
                foreach (System.Type c in classes)
                {
                    string fqcn = c.FullName;
                    string cn = c.Name.ToLower();
                    DEFAULT_IMPORTS.Add(cn, fqcn);
                }

                DEFAULT_IMPORTS.Add("switch", typeof(SwitchTask).FullName);
                DEFAULT_IMPORTS.Add("case", typeof(CaseTask).FullName);
                DEFAULT_IMPORTS.Add("default", typeof(DefaultTask).FullName);
            }

            private abstract class Statement
            {
                private class SubtreeStatement : Statement
                {
                    public SubtreeStatement()
                        : base("subtree")
                    {
                    }

                    public override void enter(DefaultBehaviorTreeReader reader, string name, bool isGuard)
                    {
                    }

                    public override bool attribute(DefaultBehaviorTreeReader reader, string name, object value)
                    {
                        if (name != "name") reader.throwAttributeNameException(this.name, name, "name");
                        if (!(value is string)) reader.throwAttributeTypeException(this.name, name, "string");
                        if ("" == (string)value) throw new System.SystemException(this.name + ": the name connot be empty");
                        if (reader.m_subtreeName != null)
                            throw new System.SystemException(this.name + ": the name has been already specified");
                        reader.m_subtreeName = (string)value;
                        return true;
                    }

                    public override void exit(DefaultBehaviorTreeReader reader)
                    {
                        if (reader.m_subtreeName == null)
                            throw new System.SystemException(this.name + ": the name has not been specified");
                        reader.switchToNewTree(reader.m_subtreeName);
                        reader.m_subtreeName = null;
                    }
                }
                private class RootStatement : Statement
                {
                    public RootStatement()
                        : base("root")
                    {
                    }

                    public override void enter(DefaultBehaviorTreeReader reader, string name, bool isGuard)
                    {
                        reader.m_subtreeName = ""; // the root tree has empty name
                    }

                    public override bool attribute(DefaultBehaviorTreeReader reader, string name, object value)
                    {
                        reader.throwAttributeTypeException(this.name, name, null);
                        return true;
                    }

                    public override void exit(DefaultBehaviorTreeReader reader)
                    {
                        reader.switchToNewTree(reader.m_subtreeName);
                        reader.m_subtreeName = null;
                    }
                }
                private class TreeTaskStatement : Statement
                {
                    public TreeTaskStatement()
                        : base(null)
                    {
                    }

                    public override void enter(DefaultBehaviorTreeReader reader, string name, bool isGuard)
                    {
                        // Root tree is the default one
                        if (reader.m_currentTree == null)
                        {
                            reader.switchToNewTree("");
                            reader.m_subtreeName = null;
                        }

                        reader.openTask(name, isGuard);
                    }

                    public override bool attribute(DefaultBehaviorTreeReader reader, string name, object value)
                    {
                        StackedTask stackedTask = reader.getCurrentTask();
                        AttrInfo ai = null;
                        if (!stackedTask.metadata.attributes.TryGetValue(name, out ai)) return false;
                        bool isNew = reader.m_encounteredAttributes.Add(name);
                        if (!isNew) throw reader.stackedTaskException(stackedTask, "attribute '" + name + "' specified more than once");
                        System.Reflection.PropertyInfo attributeProperty = reader.getProperty(stackedTask.task.GetType(), ai.propertyName);
                        reader.setProperty(attributeProperty, stackedTask.task, value);
                        return true;
                    }

                    public override void exit(DefaultBehaviorTreeReader reader)
                    {
                        if (!reader.m_isSubtreeRef)
                        {
                            reader.checkRequiredAttributes(reader.getCurrentTask());
                            reader.m_encounteredAttributes.Clear();
                        }
                    }
                }

                public static Statement Subtree = new SubtreeStatement();
                public static Statement Root = new RootStatement();
                public static Statement TreeTask = new TreeTaskStatement();

                public readonly string name;
                protected Statement(string name)
                {
                    this.name = name;
                }
                public abstract void enter(DefaultBehaviorTreeReader reader, string name, bool isGuard);
                public abstract bool attribute(DefaultBehaviorTreeReader reader, string name, object value);
                public abstract void exit(DefaultBehaviorTreeReader reader);
            }

            protected BehaviorTreeParser m_btParser;

            System.Collections.Generic.Dictionary<System.Type, Metadata> m_metadataCache = new System.Collections.Generic.Dictionary<System.Type, Metadata>();

            internal Task m_root;
            string m_subtreeName;
            Statement m_statement;
            private int m_indent;       //Indent of the current line being parsed

            public DefaultBehaviorTreeReader()
                : this(false)
            {
            }

            public DefaultBehaviorTreeReader(bool reportsComments)
                : base(reportsComments)
            {
            }

            public BehaviorTreeParser getParser()
            {
                return m_btParser;
            }

            public void setParser(BehaviorTreeParser parser)
            {
                this.m_btParser = parser;
            }

            public override void parse(char[] data, int offset, int length)
            {
                m_root = null;
                clear();
                base.parse(data, offset, length);

                // Pop all task from the stack and check their minimum number of children
                popAndCheckMinChildren(0);

                Subtree rootTree = null;
                if (!m_subtrees.TryGetValue("", out rootTree)) throw new System.SystemException("Missing root tree");
                m_root = rootTree.rootTask;
                if (m_root == null) throw new System.SystemException("The tree must have at least the root task");
                clear();
            }

            protected override void startLine(int indent)
            {
                Debug.Log(TAG + ": " + lineNumber + ": <" + indent + ">");
                this.m_indent = indent;
            }

            private Statement checkStatement(string name)
            {
                if (name == Statement.Subtree.name) return Statement.Subtree;
                if (name == Statement.Root.name) return Statement.Root;
                return Statement.TreeTask;
            }

            protected override void startStatement(string name, bool isSubtreeReference, bool isGuard)
            {
                Debug.Log(TAG + ": " + (isGuard ? " guard" : " task") + " name '" + name + "'");
                this.m_isSubtreeRef = isSubtreeReference;

                this.m_statement = isSubtreeReference ? Statement.TreeTask : checkStatement(name);
                if (isGuard)
                {
                    if (m_statement != Statement.TreeTask)
                        throw new System.SystemException(name + ": only tree's tasks can be guarded");
                }

                m_statement.enter(this, name, isGuard);
            }

            protected override void attribute(string name, object value)
            {
                Debug.Log(TAG + ": " + lineNumber + ": attribute '" + name + " : " + value + "'");

                bool validAttribute = m_statement.attribute(this, name, value);
                if (!validAttribute)
                {
                    if (m_statement == Statement.TreeTask)
                    {
                        throw stackedTaskException(getCurrentTask(), "unknown attribute '" + name + "'");
                    }
                    else
                    {
                        throw new System.SystemException(m_statement.name + ": unknown attribute '" + name + "'");
                    }
                }
            }

            private System.Reflection.PropertyInfo getProperty(System.Type clazz, string name)
            {
                return clazz.GetProperty(name);
            }

            private void setProperty(System.Reflection.PropertyInfo property, Task task, object value)
            {
                object valueObject = castValue(property, value);
                property.SetValue(task, valueObject, null);
            }

            private object castValue(System.Reflection.PropertyInfo property, object value)
            {
                System.Type type = property.PropertyType;
                object ret = null;
                if (value is System.Double || value is System.Int64)
                {
                    if (type.IsSubclassOf(typeof(Distribution)))
                    {
                        ret = m_btParser.distributionAdapters.toDistribution("constant," + value, type);
                    }
                    else
                    {
                        ret = value;
                    }
                }
                else if (value is System.Boolean)
                {
                    if (type == typeof(System.Boolean) || type == typeof(bool)) ret = value;
                }
                else if (value is string)
                {
                    string stringValue = (string)value;
                    if (type == typeof(string))
                        ret = value;
                    else if (type == typeof(char))
                    {
                        if (stringValue.Length != 1) throw new System.SystemException("Invalid character '" + value + "'");
                        ret = stringValue[0];
                    }
                    else if (type.IsSubclassOf(typeof(Distribution)))
                    {
                        ret = m_btParser.distributionAdapters.toDistribution(stringValue, type);
                    }
                    else
                    {
                        // Get policy object
                        System.Reflection.FieldInfo fieldInfo = type.GetField(stringValue, System.Reflection.BindingFlags.Public|System.Reflection.BindingFlags.Static);
                        if (fieldInfo != null)
                            ret = fieldInfo.GetValue(null);
                    }
                }
                if (ret == null) throwAttributeTypeException(getCurrentTask().name, property.Name, type.Name);
                return ret;
            }

            private void throwAttributeNameException(string statement, string name, string expectedName)
            {
                string expected = " no attribute expected";
                if (expectedName != null)
                    expected = "expected '" + expectedName + "' instead";
                throw new System.SystemException(statement + ": attribute '" + name + "' unknown; " + expected);
            }

            private void throwAttributeTypeException(string statement, string name, string expectedType)
            {
                throw new System.SystemException(statement + ": attribute '" + name + "' must be of type " + expectedType);
            }

            protected override void endLine()
            {
            }

            protected override void endStatement()
            {
                m_statement.exit(this);
            }

            private void openTask(string name, bool isGuard)
            {
                try
                {
                    Task task;
                    if (m_isSubtreeRef)
                    {
                        task = subtreeRootTaskInstance(name);
                    }
                    else
                    {
                        string className = null;
                        if (DEFAULT_IMPORTS.TryGetValue(name.ToLower(), out className))
                        {
                            task = System.Activator.CreateInstance(null, className).Unwrap() as Task;
                        }
                        else
                        {
                            task = new AI.BehaviorTree.Action(name);
                        }
                    }

                    if (!m_currentTree.inited())
                    {
                        initCurrentTree(task, m_indent);
                        m_indent = 0;
                    }
                    else if (!isGuard)
                    {
                        StackedTask stackedTask = getPrevTask();

                        m_indent -= m_currentTreeStartIndent;
                        if (stackedTask.task == m_currentTree.rootTask)
                        {
                            m_step = m_indent;
                        }
                        if (m_indent > m_currentDepth)
                        {
                            m_stack.Push(stackedTask); // push
                        }
                        else if (m_indent <= m_currentDepth)
                        {
                            // Pop tasks from the stack based on indentation
                            // and check their minimum number of children
                            int i = (m_currentDepth - m_indent) / m_step;
                            popAndCheckMinChildren(m_stack.Count - i);
                        }

                        // Check the max number of children of the parent
                        StackedTask stackedParent = m_stack.Peek();
                        int maxChildren = stackedParent.metadata.maxChildren;
                        if (stackedParent.task.getChildCount() >= maxChildren)
                            throw stackedTaskException(stackedParent, "max number of children exceeded ("
                         + (stackedParent.task.getChildCount() + 1) + " > " + maxChildren + ")");

                        // Add child task to the parent
                        stackedParent.task.addChild(task);
                    }
                    updateCurrentTask(createStackedTask(name, task), m_indent, isGuard);
                }
                catch (System.SystemException e)
                {
                    throw new System.SystemException("Cannot parse behavior tree!!!", e);
                }
            }

            private StackedTask createStackedTask(string name, Task task)
            {
                Metadata metadata = findMetadata(task.GetType());
                if (metadata == null)
                    throw new System.SystemException(name + ": @TaskConstraint annotation not found in '" + task.GetType().Name
                 + "' class hierarchy");
                return new StackedTask(lineNumber, name, task, metadata);
            }
            private Metadata findMetadata(System.Type clazz)
            {
                Metadata metadata = null;
                if (!m_metadataCache.TryGetValue(clazz, out metadata))
                {
                    TaskConstraint tca = System.Attribute.GetCustomAttribute(clazz, typeof(TaskConstraint)) as TaskConstraint;
                    if (tca != null)
                    {
                        System.Collections.Generic.Dictionary<string, AttrInfo> taskProperties = new System.Collections.Generic.Dictionary<string, AttrInfo>();
                        System.Reflection.PropertyInfo[] properties = clazz.GetProperties();
                        foreach (System.Reflection.PropertyInfo p in properties)
                        {
                            TaskProperty property = System.Attribute.GetCustomAttributes(p, typeof(TaskProperty), true)[0] as TaskProperty;
                            if (property != null)
                            {
                                AttrInfo ai = new AttrInfo(p.Name, property);
                                taskProperties.Add(ai.name, ai);
                            }
                        }
                        metadata = new Metadata(tca.minChildren, tca.maxChildren, taskProperties);
                        m_metadataCache.Add(clazz, metadata);
                    }
                }
                return metadata;
            }

            private class StackedTask
            {
                public int lineNumber;
                public string name;
                public Task task;
                public Metadata metadata;

                public StackedTask(int lineNumber, string name, Task task, Metadata metadata)
                {
                    this.lineNumber = lineNumber;
                    this.name = name;
                    this.task = task;
                    this.metadata = metadata;
                }
            }

            private class Metadata
            {
                public int minChildren;
                public int maxChildren;
                public System.Collections.Generic.Dictionary<string, AttrInfo> attributes;

                /** Creates a {@code Metadata} for a task accepting from {@code minChildren} to {@code maxChildren} children and the given
                 * attributes.
                 * @param minChildren the minimum number of children (defaults to 0 if negative)
                 * @param maxChildren the maximum number of children (defaults to {@link Integer.MAX_VALUE} if negative)
                 * @param attributes the attributes */
                public Metadata(int minChildren, int maxChildren, System.Collections.Generic.Dictionary<string, AttrInfo> attributes)
                {
                    this.minChildren = minChildren < 0 ? 0 : minChildren;
                    this.maxChildren = maxChildren < 0 ? System.Int32.MaxValue : maxChildren;
                    this.attributes = attributes;
                }
            }

            private class AttrInfo
            {
                public string name;
                public string propertyName;
                public bool required;

                public AttrInfo(string propertyName, TaskProperty annotation)
                    : this(annotation.name, propertyName, annotation.required)
                {
                }

                public AttrInfo(string name, string propertyName, bool required)
                {
                    this.name = name == null || name.Length == 0 ? propertyName : name;
                    this.propertyName = propertyName;
                    this.required = required;
                }
            }

            protected class Subtree
            {
                public string name;  // root tree must have no name
                public Task rootTask;
                public int referenceCount;

                Subtree()
                    : this(null)
                {
                }

                public Subtree(string name)
                {
                    this.name = name;
                    this.rootTask = null;
                    this.referenceCount = 0;
                }

                public void init(Task rootTask)
                {
                    this.rootTask = rootTask;
                }

                public bool inited()
                {
                    return rootTask != null;
                }

                public bool isRootTree()
                {
                    return name == null || "" == name;
                }

                public Task rootTaskInstance()
                {
                    if (referenceCount++ == 0)
                    {
                        return rootTask;
                    }
                    return rootTask.cloneTask();
                }
            }

            System.Collections.Generic.Dictionary<string, Subtree> m_subtrees = new System.Collections.Generic.Dictionary<string, Subtree>();
            Subtree m_currentTree;              //The current tree being parsed

            int m_currentTreeStartIndent;       //Indent of the current tree
            int m_currentDepth;                 //Indent of the current task
            int m_step;                         //Indent of one step
            bool m_isSubtreeRef;
            private StackedTask m_prevTask;   //The current task
            private StackedTask m_guardChain;
            private System.Collections.Generic.Stack<StackedTask> m_stack = new System.Collections.Generic.Stack<StackedTask>();    //Stack of parent tasks
            System.Collections.Generic.HashSet<string> m_encounteredAttributes = new System.Collections.Generic.HashSet<string>();
            bool m_isGuard;

            StackedTask getLastStackedTask()
            {
                return m_stack.Peek();
            }

            StackedTask getPrevTask()
            {
                return m_prevTask;
            }

            StackedTask getCurrentTask()
            {
                return m_isGuard ? m_guardChain : m_prevTask;
            }

            void updateCurrentTask(StackedTask stackedTask, int indent, bool isGuard)
            {
                this.m_isGuard = isGuard;
                stackedTask.task.setGuard(m_guardChain == null ? null : m_guardChain.task);
                if (isGuard)
                {
                    m_guardChain = stackedTask;
                }
                else
                {
                    m_prevTask = stackedTask;
                    m_guardChain = null;
                    m_currentDepth = indent;
                }
            }

            void clear()
            {
                m_prevTask = null;
                m_guardChain = null;
                m_currentTree = null;
                m_subtrees.Clear();
                m_stack.Clear();
                m_encounteredAttributes.Clear();
            }

            //
            // Subtree
            //

            void switchToNewTree(string name)
            {
                // Pop all task from the stack and check their minimum number of children
                popAndCheckMinChildren(0);

                this.m_currentTree = new Subtree(name);
                if (m_subtrees.ContainsKey(name))
                    throw new System.SystemException("A subtree named '" + name + "' is already defined");
                else
                    m_subtrees.Add(name, m_currentTree);
            }

            void initCurrentTree(Task rootTask, int startIndent)
            {
                m_currentDepth = -1;
                m_step = 1;
                m_currentTreeStartIndent = startIndent;
                m_currentTree.init(rootTask);
                m_prevTask = null;
            }

            Task subtreeRootTaskInstance(string name)
            {
                Subtree tree = null;
                if (!m_subtrees.TryGetValue(name, out tree))
                    throw new System.SystemException("Undefined subtree with name '" + name + "'");
                return tree.rootTaskInstance();
            }

            //
            // Integrity checks
            //

            private void popAndCheckMinChildren(int upToFloor)
            {
                // Check the minimum number of children in prevTask
                if (m_prevTask != null) checkMinChildren(m_prevTask);

                // Check the minimum number of children while popping up to the specified floor
                while (m_stack.Count > upToFloor)
                {
                    StackedTask stackedTask = m_stack.Pop();
                    checkMinChildren(stackedTask);
                }
            }

            private void checkMinChildren(StackedTask stackedTask)
            {
                // Check the minimum number of children
                int minChildren = stackedTask.metadata.minChildren;
                if (stackedTask.task.getChildCount() < minChildren)
                    throw stackedTaskException(stackedTask, "not enough children (" + stackedTask.task.getChildCount() + " < " + minChildren + ")");
            }

            private void checkRequiredAttributes(StackedTask stackedTask)
            {
                // Check the minimum number of children
                if (stackedTask.metadata.attributes.Count > 0)
                {
                    System.Collections.Generic.Dictionary<string, AttrInfo>.Enumerator entries = stackedTask.metadata.attributes.GetEnumerator();
                    while (entries.MoveNext())
                    {
                        System.Collections.Generic.KeyValuePair<string, AttrInfo> entry = entries.Current;
                        if (entry.Value.required && !m_encounteredAttributes.Contains(entry.Key))
                            throw stackedTaskException(stackedTask, "missing required attribute '" + entry.Key + "'");
                    }
                    
                }
            }

            private System.SystemException stackedTaskException(StackedTask stackedTask, string message)
            {
                return new System.SystemException(stackedTask.name + " at line " + stackedTask.lineNumber + ": " + message);
            }
        }
    }
}