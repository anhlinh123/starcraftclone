namespace StarcraftClone.AI.BehaviorTree
{
    /** The {@code BehaviorTreeLibraryManager} is a singleton in charge of the creation of behavior trees using the underlying library.
     * If no library is explicitly set (see the method {@link #setLibrary(BehaviorTreeLibrary)}), a default library instantiated by
     * the constructor {@link BehaviorTreeLibrary#BehaviorTreeLibrary() BehaviorTreeLibrary()} is used instead.
     * 
     * @author davebaol */
    public sealed class BehaviorTreeLibraryManager
    {

        private static BehaviorTreeLibraryManager instance = new BehaviorTreeLibraryManager();

        private BehaviorTreeLibrary library;

        private BehaviorTreeLibraryManager()
        {
            setLibrary(new BehaviorTreeLibrary());
        }

        /** Returns the singleton instance of the {@code BehaviorTreeLibraryManager}. */
        public static BehaviorTreeLibraryManager getInstance()
        {
            return instance;
        }

        /** Gets the the behavior tree library
         * @return the behavior tree library */
        public BehaviorTreeLibrary getLibrary()
        {
            return library;
        }

        /** Sets the the behavior tree library
         * @param library the behavior tree library to set */
        public void setLibrary(BehaviorTreeLibrary library)
        {
            this.library = library;
        }

        /** Creates the root task of {@link BehaviorTree} for the specified reference.
         * @param treeReference the tree identifier, typically a path
         * @return the root task of the tree cloned from the archetype.
         * @throws SerializationException if the reference cannot be successfully parsed.
         * @throws TaskCloneException if the archetype cannot be successfully parsed. */
        public Task createRootTask(string treeReference)
        {
            return library.createRootTask(treeReference);
        }

        /** Creates the {@link BehaviorTree} for the specified reference.
         * @param treeReference the tree identifier, typically a path
         * @return the tree cloned from the archetype.
         * @throws SerializationException if the reference cannot be successfully parsed.
         * @throws TaskCloneException if the archetype cannot be successfully parsed. */
        public BehaviorTree createBehaviorTree(string treeReference)
        {
            return library.createBehaviorTree(treeReference);
        }

        /** Creates the {@link BehaviorTree} for the specified reference and blackboard object.
         * @param treeReference the tree identifier, typically a path
         * @param blackboard the blackboard object (it can be {@code null}).
         * @return the tree cloned from the archetype.
         * @throws SerializationException if the reference cannot be successfully parsed.
         * @throws TaskCloneException if the archetype cannot be successfully parsed. */
        public BehaviorTree createBehaviorTree(string treeReference, object blackboard)
        {
            return library.createBehaviorTree(treeReference, blackboard);
        }

    }
}