namespace StarcraftClone.AI.BehaviorTree
{
    public class DistributionAdapters
    {

        /** Thrown to indicate that the application has attempted to convert a string to one of the distribution types, but that the
         * string does not have the appropriate format.
         * 
         * @author davebaol */
        public class DistributionFormatException : System.Exception
        {

            /** Constructs a <code>DistributionFormatException</code> with no detail message. */
            public DistributionFormatException()
                : base()
            {
            }

            /** Constructs a <code>DistributionFormatException</code> with the specified detail message.
             *
             * @param s the detail message. */
            public DistributionFormatException(string s)
                : base(s)
            {
            }

            /** Constructs a <code>DistributionFormatException</code> with the specified detail message and cause.
             * <p>
             * Note that the detail message associated with <code>cause</code> is <i>not</i> automatically incorporated in this
             * exception's detail message.
             *
             * @param message the detail message (which is saved for later retrieval by the {@link Throwable#getMessage()} method).
             * @param cause the cause (which is saved for later retrieval by the {@link Throwable#getCause()} method). (A <tt>null</tt>
             *           value is permitted, and indicates that the cause is nonexistent or unknown.) */
            public DistributionFormatException(string message, System.Exception cause)
                : base(message, cause)
            {
            }

            /** Constructs a <code>DistributionFormatException</code> with the specified cause and a detail message of
             * <tt>(cause==null ? null : cause.toString())</tt> (which typically contains the class and detail message of <tt>cause</tt>
             * ). This constructor is useful for exceptions that are little more than wrappers for other throwables.
             *
             * @param cause the cause (which is saved for later retrieval by the {@link Throwable#getCause()} method). (A <tt>null</tt>
             *           value is permitted, and indicates that the cause is nonexistent or unknown.) */
            public DistributionFormatException(System.Exception cause)
                : base(cause.Message, cause)
            {
            }

        }

        private static double parseDouble(string v)
        {
            try
            {
                return System.Convert.ToDouble(v);
            }
            catch (System.FormatException nfe)
            {
                throw new DistributionFormatException("Not a double value: " + v, nfe);
            }
        }

        private static float parseFloat(string v)
        {
            try
            {
                return (float)System.Convert.ToDouble(v);
            }
            catch (System.FormatException nfe)
            {
                throw new DistributionFormatException("Not a float value: " + v, nfe);
            }
        }

        private static int parseInteger(string v)
        {
            try
            {
                return System.Convert.ToInt32(v);
            }
            catch (System.FormatException nfe)
            {
                throw new DistributionFormatException("Not an int value: " + v, nfe);
            }
        }

        private static long parseLong(string v)
        {
            try
            {
                return System.Convert.ToInt64(v);
            }
            catch (System.FormatException nfe)
            {
                throw new DistributionFormatException("Not a long value: " + v, nfe);
            }
        }

        private Distribution CreateConstantIntegerDistribution(string[] args)
        {
            if (args.Length != 1) throw invalidNumberOfArgumentsException(args.Length - 1, 1);
            return new ConstantIntegerDistribution(parseInteger(args[0]));
        }

        private Distribution CreateTriangularIntegerDistribution(string[] args)
        {
            switch (args.Length)
            {
                case 1:
                    return new TriangularIntegerDistribution(parseInteger(args[0]));
                case 2:
                    return new TriangularIntegerDistribution(parseInteger(args[0]), parseInteger(args[1]));
                case 3:
                    return new TriangularIntegerDistribution(parseInteger(args[0]), parseInteger(args[1]), parseFloat(args[2]));
                default:
                    throw invalidNumberOfArgumentsException(args.Length, 1, 2, 3);
            }
        }

        private Distribution CreateUniformIntegerDistribution(string[] args)
        {
            switch (args.Length)
            {
                case 1:
                    return new UniformIntegerDistribution(parseInteger(args[0]));
                case 2:
                    return new UniformIntegerDistribution(parseInteger(args[0]), parseInteger(args[1]));
                default:
                    throw invalidNumberOfArgumentsException(args.Length, 1, 2);
            }
        }

        private Distribution CreateConstantLongDistribution(string[] args)
        {
            if (args.Length != 1) throw invalidNumberOfArgumentsException(args.Length, 1);
            return new ConstantLongDistribution(parseLong(args[0]));
        }

        private Distribution CreateTriangularLongDistribution(string[] args)
        {
            switch (args.Length)
            {
                case 1:
                    return new TriangularLongDistribution(parseLong(args[0]));
                case 2:
                    return new TriangularLongDistribution(parseLong(args[0]), parseLong(args[1]));
                case 3:
                    return new TriangularLongDistribution(parseLong(args[0]), parseLong(args[1]), parseDouble(args[2]));
                default:
                    throw invalidNumberOfArgumentsException(args.Length, 1, 2, 3);
            }
        }

        private Distribution CreateUniformLongDistribution(string[] args)
        {
            switch (args.Length)
            {
                case 1:
                    return new UniformLongDistribution(parseLong(args[0]));
                case 2:
                    return new UniformLongDistribution(parseLong(args[0]), parseLong(args[1]));
                default:
                    throw invalidNumberOfArgumentsException(args.Length, 1, 2);
            }
        }

        private Distribution CreateConstantFloatDistribution(string[] args)
        {
            if (args.Length != 1) throw invalidNumberOfArgumentsException(args.Length, 1);
            return new ConstantFloatDistribution(parseFloat(args[0]));
        }

        private Distribution CreateGaussianFloatDistribution(string[] args)
        {
            if (args.Length != 2) throw invalidNumberOfArgumentsException(args.Length, 2);
            return new GaussianFloatDistribution(parseFloat(args[0]), parseFloat(args[1]));
        }

        private Distribution CreateTriangularFloatDistribution(string[] args)
        {
            switch (args.Length)
            {
                case 1:
                    return new TriangularFloatDistribution(parseFloat(args[0]));
                case 2:
                    return new TriangularFloatDistribution(parseFloat(args[0]), parseFloat(args[1]));
                case 3:
                    return new TriangularFloatDistribution(parseFloat(args[0]), parseFloat(args[1]), parseFloat(args[2]));
                default:
                    throw invalidNumberOfArgumentsException(args.Length, 1, 2, 3);
            }
        }

        private Distribution CreateUniformFloatDistribution(string[] args)
        {
            switch (args.Length)
            {
                case 1:
                    return new UniformFloatDistribution(parseFloat(args[0]));
                case 2:
                    return new UniformFloatDistribution(parseFloat(args[0]), parseFloat(args[1]));
                default:
                    throw invalidNumberOfArgumentsException(args.Length, 1, 2);
            }
        }

        private Distribution CreateConstantDoubleDistribution(string[] args)
        {
            if (args.Length != 1) throw invalidNumberOfArgumentsException(args.Length, 1);
            return new ConstantDoubleDistribution(parseDouble(args[0]));
        }

        private Distribution CreateGaussianDoubleDistribution(string[] args)
        {
            if (args.Length != 2) throw invalidNumberOfArgumentsException(args.Length, 2);
            return new GaussianDoubleDistribution(parseDouble(args[0]), parseDouble(args[1]));
        }

        private Distribution CreateTriangularDoubleDistribution(string[] args)
        {
            switch (args.Length)
            {
                case 1:
                    return new TriangularDoubleDistribution(parseDouble(args[0]));
                case 2:
                    return new TriangularDoubleDistribution(parseDouble(args[0]), parseDouble(args[1]));
                case 3:
                    return new TriangularDoubleDistribution(parseDouble(args[0]), parseDouble(args[1]), parseDouble(args[2]));
                default:
                    throw invalidNumberOfArgumentsException(args.Length, 1, 2, 3);
            }
        }

        private Distribution CreateUniformDoubleDistribution(string[] args)
        {
            switch (args.Length)
            {
                case 1:
                    return new UniformDoubleDistribution(parseDouble(args[0]));
                case 2:
                    return new UniformDoubleDistribution(parseDouble(args[0]), parseDouble(args[1]));
                default:
                    throw invalidNumberOfArgumentsException(args.Length, 1, 2);
            }
        }

        public Distribution toDistribution(string value, System.Type clazz)
        {
            string[] st = value.Split(new char[] { ',', ' ', '\t', '\f' });
            if (st[0].Length <= 1) throw new DistributionFormatException("Missing ditribution type or params");

            string type = st[0];
            string[] args = new string[type.Length - 1];
            System.Array.Copy(st, 1, args, 0, args.Length);
            Distribution distribution = null;

            if (clazz == typeof(IntegerDistribution))
            {
                switch (type)
                {
                    case "constant":
                        {
                            distribution = CreateConstantIntegerDistribution(args);
                            break;
                        }
                    case "triangular":
                        {
                            distribution = CreateTriangularIntegerDistribution(args);
                            break;
                        }
                    case "uniform":
                        {
                            distribution = CreateUniformIntegerDistribution(args);
                            break;
                        }
                    default:
                        break;
                }
            }
            else if (clazz == typeof(LongDistribution))
            {
                switch (type)
                {
                    case "constant":
                        {
                            distribution = CreateConstantLongDistribution(args);
                            break;
                        }
                    case "triangular":
                        {
                            distribution = CreateTriangularLongDistribution(args);
                            break;
                        }
                    case "uniform":
                        {
                            distribution = CreateUniformLongDistribution(args);
                            break;
                        }
                    default:
                        break;
                }
            }
            else if (clazz == typeof(FloatDistribution))
            {
                switch (type)
                {
                    case "constant":
                        {
                            distribution = CreateConstantFloatDistribution(args);
                            break;
                        }
                    case "gaussian":
                        {
                            distribution = CreateGaussianFloatDistribution(args);
                            break;
                        }
                    case "triangular":
                        {
                            distribution = CreateTriangularFloatDistribution(args);
                            break;
                        }
                    case "uniform":
                        {
                            distribution = CreateUniformFloatDistribution(args);
                            break;
                        }
                    default:
                        break;
                }
            }
            else if (clazz == typeof(DoubleDistribution))
            {
                switch (type)
                {
                    case "constant":
                        {
                            distribution = CreateConstantDoubleDistribution(args);
                            break;
                        }
                    case "gaussian":
                        {
                            distribution = CreateGaussianDoubleDistribution(args);
                            break;
                        }
                    case "triangular":
                        {
                            distribution = CreateTriangularDoubleDistribution(args);
                            break;
                        }
                    case "uniform":
                        {
                            distribution = CreateUniformDoubleDistribution(args);
                            break;
                        }
                    default:
                        break;
                }
            }

            if (distribution == null)
                throw new DistributionFormatException("Cannot create a '" + clazz.Name + "' of type '" + type + "'");

            return distribution;
        }

        private static DistributionFormatException invalidNumberOfArgumentsException(int found, params int[] expected)
        {
            string message = "Found " + found + " arguments in the distribution; expected ";
            if (expected.Length < 2)
                message += expected.Length;
            else
            {
                string sep = "";
                int i = 0;
                while (i < expected.Length - 1)
                {
                    message += sep + expected[i++];
                    sep = ", ";
                }
                message += " or " + expected[i];
            }
            return new DistributionFormatException(message);
        }
    }
}