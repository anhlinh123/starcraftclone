namespace StarcraftClone.AI.BehaviorTree
{
    /** A {@code BehaviorTreeLibrary} is a repository of behavior tree archetypes. Behavior tree archetypes never run. Indeed, they are
     * only cloned to create behavior tree instances that can run.
     * 
     * @author davebaol */
    public class BehaviorTreeLibrary
    {

        protected System.Collections.Generic.Dictionary<string, BehaviorTree> repository;
        protected BehaviorTreeParser parser;

        /** Creates a {@code BehaviorTreeLibrary} with the given resolver and debug level.
         * @param resolver the {@link FileHandleResolver}
         * @param parseDebugLevel the debug level the parser will use */
        public BehaviorTreeLibrary()
        {
            this.repository = new System.Collections.Generic.Dictionary<string, BehaviorTree>();
            this.parser = new BehaviorTreeParser();
        }

        /** Creates the root task of {@link BehaviorTree} for the specified reference.
         * @param treeReference the tree identifier, typically a path
         * @return the root task of the tree cloned from the archetype.
         * @throws SerializationException if the reference cannot be successfully parsed.
         * @throws TaskCloneException if the archetype cannot be successfully parsed. */
        public Task createRootTask(string treeReference)
        {
            return (Task)retrieveArchetypeTree(treeReference).getChild(0).cloneTask();
        }

        /** Creates the {@link BehaviorTree} for the specified reference.
         * @param treeReference the tree identifier, typically a path
         * @return the tree cloned from the archetype.
         * @throws SerializationException if the reference cannot be successfully parsed.
         * @throws TaskCloneException if the archetype cannot be successfully parsed. */
        public BehaviorTree createBehaviorTree(string treeReference)
        {
            return createBehaviorTree(treeReference, null);
        }

        /** Creates the {@link BehaviorTree} for the specified reference and blackboard object.
         * @param treeReference the tree identifier, typically a path
         * @param blackboard the blackboard object (it can be {@code null}).
         * @return the tree cloned from the archetype.
         * @throws SerializationException if the reference cannot be successfully parsed.
         * @throws TaskCloneException if the archetype cannot be successfully parsed. */
        public BehaviorTree createBehaviorTree(string treeReference, object blackboard)
        {
            BehaviorTree bt = (BehaviorTree)retrieveArchetypeTree(treeReference).cloneTask();
            bt.setObject(blackboard);
            return bt;
        }

        /** Retrieves the archetype tree from the library. If the library doesn't contain the archetype tree it is loaded and added to
         * the library.
         * @param treeReference the tree identifier, typically a path
         * @return the archetype tree.
         * @throws SerializationException if the reference cannot be successfully parsed. */
        protected BehaviorTree retrieveArchetypeTree(string treeReference)
        {
            BehaviorTree archetypeTree = null;
            if (repository.TryGetValue(treeReference, out archetypeTree) == false)
            {
                //			if (assetManager != null) {
                //				// TODO: fix me!!!
                //				// archetypeTree = assetManager.load(name, BehaviorTree.class, null);
                //				repository.put(treeReference, archetypeTree);
                //				return null;
                //			}
                archetypeTree = parser.parse(FileSystem.FileManager.OpenFile(treeReference), null);
                registerArchetypeTree(treeReference, archetypeTree);
            }
            return archetypeTree;
        }

        /** Registers the {@link BehaviorTree} archetypeTree with the specified reference. Existing archetypes in the repository with
         * the same treeReference will be replaced.
         * @param treeReference the tree identifier, typically a path.
         * @param archetypeTree the archetype tree.
         * @throws IllegalArgumentException if the archetypeTree is null */
        public void registerArchetypeTree(string treeReference, BehaviorTree archetypeTree)
        {
            if (archetypeTree == null)
            {
                throw new System.ArgumentException("The registered archetype must not be null.");
            }
            repository.Add(treeReference, archetypeTree);
        }

        /** Returns {@code true} if an archetype tree with the specified reference is registered in this library.
         * @param treeReference the tree identifier, typically a path.
         * @return {@code true} if the archetype is registered already; {@code false} otherwise. */
        public bool hasArchetypeTree(string treeReference)
        {
            return repository.ContainsKey(treeReference);
        }

    }
}