namespace StarcraftClone.AI.BehaviorTree
{
    /** A {@code SingleRunningChildBranch} task is a branch task that supports only one running child at a time.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation
     * @author davebaol */
    public abstract class SingleRunningChildBranch : BranchTask
    {

        /** The child in the running status or {@code null} if no child is running. */
        protected Task runningChild;

        /** The index of the child currently processed. */
        protected int currentChildIndex;

        /** Array of random children. If it's {@code null} this task is deterministic. */
        protected Task[] randomChildren;

        /** Creates a {@code SingleRunningChildBranch} task with no children */
        public SingleRunningChildBranch()
            : base()
        {
        }

        /** Creates a {@code SingleRunningChildBranch} task with a list of children
         * 
         * @param tasks list of this task's children, can be empty */
        public SingleRunningChildBranch(System.Collections.Generic.List<Task> tasks)
            : base(tasks)
        {
        }

        public override void childRunning(Task task, Task reporter)
        {
            runningChild = task;
            running(); // Return a running status when a child says it's running
        }

        public override void childSuccess(Task task)
        {
            this.runningChild = null;
        }

        public override void childFail(Task task)
        {
            this.runningChild = null;
        }

        public override void run() {
		if (runningChild != null) {
			runningChild.run();
		} else {
			if (currentChildIndex < children.Count) {
				if (randomChildren != null) {
					int last = children.Count;
					if (currentChildIndex < last) {
						// Random swap
						int otherChildIndex = RandomHelper.Next(currentChildIndex, last);
						Task tmp = randomChildren[currentChildIndex];
						randomChildren[currentChildIndex] = randomChildren[otherChildIndex];
						randomChildren[otherChildIndex] = tmp;
					}
					runningChild = randomChildren[currentChildIndex];
				} else {
					runningChild = children[currentChildIndex];
				}
				runningChild.setControl(this);
				runningChild.start();
				if (!runningChild.checkGuard(this))
					runningChild.fail();
				run();
			} else {
				// Should never happen; this case must be handled by subclasses in childXXX methods
			}
		}
	}

        public override void start()
        {
            this.currentChildIndex = 0;
            runningChild = null;
        }

        protected override void cancelRunningChildren(int startIndex)
        {
            base.cancelRunningChildren(startIndex);
            runningChild = null;
        }

        public override void reset()
        {
            base.reset();
            this.currentChildIndex = 0;
            this.runningChild = null;
            this.randomChildren = null;
        }

        protected override Task copyTo(Task task)
        {
            SingleRunningChildBranch branch = (SingleRunningChildBranch)task;
            branch.randomChildren = null;

            return base.copyTo(task);
        }

        protected Task[] createRandomChildren()
        {
            Task[] rndChildren = new Task[children.Count];
            System.Array.Copy(children.ToArray(), 0, rndChildren, 0, children.Count);
            return rndChildren;
        }

    }
}