﻿namespace StarcraftClone.AI.BehaviorTree
{
    public class Action : LeafTask
    {
        private string name;

        public Action(string name)
        {
            this.name = name;
        }

        public Action()
        {
            this.name = null;
        }

        public override Status execute()
        {
            if (tree != null)
            {
                BehaviorTree.TreeFunction function = tree.getFunction(name);
                if (function != null)
                    return function();
            }
            return Status.SUCCEEDED;
        }

        protected override Task copyTo(Task task)
        {
            Action action = (Action)task;
            action.name = name;
            return action;
        }
    }
}
