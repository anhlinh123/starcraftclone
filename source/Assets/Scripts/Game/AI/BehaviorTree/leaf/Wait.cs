namespace StarcraftClone.AI.BehaviorTree
{
    /** {@code Wait} is a leaf that keeps running for the specified amount of time then succeeds.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author davebaol */
    public class Wait : LeafTask
    {

        /** Mandatory task attribute specifying the random distribution that determines the timeout in seconds. */
        [TaskProperty("", true)]
        public FloatDistribution seconds { get; set; }

        private float startTime;
        private float timeout;

        /** Creates a {@code Wait} task that immediately succeeds. */
        public Wait()
            : this(ConstantFloatDistribution.ZERO)
        {
        }

        /** Creates a {@code Wait} task running for the specified number of seconds.
         * 
         * @param seconds the number of seconds to wait for */
        public Wait(float seconds)
            : this(new ConstantFloatDistribution(seconds))
        {
        }

        /** Creates a {@code Wait} task running for the specified number of seconds.
         * 
         * @param seconds the random distribution determining the number of seconds to wait for */
        public Wait(FloatDistribution seconds)
        {
            this.seconds = seconds;
        }

        /** Draws a value from the distribution that determines the seconds to wait for.
         * <p>
         * This method is called when the task is entered. Also, this method internally calls {@link Timepiece#getTime()
         * GdxAI.getTimepiece().getTime()} to get the current AI time. This means that
         * <ul>
         * <li>if you forget to {@link Timepiece#update(float) update the timepiece} this task will keep running indefinitely.</li>
         * <li>the timepiece should be updated before this task runs.</li>
         * </ul> */
        public override void start()
        {
            timeout = seconds.nextFloat();
            startTime = Timepiece.getTime();
        }

        /** Executes this {@code Wait} task.
         * @return {@link Status#SUCCEEDED} if the specified timeout has expired; {@link Status#RUNNING} otherwise. */
        public override Status execute()
        {
            return Timepiece.getTime() - startTime < timeout ? Status.RUNNING : Status.SUCCEEDED;
        }

        protected override Task copyTo(Task task)
        {
            ((Wait)task).seconds = seconds;
            return task;
        }

    }
}