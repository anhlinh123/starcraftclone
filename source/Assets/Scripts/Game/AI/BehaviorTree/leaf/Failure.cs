namespace StarcraftClone.AI.BehaviorTree
{
    /** {@code Failure} is a leaf that immediately fails.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author davebaol */
    public class Failure : LeafTask
    {

        /** Creates a {@code Failure} task. */
        public Failure()
        {
        }

        /** Executes this {@code Failure} task.
         * @return {@link Status#FAILED}. */
        public override Status execute()
        {
            return Status.FAILED;
        }

        protected override Task copyTo(Task task)
        {
            return task;
        }

    }
}