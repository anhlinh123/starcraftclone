namespace StarcraftClone.AI.BehaviorTree
{
    /** {@code Success} is a leaf that immediately succeeds.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author davebaol */
    public class Success : LeafTask
    {

        /** Creates a {@code Success} task. */
        public Success()
        {
        }

        /** Executes this {@code Success} task.
         * @return {@link Status#SUCCEEDED}. */
        public override Status execute()
        {
            return Status.SUCCEEDED;
        }

        protected override Task copyTo(Task task)
        {
            return task;
        }

    }
}