namespace StarcraftClone.AI.BehaviorTree
{

    /** A {@code RandomSequence} is a sequence task's variant that runs its children in a random order.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation */
    public class RandomSequence : Sequence
    {

        /** Creates a {@code RandomSequence} branch with no children. */
        public RandomSequence()
            : base()
        {
        }

        /** Creates a {@code RandomSequence} branch with the given children.
         * 
         * @param tasks the children of this task */
        public RandomSequence(System.Collections.Generic.List<Task> tasks)
            : base(tasks)
        {
        }

        /** Creates a {@code RandomSequence} branch with the given children.
         * 
         * @param tasks the children of this task */
        public RandomSequence(params Task[] tasks)
            : base(new System.Collections.Generic.List<Task>(tasks))
        {
        }

        public override void start()
        {
            base.start();
            if (randomChildren == null) randomChildren = createRandomChildren();
        }
    }
}