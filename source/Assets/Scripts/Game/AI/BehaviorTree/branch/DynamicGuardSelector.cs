namespace StarcraftClone.AI.BehaviorTree
{

    /** A {@code DynamicGuardSelector} is a branch task that executes the first child whose guard is evaluated to {@code true}. At
     * every AI cycle, the children's guards are re-evaluated, so if the guard of the running child is evaluated to {@code false}, it
     * is cancelled, and the child with the highest priority starts running. The {@code DynamicGuardSelector} task finishes when no
     * guard is evaluated to {@code true} (thus failing) or when its active child finishes (returning the active child's termination
     * status).
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author davebaol */
    public class DynamicGuardSelector : BranchTask
    {

        /** The child in the running status or {@code null} if no child is running. */
        protected Task runningChild;

        /** Creates a {@code DynamicGuardSelector} branch with no children. */
        public DynamicGuardSelector()
            : base()
        {
        }

        /** Creates a {@code DynamicGuardSelector} branch with the given children.
         * 
         * @param tasks the children of this task */
        public DynamicGuardSelector(params Task[] tasks)
            : base(new System.Collections.Generic.List<Task>(tasks))
        {
        }

        /** Creates a {@code DynamicGuardSelector} branch with the given children.
         * 
         * @param tasks the children of this task */
        public DynamicGuardSelector(System.Collections.Generic.List<Task> tasks)
            : base(tasks)
        {
        }

        public override void childRunning(Task task, Task reporter)
        {
            runningChild = task;
            running(); // Return a running status when a child says it's running
        }

        public override void childSuccess(Task task)
        {
            this.runningChild = null;
            success();
        }

        public override void childFail(Task task)
        {
            this.runningChild = null;
            fail();
        }

        public override void run()
        {
            // Check guards
            Task childToRun = null;
            for (int i = 0, n = children.Count; i < n; i++)
            {
                Task child = children[i];
                if (child.checkGuard(this))
                {
                    childToRun = child;
                    break;
                }
            }

            if (runningChild != null && runningChild != childToRun)
            {
                runningChild.cancel();
                runningChild = null;
            }
            if (childToRun == null)
            {
                fail();
            }
            else
            {
                if (runningChild == null)
                {
                    runningChild = childToRun;
                    runningChild.setControl(this);
                    runningChild.start();
                }
                runningChild.run();
            }
        }

        public override void reset()
        {
            base.reset();
            this.runningChild = null;
        }

        protected override Task copyTo(Task task)
        {
            DynamicGuardSelector branch = (DynamicGuardSelector)task;
            branch.runningChild = null;

            return base.copyTo(task);
        }

    }
}