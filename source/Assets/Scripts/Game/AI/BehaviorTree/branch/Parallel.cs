namespace StarcraftClone.AI.BehaviorTree
{

    /** A {@code Parallel} is a special branch task that starts or resumes all children every single time. The actual behavior of
     * parallel task depends on its {@link #policy}:
     * <ul>
     * <li>{@link Policy#Sequence}: the parallel task fails as soon as one child fails; if all children succeed, then the parallel
     * task succeeds. This is the default policy.</li>
     * <li>{@link Policy#Selector}: the parallel task succeeds as soon as one child succeeds; if all children fail, then the parallel
     * task fails.</li>
     * </ul>
     * 
     * The typical use case: make the game entity react on event while sleeping or wandering.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation
     * @author davebaol */
    public class Parallel : BranchTask
    {

        /** Optional task attribute specifying the parallel policy (defaults to {@link Policy#Sequence}) */
        [TaskProperty]
        public Policy policy { get; set; }

        private bool noRunningTasks;
        private bool? lastResult;
        private int currentChildIndex;

        /** Creates a parallel task with sequence policy and no children */
        public Parallel()
            : this(new System.Collections.Generic.List<Task>())
        {
        }

        /** Creates a parallel task with sequence policy and the given children
         * @param tasks the children */
        public Parallel(params Task[] tasks)
            : base(new System.Collections.Generic.List<Task>(tasks))
        {
        }

        /** Creates a parallel task with sequence policy and the given children
         * @param tasks the children */
        public Parallel(System.Collections.Generic.List<Task> tasks)
            : this(Policy.Sequence, tasks)
        {
        }

        /** Creates a parallel task with the given policy and no children
         * @param policy the policy */
        public Parallel(Policy policy)
            : this(policy, new System.Collections.Generic.List<Task>())
        {
        }

        /** Creates a parallel task with the given policy and children
         * @param policy the policy
         * @param tasks the children */
        public Parallel(Policy policy, params Task[] tasks)
            : this(policy, new System.Collections.Generic.List<Task>(tasks))
        {
        }

        /** Creates a parallel task with the given policy and children
         * @param policy the policy
         * @param tasks the children */
        public Parallel(Policy policy, System.Collections.Generic.List<Task> tasks)
            : base(tasks)
        {
            this.policy = policy;
            noRunningTasks = true;
        }

        public override void run()
        {
            noRunningTasks = true;
            lastResult = null;
            for (currentChildIndex = 0; currentChildIndex < children.Count; currentChildIndex++)
            {
                Task child = children[currentChildIndex];
                if (child.getStatus() == Status.RUNNING)
                {
                    child.run();
                }
                else
                {
                    child.setControl(this);
                    child.start();
                    if (child.checkGuard(this))
                        child.run();
                    else
                        child.fail();
                }

                if (lastResult != null)
                { // Current child has finished either with success or fail
                    cancelRunningChildren(noRunningTasks ? currentChildIndex + 1 : 0);
                    if (lastResult == true)
                        success();
                    else
                        fail();
                    return;
                }
            }
            running();
        }

        public override void childRunning(Task task, Task reporter)
        {
            noRunningTasks = false;
        }

        public override void childSuccess(Task runningTask)
        {
            lastResult = policy.onChildSuccess(this);
        }

        public override void childFail(Task runningTask)
        {
            lastResult = policy.onChildFail(this);
        }

        public override void reset()
        {
            base.reset();
            noRunningTasks = true;
        }

        protected override Task copyTo(Task task)
        {
            Parallel parallel = (Parallel)task;
            parallel.policy = policy; // no need to clone since it is immutable

            return base.copyTo(task);
        }

        /** The enumeration of the policies supported by the {@link Parallel} task. */
        public sealed class Policy
        {
            /** The sequence policy makes the {@link Parallel} task fail as soon as one child fails; if all children succeed, then the
             * parallel task succeeds. This is the default policy. */
            public static Policy Sequence = new Policy(
                (Parallel parallel) => parallel.noRunningTasks && parallel.currentChildIndex == parallel.children.Count - 1 ? true : (bool?)null,
                (Parallel parallel) => false
                );
            /** The selector policy makes the {@link Parallel} task succeed as soon as one child succeeds; if all children fail, then the
             * parallel task fails. */
            public static Policy Selector = new Policy(
                (Parallel parallel) => true,
                (Parallel parallel) => parallel.noRunningTasks && parallel.currentChildIndex == parallel.children.Count - 1 ? false : (bool?)null
            );

            delegate bool? OnChildResult(Parallel parallel);
            OnChildResult success, fail;
            Policy(OnChildResult success, OnChildResult fail) { this.success = success; this.fail = fail; }
            /** Called by parallel task each time one of its children succeeds.
             * @param parallel the parallel task
             * @return {@code Boolean.TRUE} if parallel must succeed, {@code Boolean.FALSE} if parallel must fail and {@code null} if
             *         parallel must keep on running. */
            public bool? onChildSuccess(Parallel parallel) { return success(parallel); }

            /** Called by parallel task each time one of its children fails.
             * @param parallel the parallel task
             * @return {@code Boolean.TRUE} if parallel must succeed, {@code Boolean.FALSE} if parallel must fail and {@code null} if
             *         parallel must keep on running. */
            public bool? onChildFail(Parallel parallel) { return fail(parallel); }

        }
    }
}