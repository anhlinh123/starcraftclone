namespace StarcraftClone.AI.BehaviorTree
{

    /** A {@code Sequence} is a branch task that runs every children until one of them fails. If a child task succeeds, the selector
     * will start and run the next child task.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation */
    public class Sequence : SingleRunningChildBranch
    {

        /** Creates a {@code Sequence} branch with no children. */
        public Sequence()
            : base()
        {
        }

        /** Creates a {@code Sequence} branch with the given children.
         * 
         * @param tasks the children of this task */
        public Sequence(System.Collections.Generic.List<Task> tasks)
            : base(tasks)
        {
        }

        /** Creates a {@code Sequence} branch with the given children.
         * 
         * @param tasks the children of this task */
        public Sequence(params Task[] tasks)
            : base(new System.Collections.Generic.List<Task>(tasks))
        {
        }

        public override void childSuccess(Task runningTask)
        {
            base.childSuccess(runningTask);
            if (++currentChildIndex < children.Count)
            {
                run(); // Run next child
            }
            else
            {
                success(); // All children processed, return success status
            }
        }

        public override void childFail(Task runningTask)
        {
            base.childFail(runningTask);
            fail(); // Return failure status when a child says it failed
        }

    }
}