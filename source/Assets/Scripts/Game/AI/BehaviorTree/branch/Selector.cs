namespace StarcraftClone.AI.BehaviorTree
{

    /** A {@code Selector} is a branch task that runs every children until one of them succeeds. If a child task fails, the selector
     * will start and run the next child task.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation */
    public class Selector : SingleRunningChildBranch
    {

        /** Creates a {@code Selector} branch with no children. */
        public Selector()
            : base()
        {
        }

        /** Creates a {@code Selector} branch with the given children.
         * 
         * @param tasks the children of this task */
        public Selector(params Task[] tasks)
            : base(new System.Collections.Generic.List<Task>())
        {
        }

        /** Creates a {@code Selector} branch with the given children.
         * 
         * @param tasks the children of this task */
        public Selector(System.Collections.Generic.List<Task> tasks)
            : base(tasks)
        {
        }

        public override void childFail(Task runningTask)
        {
            base.childFail(runningTask);
            if (++currentChildIndex < children.Count)
            {
                run(); // Run next child
            }
            else
            {
                fail(); // All children processed, return failure status
            }
        }

        public override void childSuccess(Task runningTask)
        {
            base.childSuccess(runningTask);
            success(); // Return success status when a child says it succeeded
        }

    }
}