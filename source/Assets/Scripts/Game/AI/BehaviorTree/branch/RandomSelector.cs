namespace StarcraftClone.AI.BehaviorTree
{

    /** A {@code RandomSelector} is a selector task's variant that runs its children in a random order.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation */
    public class RandomSelector : Selector
    {

        /** Creates a {@code RandomSelector} branch with no children. */
        public RandomSelector()
            : base()
        {
        }

        /** Creates a {@code RandomSelector} branch with the given children.
         * 
         * @param tasks the children of this task */
        public RandomSelector(params Task[] tasks)
            : base(new System.Collections.Generic.List<Task>(tasks))
        {
        }

        /** Creates a {@code RandomSelector} branch with the given children.
         * 
         * @param tasks the children of this task */
        public RandomSelector(System.Collections.Generic.List<Task> tasks)
            : base(tasks)
        {
        }

        public override void start()
        {
            base.start();
            if (randomChildren == null) randomChildren = createRandomChildren();
        }
    }
}