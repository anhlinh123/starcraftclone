namespace StarcraftClone.AI.BehaviorTree
{
    [System.AttributeUsage(System.AttributeTargets.Class, Inherited = true)]
    public class TaskConstraint : System.Attribute
    {
        public readonly int minChildren = 0;
        public readonly int maxChildren = System.Int32.MaxValue;
        public TaskConstraint(int minChildren = 0, int maxChildren = System.Int32.MaxValue)
        {
            this.minChildren = minChildren;
            this.maxChildren = maxChildren;
        }
    }
}