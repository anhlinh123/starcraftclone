namespace StarcraftClone.AI.BehaviorTree
{
	[System.AttributeUsage(System.AttributeTargets.Property)]
    public class TaskProperty : System.Attribute
    {
        public readonly string name = "";
        public readonly bool required = false;
        public TaskProperty(string name = "", bool required = false)
        {
            this.name = name;
            this.required = required;
        }
    }
}
