namespace StarcraftClone.AI.BehaviorTree
{

    /** The behavior tree itself.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation
     * @author davebaol */
    public class BehaviorTree : Task
    {

        private Task rootTask;
        private object _object;
        internal GuardEvaluator guardEvaluator;

        public System.Collections.Generic.List<Listener> listeners;

        public delegate Status TreeFunction();
        private System.Collections.Generic.Dictionary<string, TreeFunction> functions;

        /** Creates a {@code BehaviorTree} with no root task and no blackboard object. Both the root task and the blackboard object must
         * be set before running this behavior tree, see {@link #addChild(Task) addChild()} and {@link #setObject(Object) setObject()}
         * respectively. */
        public BehaviorTree()
            : this(null, null)
        {

        }

        /** Creates a behavior tree with a root task and no blackboard object. Both the root task and the blackboard object must be set
         * before running this behavior tree, see {@link #addChild(Task) addChild()} and {@link #setObject(Object) setObject()}
         * respectively.
         * 
         * @param rootTask the root task of this tree. It can be {@code null}. */
        public BehaviorTree(Task rootTask)
            : this(rootTask, null)
        {

        }

        /** Creates a behavior tree with a root task and a blackboard object. Both the root task and the blackboard object must be set
         * before running this behavior tree, see {@link #addChild(Task) addChild()} and {@link #setObject(Object) setObject()}
         * respectively.
         * 
         * @param rootTask the root task of this tree. It can be {@code null}.
         * @param object the blackboard. It can be {@code null}. */
        public BehaviorTree(Task rootTask, object _object)
        {
            this.rootTask = rootTask;
            this._object = _object;
            this.tree = this;
            this.guardEvaluator = new GuardEvaluator(this);
        }

        /** Returns the blackboard object of this behavior tree. */
        public override object getObject()
        {
            return _object;
        }

        /** Sets the blackboard object of this behavior tree.
         * 
         * @param object the new blackboard */
        public void setObject(object _object)
        {
            this._object = _object;
        }

        /** This method will add a child, namely the root, to this behavior tree.
         * 
         * @param child the root task to add
         * @return the index where the root task has been added (always 0).
         * @throws InvalidOperationException if the root task is already set. */
        protected override int addChildToTask(Task child)
        {
            if (this.rootTask != null) throw new System.InvalidOperationException("A behavior tree cannot have more than one root task");
            this.rootTask = child;
            return 0;
        }

        public override int getChildCount()
        {
            return rootTask == null ? 0 : 1;
        }

        public override Task getChild(int i)
        {
            if (i == 0 && rootTask != null) return rootTask;
            throw new System.IndexOutOfRangeException("index can't be >= size: " + i + " >= " + getChildCount());
        }

        public override void childRunning(Task runningTask, Task reporter)
        {
            running();
        }

        public override void childFail(Task runningTask)
        {
            fail();
        }

        public override void childSuccess(Task runningTask)
        {
            success();
        }

        /** This method should be called when game entity needs to make decisions: call this in game loop or after a fixed time slice if
         * the game is real-time, or on entity's turn if the game is turn-based */
        public void step()
        {
            if (rootTask.status == Status.RUNNING)
            {
                rootTask.run();
            }
            else
            {
                rootTask.setControl(this);
                rootTask.start();
                if (rootTask.checkGuard(this))
                    rootTask.run();
                else
                    rootTask.fail();
            }
        }

        public override void run()
        {
        }

        public override void reset()
        {
            base.reset();
            tree = this;
        }

        protected override Task copyTo(Task task)
        {
            BehaviorTree tree = (BehaviorTree)task;
            tree.rootTask = rootTask.cloneTask();

            return task;
        }

        public void addFunction(string name, TreeFunction function)
        {
            if (functions == null)
                functions = new System.Collections.Generic.Dictionary<string, TreeFunction>();
            if (functions.ContainsKey(name))
            {
                functions[name] = function;
            }
            else
                functions.Add(name, function);
        }

        public TreeFunction getFunction(string name)
        {
            TreeFunction function = null;
            if (functions != null)
                functions.TryGetValue(name, out function);
            return function;
        }

        public void removeFunctions()
        {
            if (functions != null)
                functions.Clear();
        }

        public void addListener(Listener listener)
        {
            if (listeners == null) listeners = new System.Collections.Generic.List<Listener>();
            listeners.Add(listener);
        }

        public void removeListener(Listener listener)
        {
            if (listeners != null) listeners.Remove(listener);
        }

        public void removeListeners()
        {
            if (listeners != null) listeners.Clear();
        }

        public void notifyStatusUpdated(Task task, Status previousStatus)
        {
            foreach (Listener listener in listeners)
            {
                listener.statusUpdated(task, previousStatus);
            }
        }

        public void notifyChildAdded(Task task, int index)
        {
            foreach (Listener listener in listeners)
            {
                listener.childAdded(task, index);
            }
        }

        internal sealed class GuardEvaluator : Task
        {

            // No argument constructor useful for Kryo serialization
            public GuardEvaluator()
            {
            }

            public GuardEvaluator(BehaviorTree tree)
            {
                this.tree = tree;
            }

            protected override int addChildToTask(Task child)
            {
                return 0;
            }

            public override int getChildCount()
            {
                return 0;
            }

            public override Task getChild(int i)
            {
                return null;
            }

            public override void run()
            {
            }

            public override void childSuccess(Task task)
            {
            }

            public override void childFail(Task task)
            {
            }

            public override void childRunning(Task runningTask, Task reporter)
            {
            }

            protected override Task copyTo(Task task)
            {
                return null;
            }

        }

        /** The listener interface for receiving task events. The class that is interested in processing a task event implements this
         * interface, and the object created with that class is registered with a behavior tree, using the
         * {@link BehaviorTree#addListener(Listener)} method. When a task event occurs, the corresponding method is invoked.
         *
         * @param <E> type of the blackboard object that tasks use to read or modify game state
         * 
         * @author davebaol */
        public interface Listener
        {

            /** This method is invoked when the task status is set. This does not necessarily mean that the status has changed.
             * @param task the task whose status has been set
             * @param previousStatus the task's status before the update */
            void statusUpdated(Task task, Status previousStatus);

            /** This method is invoked when a child task is added to the children of a parent task.
             * @param task the parent task of the newly added child
             * @param index the index where the child has been added */
            void childAdded(Task task, int index);
        }
    }
}