namespace StarcraftClone.AI.BehaviorTree
{

    /** A {@code LeafTask} is a terminal task of a behavior tree, contains action or condition logic, can not have any child.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation
     * @author davebaol */
    [TaskConstraint(0, 0)]
    public abstract class LeafTask : Task
    {

        /** Creates a leaf task. */
        public LeafTask()
        {
        }

        /** This method contains the update logic of this leaf task. The actual implementation MUST return one of {@link Status#RUNNING}
         * , {@link Status#SUCCEEDED} or {@link Status#FAILED}. Other return values will cause an {@code IllegalStateException}.
         * @return the status of this leaf task */
        public abstract Status execute();

        /** This method contains the update logic of this task. The implementation delegates the {@link #execute()} method. */
        public override sealed void run()
        {
            Status result = execute();
            switch (result)
            {
                case Status.SUCCEEDED:
                    success();
                    return;
                case Status.FAILED:
                    fail();
                    return;
                case Status.RUNNING:
                    running();
                    return;
                default:
                    throw new System.InvalidOperationException("Invalid status '" + result + "' returned by the execute method");
            }
        }

        /** Always throws {@code IllegalStateException} because a leaf task cannot have any children. */
        protected override int addChildToTask(Task child)
        {
            throw new System.InvalidOperationException("A leaf task cannot have any children");
        }

        public override int getChildCount()
        {
            return 0;
        }

        public override Task getChild(int i)
        {
            throw new System.IndexOutOfRangeException("A leaf task can not have any child");
        }

        public override sealed void childRunning(Task runningTask, Task reporter)
        {
        }

        public override sealed void childFail(Task runningTask)
        {
        }

        public override sealed void childSuccess(Task runningTask)
        {
        }

    }
}