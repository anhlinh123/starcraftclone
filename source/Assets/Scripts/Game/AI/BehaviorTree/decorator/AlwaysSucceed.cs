namespace StarcraftClone.AI.BehaviorTree
{
    /** An {@code AlwaysSucceed} decorator will succeed no matter the wrapped task succeeds or fails.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation */
    public class AlwaysSucceed : Decorator
    {

        /** Creates an {@code AlwaysSucceed} decorator with no child. */
        public AlwaysSucceed()
        {
        }

        /** Creates an {@code AlwaysSucceed} decorator with the given child.
         * 
         * @param task the child task to wrap */
        public AlwaysSucceed(Task task)
            : base(task)
        {
        }

        public override void childFail(Task runningTask)
        {
            childSuccess(runningTask);
        }

    }
}