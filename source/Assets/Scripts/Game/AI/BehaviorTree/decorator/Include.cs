namespace StarcraftClone.AI.BehaviorTree
{
    /** An {@code Include} decorator grafts a subtree. When the subtree is grafted depends on the value of the {@link #lazy} attribute:
     * at clone-time if is {@code false}, at run-time if is {@code true}.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author davebaol
     * @author implicit-invocation */
    [TaskConstraint(0, 0)]
    public class Include : Decorator
    {

        /** Mandatory task attribute indicating the path of the subtree to include. */
        [TaskProperty("", true)]
        public string subtree { get; set; }

        /** Optional task attribute indicating whether the subtree should be included at clone-time ({@code false}, the default) or at
         * run-time ({@code true}). */
        [TaskProperty]
        public bool lazy { get; set; }

        /** Creates a non-lazy {@code Include} decorator without specifying the subtree. */
        public Include()
        {
        }

        /** Creates a non-lazy {@code Include} decorator for the specified subtree.
         * @param subtree the subtree reference, usually a path */
        public Include(string subtree)
        {
            this.subtree = subtree;
        }

        /** Creates an eager or lazy {@code Include} decorator for the specified subtree.
         * @param subtree the subtree reference, usually a path
         * @param lazy whether inclusion should happen at clone-time (false) or at run-time (true) */
        public Include(string subtree, bool lazy)
        {
            this.subtree = subtree;
            this.lazy = lazy;
        }

        /** The first call of this method lazily sets its child to the referenced subtree created through the
         * {@link BehaviorTreeLibraryManager}. Subsequent calls do nothing since the child has already been set. A
         * {@link UnsupportedOperationException} is thrown if this {@code Include} is eager.
         * 
         * @throws UnsupportedOperationException if this {@code Include} is eager */
        public override void start()
        {
            if (!lazy)
                throw new System.NotSupportedException("A non-lazy " + GetType().Name + " isn't meant to be run!");

            if (child == null)
            {
                // Lazy include is grafted at run-time
                addChild(createSubtreeRootTask());
            }
        }

        /** Returns a clone of the referenced subtree if this {@code Import} is eager; otherwise returns a clone of itself. */
        public override Task cloneTask()
        {
            if (lazy) return base.cloneTask();

            // Non lazy include is grafted at clone-time
            return createSubtreeRootTask();
        }

        /** Copies this {@code Include} to the given task. A {@link TaskCloneException} is thrown if this {@code Include} is eager.
         * @param task the task to be filled
         * @return the given task for chaining
         * @throws TaskCloneException if this {@code Include} is eager. */
        protected override Task copyTo(Task task)
        {
            if (!lazy) throw new TaskCloneException("A non-lazy " + GetType().Name + " should never be copied.");

            Include include = (Include)task;
            include.subtree = subtree;
            include.lazy = lazy;

            return task;
        }

        private Task createSubtreeRootTask()
        {
            return BehaviorTreeLibraryManager.getInstance().createRootTask(subtree);
        }
    }
}