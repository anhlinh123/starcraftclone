namespace StarcraftClone.AI.BehaviorTree
{
    /** An {@code Invert} decorator will succeed if the wrapped task fails and will fail if the wrapped task succeeds.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation */
    public class Invert : Decorator
    {

        /** Creates an {@code Invert} decorator with no child. */
        public Invert()
        {
        }

        /** Creates an {@code Invert} decorator with the given child.
         * 
         * @param task the child task to wrap */
        public Invert(Task task)
            : base(task)
        {
        }

        public override void childSuccess(Task runningTask)
        {
            base.childFail(runningTask);
        }

        public override void childFail(Task runningTask)
        {
            base.childSuccess(runningTask);
        }

    }
}