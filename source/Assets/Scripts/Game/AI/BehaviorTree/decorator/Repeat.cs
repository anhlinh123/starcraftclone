namespace StarcraftClone.AI.BehaviorTree
{
    /** A {@code Repeat} decorator will repeat the wrapped task a certain number of times, possibly infinite. This task always succeeds
     * when reaches the specified number of repetitions.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation */
    public class Repeat : LoopDecorator
    {

        /** Optional task attribute specifying the integer distribution that determines how many times the wrapped task must be
         * repeated. Defaults to {@link ConstantIntegerDistribution#NEGATIVE_ONE} which indicates an infinite number of repetitions.
         * 
         * @see #start() */
        [TaskProperty]
        public IntegerDistribution times { get; set; }

        private int count;

        /** Creates an infinite repeat decorator with no child task. */
        public Repeat()
            : this(null)
        {
        }

        /** Creates an infinite repeat decorator that wraps the given task.
         * 
         * @param child the task that will be wrapped */
        public Repeat(Task child)
            : this(ConstantIntegerDistribution.NEGATIVE_ONE, child)
        {
        }

        /** Creates a repeat decorator that executes the given task the number of times (possibly infinite) determined by the given
         * distribution. The number of times is drawn from the distribution by the {@link #start()} method. Any negative value means
         * forever.
         * 
         * @param times the integer distribution specifying how many times the child must be repeated.
         * @param child the task that will be wrapped */
        public Repeat(IntegerDistribution times, Task child)
            : base(child)
        {
            this.times = times;
        }

        /** Draws a value from the distribution that determines how many times the wrapped task must be repeated. Any negative value
         * means forever.
         * <p>
         * This method is called when the task is entered. */
        public override void start()
        {
            count = times.nextInt();
        }
        public override bool condition()
        {
            return loop && count != 0;
        }

        public override void childSuccess(Task runningTask)
        {
            if (count > 0) count--;
            if (count == 0)
            {
                base.childSuccess(runningTask);
                loop = false;
            }
            else
                loop = true;
        }

        public override void childFail(Task runningTask)
        {
            childSuccess(runningTask);
        }

        protected override Task copyTo(Task task)
        {
            Repeat repeat = (Repeat)task;
            repeat.times = times; // no need to clone since it is immutable

            return base.copyTo(task);
        }
    }
}
