namespace StarcraftClone.AI.BehaviorTree
{
    /** An {@code AlwaysFail} decorator will fail no matter the wrapped task fails or succeeds.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation */
    public class AlwaysFail : Decorator
    {

        /** Creates an {@code AlwaysFail} decorator with no child. */
        public AlwaysFail()
        {
        }

        /** Creates an {@code AlwaysFail} decorator with the given child.
         * 
         * @param task the child task to wrap */
        public AlwaysFail(Task task)
            : base(task)
        {
        }

        public override void childSuccess(Task runningTask)
        {
            childFail(runningTask);
        }

    }
}