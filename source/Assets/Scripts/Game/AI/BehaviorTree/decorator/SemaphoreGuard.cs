namespace StarcraftClone.AI.BehaviorTree
{
    /*
import com.badlogic.gdx.ai.btree.Decorator;
import com.badlogic.gdx.ai.btree.Task;
import com.badlogic.gdx.ai.btree.annotation.TaskAttribute;
import com.badlogic.gdx.ai.utils.NonBlockingSemaphore;
import com.badlogic.gdx.ai.utils.NonBlockingSemaphoreRepository;
    */
    /** A {@code SemaphoreGuard} decorator allows you to specify how many characters should be allowed to concurrently execute its
     * child which represents a limited resource used in different behavior trees (note that this does not necessarily involve
     * multithreading concurrency).
     * <p>
     * This is a simple mechanism for ensuring that a limited shared resource is not over subscribed. You might have a pool of 5
     * pathfinders, for example, meaning at most 5 characters can be pathfinding at a time. Or you can associate a semaphore to the
     * player character to ensure that at most 3 enemies can simultaneously attack him.
     * <p>
     * This decorator fails when it cannot acquire the semaphore. This allows a selector task higher up the tree to find a different
     * action that doesn't involve the contested resource.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author davebaol */
    public class SemaphoreGuard : Decorator
    {

        ///** Mandatory task attribute specifying the semaphore name. */
        //[TaskAttribute("", true)]
        //public String name;

        //[NonSerialized]
        //private NonBlockingSemaphore semaphore;
        //private bool semaphoreAcquired;

        ///** Creates a {@code SemaphoreGuard} decorator with no child. */
        //public SemaphoreGuard()
        //{
        //}

        ///** Creates a {@code SemaphoreGuard} decorator with the given child.
        // * 
        // * @param task the child task to wrap */
        //public SemaphoreGuard(Task task)
        //    : base(task)
        //{
        //}

        ///** Creates a {@code SemaphoreGuard} decorator with no child the specified semaphore name.
        // * 
        // * @param name the semaphore name */
        //public SemaphoreGuard(String name)
        //    : base()
        //{
        //    this.name = name;
        //}

        ///** Creates a {@code SemaphoreGuard} decorator with the specified semaphore name and child.
        // * 
        // * @param name the semaphore name
        // * @param task the child task to wrap */
        //public SemaphoreGuard(String name, Task task)
        //    : base(task)
        //{
        //    this.name = name;
        //}

        ///** Acquires the semaphore. Also, the first execution of this method retrieves the semaphore by name and stores it locally.
        // * <p>
        // * This method is called when the task is entered. */
        //public override void start()
        //{
        //    if (semaphore == null)
        //    {
        //        semaphore = NonBlockingSemaphoreRepository.getSemaphore(name);
        //    }
        //    semaphoreAcquired = semaphore.acquire();
        //    base.start();
        //}

        ///** Runs its child if the semaphore has been successfully acquired; immediately fails otherwise. */
        //public override void run()
        //{
        //    if (semaphoreAcquired)
        //    {
        //        base.run();
        //    }
        //    else
        //    {
        //        fail();
        //    }
        //}

        ///** Releases the semaphore.
        // * <p>
        // * This method is called when the task exits. */
        //public override void end()
        //{
        //    if (semaphoreAcquired)
        //    {
        //        if (semaphore == null)
        //        {
        //            semaphore = NonBlockingSemaphoreRepository.getSemaphore(name);
        //        }
        //        semaphore.release();
        //        semaphoreAcquired = false;
        //    }
        //    base.end();
        //}

        //public override void reset()
        //{
        //    base.reset();
        //    semaphore = null;
        //    semaphoreAcquired = false;
        //}

        //protected override Task copyTo(Task task)
        //{
        //    SemaphoreGuard semaphoreGuard = (SemaphoreGuard)task;
        //    semaphoreGuard.name = name;
        //    semaphoreGuard.semaphore = null;
        //    semaphoreGuard.semaphoreAcquired = false;

        //    return base.copyTo(task);
        //}
    }
}