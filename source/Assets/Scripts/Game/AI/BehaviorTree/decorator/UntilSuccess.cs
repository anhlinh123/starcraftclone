namespace StarcraftClone.AI.BehaviorTree
{
    /** The {@code UntilSuccess} decorator will repeat the wrapped task until that task succeeds, which makes the decorator succeed.
     * <p>
     * Notice that a wrapped task that always fails without entering the running status will cause an infinite loop in the current
     * frame.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author implicit-invocation
     * @author davebaol */
    public class UntilSuccess : LoopDecorator
    {

        /** Creates an {@code UntilSuccess} decorator with no child. */
        public UntilSuccess()
        {
        }

        /** Creates an {@code UntilSuccess} decorator with the given child.
         * 
         * @param task the child task to wrap */
        public UntilSuccess(Task task)
            : base(task)
        {
        }

        public override void childSuccess(Task runningTask)
        {
            success();
            loop = false;
        }

        public override void childFail(Task runningTask)
        {
            loop = true;
        }
    }
}