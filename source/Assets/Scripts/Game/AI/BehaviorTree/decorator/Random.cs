namespace StarcraftClone.AI.BehaviorTree
{
    /** The {@code Random} decorator succeeds with the specified probability, regardless of whether the wrapped task fails or succeeds.
     * Also, the wrapped task is optional, meaning that this decorator can act like a leaf task.
     * <p>
     * Notice that if success probability is 1 this task is equivalent to the decorator {@link AlwaysSucceed} and the leaf
     * {@link Success}. Similarly if success probability is 0 this task is equivalent to the decorator {@link AlwaysFail} and the leaf
     * {@link Failure}.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author davebaol */
    [TaskConstraint(0, 1)]
    public class Random : Decorator
    {

        /** Optional task attribute specifying the random distribution that determines the success probability. It defaults to
         * {@link ConstantFloatDistribution#ZERO_POINT_FIVE}. */
        [TaskProperty]
        public FloatDistribution success_chance { get; set; }

        private float p;

        /** Creates a {@code Random} decorator with no child that succeeds or fails with equal probability. */
        public Random()
            : this(ConstantFloatDistribution.ZERO_POINT_FIVE)
        {
        }

        /** Creates a {@code Random} decorator with the given child that succeeds or fails with equal probability.
         * 
         * @param task the child task to wrap */
        public Random(Task task)
            : this(ConstantFloatDistribution.ZERO_POINT_FIVE, task)
        {
        }

        /** Creates a {@code Random} decorator with no child that succeeds with the specified probability.
         * 
         * @param success the random distribution that determines success probability */
        public Random(FloatDistribution success)
            : base()
        {
            this.success_chance = success;
        }

        /** Creates a {@code Random} decorator with the given child that succeeds with the specified probability.
         * 
         * @param success the random distribution that determines success probability
         * @param task the child task to wrap */
        public Random(FloatDistribution success, Task task)
            : base(task)
        {
            this.success_chance = success;
        }

        /** Draws a value from the distribution that determines the success probability.
         * <p>
         * This method is called when the task is entered. */
        public override void start()
        {
            p = success_chance.nextFloat();
        }

        public override void run()
        {
            if (child != null)
                base.run();
            else
                decide();
        }

        public override void childFail(Task runningTask)
        {
            decide();
        }

        public override void childSuccess(Task runningTask)
        {
            decide();
        }

        private void decide()
        {
            if (RandomHelper.NextDouble() <= p)
                success();
            else
                fail();
        }

        protected override Task copyTo(Task task)
        {
            Random random = (Random)task;
            random.success_chance = success_chance; // no need to clone since it is immutable

            return base.copyTo(task);
        }

    }
}
