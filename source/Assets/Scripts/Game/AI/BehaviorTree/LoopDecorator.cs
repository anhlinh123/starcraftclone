namespace StarcraftClone.AI.BehaviorTree
{

    /** {@code LoopDecorator} is an abstract class providing basic functionalities for concrete looping decorators.
     * 
     * @param <E> type of the blackboard object that tasks use to read or modify game state
     * 
     * @author davebaol */
    public abstract class LoopDecorator : Decorator
    {

        /** Whether the {@link #run()} method must keep looping or not. */
        protected bool loop;

        /** Creates a loop decorator with no child task. */
        public LoopDecorator()
        {
        }

        /** Creates a loop decorator that wraps the given task.
         * 
         * @param child the task that will be wrapped */
        public LoopDecorator(Task child)
            : base(child)
        {
        }

        /** Whether the {@link #run()} method must keep looping or not.
         * @return {@code true} if it must keep looping; {@code false} otherwise. */
        public virtual bool condition()
        {
            return loop;
        }

        public override void run()
        {
            loop = true;
            while (condition())
            {
                if (child.status == Status.RUNNING)
                {
                    child.run();
                }
                else
                {
                    child.setControl(this);
                    child.start();
                    if (child.checkGuard(this))
                        child.run();
                    else
                        child.fail();
                }
            }
        }

        public override void childRunning(Task runningTask, Task reporter)
        {
            base.childRunning(runningTask, reporter);
            loop = false;
        }

    }
}