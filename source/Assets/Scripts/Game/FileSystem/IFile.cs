﻿namespace StarcraftClone.FileSystem
{
    public interface IFile
    {
        System.IO.Stream Read();
        System.IO.StreamReader Read(System.Text.Encoding encoding);
    }
}
