﻿namespace StarcraftClone.FileSystem
{
    public static class FileManager
    {
        private static System.Collections.Generic.List<FileSystem> m_fileSystems = new System.Collections.Generic.List<FileSystem>();

        public static void AddFileSystem(FileSystem fileSystem)
        {
            if (!m_fileSystems.Contains(fileSystem))
                m_fileSystems.Add(fileSystem);
        }

        public static void RemoveFileSystem(FileSystem fileSystem)
        {
            m_fileSystems.Remove(fileSystem);
        }

        public static IFile OpenFile(string filePath)
        {
            IFile file = null;
            foreach (FileSystem fs in m_fileSystems)
            {
                if (fs.TryCreateIFile(filePath, out file))
                    break;
            }
            return file;
        }
    }
}
