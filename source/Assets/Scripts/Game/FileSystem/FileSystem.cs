﻿namespace StarcraftClone.FileSystem
{
    public abstract class FileSystem
    {
        private System.Collections.Generic.List<string> m_searchPaths = null;
        protected abstract bool DoesFileExist(string filePath);
        protected abstract bool TryCreateIFileV(string filePath, out IFile file);

        public void SetSearchPath(System.Collections.Generic.List<string> searchPaths)
        {
            m_searchPaths = searchPaths;
        }

        public void SetSearchPath(params string[] searchPaths)
        {
            m_searchPaths = new System.Collections.Generic.List<string>(searchPaths);
        }

        public void AddSearchPath(string path)
        {
            if (!m_searchPaths.Contains(path))
                m_searchPaths.Add(path);
        }

        public bool TryCreateIFile(string name, out IFile file)
        {
            string fullPath = null;
            foreach (string p in m_searchPaths)
            {
                fullPath = p + name;
                if (DoesFileExist(p + name))
                    return TryCreateIFileV(fullPath, out file);
            }
            if (DoesFileExist(name))
                return TryCreateIFileV(name, out file);

            file = null;
            return false;
        }
    }
}
