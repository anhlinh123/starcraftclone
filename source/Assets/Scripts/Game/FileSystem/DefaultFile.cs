﻿namespace StarcraftClone.FileSystem
{
    public class DefaultFile : IFile
    {
        private string m_path = null;

        public DefaultFile(string path)
        {
            m_path = path;
        }

        public System.IO.Stream Read()
        {
            return System.IO.File.OpenRead(m_path);
        }

        public System.IO.StreamReader Read(System.Text.Encoding encoding)
        {
            return new System.IO.StreamReader(Read(), encoding);
        }
    }
}
