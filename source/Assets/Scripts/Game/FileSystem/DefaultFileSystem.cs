﻿namespace StarcraftClone.FileSystem
{
    public class DefaultFileSystem : FileSystem
    {
        public DefaultFileSystem()
            : this(new System.Collections.Generic.List<string>())
        {
        }

        public DefaultFileSystem(System.Collections.Generic.List<string> searchPaths)
        {
            SetSearchPath(searchPaths);
        }

        public DefaultFileSystem(params string[] searchPaths)
        {
            SetSearchPath(searchPaths);
        }

        protected override bool DoesFileExist(string filePath)
        {
            return System.IO.File.Exists(filePath);
        }

        protected override bool TryCreateIFileV(string filePath, out IFile file)
        {
            file = new DefaultFile(filePath);
            return true;
        }
    }
}
