﻿using UnityEngine;

namespace StarcraftClone
{
    class Lobby : MonoBehaviour
    {
        LevelInfo levelInfo;
        public LevelInfo LevelInformation
        {
            get
            {
                return levelInfo;
            }
            set
            {
                levelInfo = value;
                RandomizeLocation();
                Debug.Log("numPlayers = " + levelInfo.playerInfos.Length);
            }
        }

        void RandomizeLocation()
        {
            if (levelInfo.playerInfos != null && levelInfo.playerInfos.Length > 0)
                levelInfo.playerInfos[0].isHuman = true;
        }
    }
}
