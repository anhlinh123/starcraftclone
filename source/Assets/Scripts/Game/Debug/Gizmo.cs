﻿using System.Collections.Generic;
using UnityEngine;

namespace StarcraftClone
{
    static class Gizmo
    {
        static Dictionary<string, IGeometry> _dictionary;

        static Gizmo()
        {
            _dictionary = new Dictionary<string, IGeometry>();
        }

        public static void Draw(string name, IGeometry geometry)
        {
            if (_dictionary.ContainsKey(name))
            {
                _dictionary[name] = geometry;
            }
            else
            {
                _dictionary.Add(name, geometry);
            }
        }

        public static void Flush()
        {
            foreach (IGeometry geometry in _dictionary.Values)
            {
                geometry.Draw();
            }
        }
    }

    interface IGeometry
    {
        void Draw();
    }

    class LineGeometry : IGeometry
    {
        public Vector3 from;
        public Vector3 to;
        public Color color;
        public LineGeometry(Vector3 from, Vector3 to, Color color)
        {
            this.from = from;
            this.to = to;
            this.color = color;
        }

        public void Draw()
        {
            Gizmos.color = color;
            Gizmos.DrawLine(from, to);
        }
    }

    class RectGeometry : IGeometry
    {
        public Vector3 pointA;
        public Vector3 pointB;
        public Color color;
        public RectGeometry(Vector3 pointA, Vector3 pointB, Color color)
        {
            this.pointA = pointA;
            this.pointB = pointB;
            this.color = color;
        }

        public void Draw()
        {
            Gizmos.color = color;
            Gizmos.DrawLine(pointA, new Vector3(pointA.x, pointB.y));
            Gizmos.DrawLine(pointA, new Vector3(pointB.x, pointA.y));
            Gizmos.DrawLine(pointB, new Vector3(pointA.x, pointB.y));
            Gizmos.DrawLine(pointB, new Vector3(pointB.x, pointA.y));
        }
    }
}
