﻿/* Wrap the Unity Debug class inside to easily turn on/off debug in debug/release build
 * */

namespace StarcraftClone
{
    public static class Debug
    {
        [System.Diagnostics.Conditional("DEBUG")]
        public static void Log(object message)
        {
            UnityEngine.Debug.Log(message);
        }

        [System.Diagnostics.Conditional("DEBUG")]
        public static void Log(object message, UnityEngine.Object context)
        {
            UnityEngine.Debug.Log(message, context);
        }
    }
}
